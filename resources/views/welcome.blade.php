<!DOCTYPE html>
@if(Agent::isDesktop())
    <html>
        <head>
            <title>aPure Group By</title>
        </head>
        <body>
            <h2 style="text-align:center">Desktops are not allowed on this platform. Please use your mobile device.</h2>
        </body>
    </html>
@endif

@if(Agent::is('iPhone'))
    @include('mobile.ios')
@endif

@if(Agent::isAndroidOS())
    @include('mobile.android')
@endif

@if(Agent::isTablet())

@endif

@if(Agent::isRobot())

@endif