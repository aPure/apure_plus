<style>
.vip_submenu{
    position: relative;
    top: 0;
    height: 5%;
    width: 100%;
    background-color: green;
    color: white;
    font-size: 130%;
    padding: 10px 7px;
    font-weight: bold;
}
.vip_introduction{
    position: relative;
    height: 15%;
    width: 100%;
    text-align: center;
}
.chart{
    position: relative;
    padding-top: 5%;
    height: 35%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
}
.admin_buttons{
    position: relative;
    height: 25%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
    border-bottom: 15px rgba(0, 0, 0, 0.5) solid;
    background-color: green;
}
.add_buyers{
    position: relative;
    height: 12%;
    width: 100%;
    text-align: center;
    background-color: rgba(128, 128, 128, 0.3);
    color: white;
    font-weight: bold;
    font-size: 150%;
    padding-top: 8%;
}

.group_info_performance{
    position: absolute;
    top: 0;
    right: 0;
    width: 50%;
    height: 100%;
}
.button_left{
    position: absolute;
    left:0;
}
.button_right{
    position: absolute;
    right:0;
}
.toolbar-fixed .floating-button, .toolbar-through .floating-button{
    position: absolute;
    bottom: 10px;
    left:35%;
}
.all_buyers{
    position: relative;
}
</style>

<div data-page="buyers_list" class="page cached">
    <div class="page-content" >
        <div class="vip_submenu"><a class="vip_mode">團購資訊</a> <a class="buyer_mode">團購商店</a></div>
        <div class="vip_introduction_buyers_list"></div>
        <div class="all_buyers accordion-list list-block">
            <ul>
                
            </ul> 

        </div>
        <a class="go_back_vip">Go back</a>
        <div class="add_buyers">+</div>
    </div>
</div>