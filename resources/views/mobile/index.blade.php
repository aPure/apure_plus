<div data-page="index" class="page cached">
    <div class="page-content" >
    <!-- <div class="content-block-title"></div> -->
        <div class="swiper-custom">
            <div class="swiper-container swiper-1">
                <div class="swiper-pagination"></div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide"><img src='img/pure55banner0701.jpg' /></div>
                    <div class="swiper-slide"><img src='img/pure55man.jpg' /></div>
                    <div class="swiper-slide"><img src='img/stock_banner.jpg' /></div>
                </div>
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
            </div>
        </div>

        <div class="content-block">
            <img src='img/anniversary.jpg' />
        </div>
        <div class="content-block-title"></div>

        <div class="content-block">
            <img src='img/002135.jpg' />
        </div>

        <div class="content-block">
            <img src='img/002136.jpg' />
        </div>

        <div class="content-block">
            <img src='img/002137.jpg' />
        </div>

        <div class="content-block">
            <img src='img/162700.jpg' />
        </div>

        <div class="swiper-container swiper-2">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src='img/112812.jpg' /></div>
                <div class="swiper-slide"><img src='img/112810.jpg' /></div>
                <div class="swiper-slide"><img src='img/112807.jpg' /></div>
                <div class="swiper-slide"><img src='img/112812.jpg' /></div>
            </div>
        </div>


        <div class="content-block">
            <img src='img/162634.jpg' />
        </div>

        <div class="swiper-container swiper-3">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src='img/071002.jpg' /></div>
                <div class="swiper-slide"><img src='img/070956.jpg' /></div>
                <div class="swiper-slide"><img src='img/070950.jpg' /></div>
                <div class="swiper-slide"><img src='img/071002.jpg' /></div>
            </div>
        </div>

        <div class="content-block">
            <img src='img/162613.jpg' />
        </div>

        <div class="swiper-container swiper-4">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src='img/120704.jpg' />1980 <br /> 長版POLO</div>
                <div class="swiper-slide"><img src='img/091555.jpg' /></div>
                <div class="swiper-slide"><img src='img/091521.jpg' /></div>
                <div class="swiper-slide"><img src='img/091555.jpg' /></div>
            </div>
        </div>

        <div class="content-block-title"></div>

        <div class="swiper-container swiper-5">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src='img/165758.jpg' /></div>
                <div class="swiper-slide"><img src='img/165741.jpg' /></div>
            </div>
        </div>
        <div class="swiper-container swiper-6">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src='img/165652.jpg' /></div>
                <div class="swiper-slide"><img src='img/165635.jpg' /></div>
            </div>
        </div>

        <!-- all-product-area start-->
        <div class="all-product-area warper">
            <div class="buttons-row">
                <!-- Link to 1st tab, active -->
                <a href="#tab1" class="tab-link active">FEATURED</a>
                <!-- Link to 2nd tab -->
                <a href="#tab2" class="tab-link ">POPULAR</a>
                <!-- Link to 3rd tab -->
                <a href="#" class="tab-link">SEE MORE</a>
            </div>
                <div class="tabs">
                <!-- Tab 1, active by default -->
                <div id="tab1" class="tab active">
                    <div class="content-block">
                        <div class="row">
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/1.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aenean sagittis</a></h3>
                                                <p>MODEL: CC3005-85E</p>
                                                <h4>$ 1100</h4>
                                                <div class="pro-rating-s">




                                                    <p>(25 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                    <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/2.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aenean tristiqu</a></h3>
                                                <p>MODEL: STR-2018</p>
                                                <h4>$ 2110</h4>
                                                <div class="pro-rating-s">




                                                    <p>(65 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/3.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aliquam lobortis</a></h3>
                                                <p>MODEL: CC3005-858</p>
                                                <h4>$ 1585</h4>
                                                <div class="pro-rating-s">




                                                    <p>(30 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                    <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/4.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Auctor gravida</a></h3>
                                                <p>MODEL: STR-2458</p>
                                                <h4>$ 1235</h4>
                                                <div class="pro-rating-s">




                                                    <p>(15 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                        </div>
                    </div>
                </div>
                <!-- Tab 2 -->
                <div id="tab2" class="tab">
                    <div class="content-block">
                        <div class="row">
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                    <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/5.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Auctor gravida</a></h3>
                                                <p>MODEL: STR-2458</p>
                                                <h4>$ 1235</h4>
                                                <div class="pro-rating-s">




                                                    <p>(15 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/6.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aliquam lobortis</a></h3>
                                                <p>MODEL: CC3005-858</p>
                                                <h4>$ 1585</h4>
                                                <div class="pro-rating-s">




                                                    <p>(30 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/7.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aenean sagittis</a></h3>
                                                <p>MODEL: CC3005-85E</p>
                                                <h4>$ 1100</h4>
                                                <div class="pro-rating-s">




                                                    <p>(25 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <ul>
                                                        <li><a href="#"><i class="fa fa-shopping-basket"></i></a></li>
                                                        <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                            <!-- single content  start-->
                            <div class="col-100 tablet-50">
                                    <div class="single-product">
                                    <div class="row">
                                        <div class="col-50">
                                            <div class="pro-img">
                                                <a href="single-product.html">
                                                    <img src="img/product/8.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-50">
                                            <div class="pro-content">
                                                <h3><a href="single-product.html">Aenean eu tristiqu</a></h3>
                                                <p>MODEL: STR-2018</p>
                                                <h4>$ 2110</h4>
                                                <div class="pro-rating-s">
                                                    <p>(65 Customer Rivew) </p>
                                                </div>
                                                <div class="add-cate">
                                                    <a href="#"><i class="fa fa-shopping-basket"></i></a>
                                                    <a href="#"><i class="fa fa-heart"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- single content  end-->
                        </div>
                    </div>
                </div>

                </div>
        </div>
        <!-- all-product-area end-->
    </div>
</div>
