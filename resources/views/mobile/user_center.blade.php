<style>
.button_purchase_summary{
    position: absolute;
    right: 0;
    border-radius: 20px;
    border: 2px black solid;
    width: 90px;
    text-align: center;
    height: 25px;
    vertical-align: middle;
}
.text_and_button_below{
    font-size: 20px;
    height: 30px;
}
.user_center_titles{
    font-weight: bold;
    color: black;
    font-size: 20px;
}
</style>

<div data-page="user_center" class="page cached">
    @if(Agent::isAndroidOS())
        @include('mobile.android_toolbar')
    @endif

    <div class="page-content" >

        <div class="content-block-title user_center_titles">
            基本資料
        </div>

        <div class="list-block infos_purchase">
            <ul>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>姓名</span><input class="user_name" type="text" value="" disabled />
                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>電話</span><input class="user_phone" type="text" value="" disabled />
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>
        <div class="content-block-title user_center_titles">
            團購主
        </div>

        <div class="list-block">
            <ul>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>姓名</span><input class="vip_name" type="text" value="" disabled />
                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>電話</span><a class="vip_phone external link" disabled></a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="list-block">
            <ul>
              
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title go_to_summary_shopping">
                            <span>訂單資訊</span>
                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title change_password">
                            <span>修改密碼</span><span class="badge color-red if_password_not_changed_control">N</span>

                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title remove_myself">
                            <span>Remove myself</span>

                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title apure_contact">
                            <span>aPure客服</span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

       

    </div>
</div>

