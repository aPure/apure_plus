<div data-page="login" class="page cached form">

    <img src="img/signIn.jpg" />
    <div class="page-content login_opacity">
        <img class="logo" src="img/Logo_white.png" />
        <form class="login_form" id="login_info">
            {{ csrf_field() }}
            <input type="tel" class="phone" placeholder="Phone" name="phone" required/>
            <input type="password" class="pwd" name="password" placeholder="Password" required/>
            <input type="hidden" name="isSalesID" value="0"/>
            <input type="hidden" name="mobile" value="1"/>
            <input type="button" id="submitLogin" value="Login" />
        </form>
    </div>

</div>

