<div class="panel-overlay"></div>
<div class="panel panel-left panel-cover layout-white">
    <div class="list-block-title"><i class="icon icon-back color-gray close-panel position_panel_back"></i><span class="period_menu"></span></div>
    
    <div class="list-block theme-black">
        <div class="list-group">
            <ul>
                <li class="list-group-title"></li>
                <li>
                    <div class="item-content button_block">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/user.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text user_from_panel">個人資料<span class="badge color-red if_password_not_changed_control">N</span></a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="item-content button_block">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/shoppingcart.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text cart_from_panel">我的購物車<span class="badge choosen_items color-red">15</span></a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="item-content button_block">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/openshop.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text house_from_panel">目前開放團購商品</a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="item-content button_block">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/hostuser.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text user_from_panel">團購主資訊</a>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <div class="list-group logout_botton_group">
            <ul>
                <li class="list-group-title"></li>
                <li>
                    <div class="item-content">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/logout.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text logout">登出</a>
                        </div>
                    </div>
                </li>
                
            </ul>
        </div>

        <div class="list-group logout_botton_group">
            <ul>
                <!-- <li class="list-group-title browser_recommend">Works better on Chrome and Safari</li> -->
                <!-- <li>
                    <div class="item-content">
                        <div class="item-media">
                            <img class="icon" src="http://apure.com.tw/img/group/icons/logout.png" />
                        </div>
                        <div class="item-inner">
                            <a href="#" class="external menu_text logout">登出</a>
                        </div>
                    </div>
                </li> -->
                
            </ul>
        </div>
        
    </div>
</div>

<div class="panel panel-right panel-cover layout-white">
    <div class="list-block-title">Categories</div>
    
    <div class="list-block theme-black">
        <div class="list-group">
            <ul class="categories">
                
            </ul>
        </div>
        
    </div>
</div>