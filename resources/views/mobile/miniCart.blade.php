<section id="miniCart">
		<header>
			<div class="total">$<span class="num" id="total-cart">0.00</span></div>
			<!-- <div class="checkout"><a href="#order" title="購物車" class="ir">結帳</a></div>
			<div class="unfold"></div> -->
		</header>
		<div class="miniCartMid" style="display: none;">
			<div class="miniCartInform">
				<ul class="inform">
        	<!-- list of promotions -->
        	<li>feGood氣爽衣雙11普天同慶 - 2件$900【以兩件為單位購買】</li>
					<li>全館滿2000送200，還差100元</li>
				</ul>
        
			</div>
			<div class="miniCartPicked">
				<ul class="picked" id="show-cart">
					<li>
						<div class="itemPic">
							<img src="img/product/T09018_1_cs.jpg">
						</div>
						<div class="itemName">女圓領休閒氣爽衣</div>
						<div class="itemTxt"><span class="itemQuantity">X1</span><span class="itemPrice">$ 450</span></div>
						<div class="delete" onclick="deleteCar(this,'${cart.GoodsSerNo}','${cart.Total}','');"></div>
					</li>
				</ul>
			</div>
		</div>
		
</section>