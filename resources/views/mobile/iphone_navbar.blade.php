<div class="navbar theme-white">
    <div class="navbar-inner cached" data-page="special_current_customers">
    
        <div class="left ">
            <a href="#" class="open-left-panel" style="width: 30px; height: 30px;"><i class="icon icon-bars"></i></a>
        </div>
        <div class="center sliding">
            <img class="navbarlogo" src='img/logo_white_without_slogan.png' />
        </div>

        <div class="right"></div>
        
    </div>

    <div class="navbar-inner cached" data-page="confirm_shopping">
        
        <div class="left ">
            
        </div>
        <div class="center sliding" style="color:white">
            購物車
        </div>
        <div class="right">
            <a href="#" class="go_back_home" style="color:white">X</a>
        </div>
        
    </div>

    <div class="navbar-inner cached" data-page="info_shopping">
        
        <div class="left ">
            <a href="#" class="back link toolbarfinish"><i class="icon icon-back"></i></a>
        </div>
        <div class="center sliding" style="color:white">
            購物車
        </div>
        <div class="right">
            <a href="#" class="go_back_home toolbarfinish" style="color:white">X</a>
        </div>
        
    </div>

    <div class="navbar-inner cached" data-page="user_center">
        
        <div class="left ">
            <a href="#" class="link back"><i class="icon icon-back"></i></a>
        </div>
        <div class="center sliding" style="color:white">
            訂單查詢
        </div>
        
    </div>

</div>