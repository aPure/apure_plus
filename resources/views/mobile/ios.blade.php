@extends('mobile.layout')
@section('specific_style')
    <link rel="stylesheet" href="css/framework7.ios.min.css">
    <link rel="stylesheet" href="css/framework7.ios.colors.min.css">

    <style> 
        .navbar .center{
            height: 90%;
        }
        .top_banner{
            margin-top: 20px;
            margin-bottom: 2px;
            padding: 0 9px;
        }
    </style>

@endsection