@extends('mobile.layout')
@section('specific_style')
    <link rel="stylesheet" href="css/framework7.material.min.css">
    <link rel="stylesheet" href="css/framework7.material.colors.min.css">
    <style>
        .navbar .top{
            margin-bottom:-15px;
        }
        .top_banner{
            margin-top: 10px;
            margin-bottom: 2px;
            padding: 0 9px;
        }
        .toolbar{
            display: none;
        }

    </style>
@endsection