<div class="popup popup-blur">
    <a href="#" class="close-popup">X</a>
    <div class='product_container'>
      <div class='single_product_image' id='".$value[0]."'>
          <img src="">
      </div>
      <div class='product_add_to_cart add-to-cart' data-number='1' data-id='' data-price='' data-name='' data-img='' data-size='' data-color='' >
          <img src='img/button_on.png'>
      </div>
      <div class='product_description'></div>
      <div class='product_number_choice'><select>
        @include('choice_number_item')
      </select></div>
      <div class='product_size_choice'>
        <div class="one">S</div><div class="two">M</div><div class="three">L</div><div class="four">XL</div><div class="five">XXL</div><div class="six">C130</div><div class="seven">C140</div>
      </div>
      <div class='product_price'></div>
      <div class='product_original_price'></div>
    </div>
                        
</div>