<style>
    .check{
        width: 30%;
        left: 35%;
        position: absolute;
        top: 30%;
    }
    .confirm_text{
        width: 60%;
        position: absolute;
        left: 20%;
        top: 50%;
        text-align: center;
        font-size: 28px;
    }
    .selected_bar_finish_shopping{
        background-color: #ff5d55;
        width: 33%;
        height: 5px;
        left:66%;
        top: 20px;
        position: relative;
        z-index: 100;
    }
</style>
<div data-page="finish_shopping" class="page cached">
    @if(Agent::isAndroidOS())
        @include('mobile.android_toolbar')
    @endif
    <div class="page-content" >
        <div class="submenu">
            <div class="confirm_button">確認購物車</div> <div class="info_button">訂購及取貨資訊</div> <div class="finish_button">完成</div>
        </div>
        <div class='selected_bar_finish_shopping'></div><div class='submenu_sep_info_shopping'></div>
        <img class="check" src='img/check.png' />
        <div class="confirm_text">    
            <p>訂單已送出</p> 
            <p>aPure感謝您的訂購</p>  
        </div>
    </div>
</div>
