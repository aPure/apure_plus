<style>
.selected_bar_info_shopping{
    background-color: #ff5d55;
    width: 33%;
    height: 5px;
    left:33%;
    top: 20px;
    position: relative;
    z-index: 100;
}
.submenu_sep_info_shopping{
    background-color: gray;
    width: 100%;
    height: 5px;
    left:0;
    top: 15px;
    position: relative;
    z-index:0;
}
.terms_conditions{
    margin: 55px;
    text-align: center;
}
</style>
<div data-page="info_shopping" class="page cached">
    @if(Agent::isAndroidOS())
        @include('mobile.android_toolbar')
    @endif
    <div class="page-content" >

        <div class="submenu">
            <div class="confirm_button">確認購物車</div> <div class="info_button">訂購及取貨資訊</div> <div class="finish_button">完成</div>
        </div>
        <div class='selected_bar_info_shopping'></div><div class='submenu_sep_info_shopping'></div>
        <div class="content-block-title">
            取貨人資料(團購主)
        </div>

        <div class="list-block">
            <ul>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>姓名</span><input class="vip_name" type="text" value="" disable />
                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>電話</span><input class="vip_phone" type="text" value="" disable />
                        </div>
                    </div>
                </li>
                
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>送貨地址</span><input class="vip_address" type="text" value="" disable />
                        </div>
                    </div>
                </li>


            </ul>
        </div>
        <div class="content-block-title">
            訂購人資訊
        </div>

        <div class="list-block">
            <ul>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>姓名</span><input class="user_name" type="text" value="" disable />
                        </div>
                    </div>
                </li>
                <li class="item-content">
                    <div class="item-inner">
                        <div class="item-title">
                            <span>電話</span><input class="user_phone" type="text" value="" disable />
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <!-- <div class="content-block-title terms_conditions">
            <input type="checkbox" value="" class="accept_terms" /> 同意將商品寄給團購主
        </div> -->

    </div>
</div>
