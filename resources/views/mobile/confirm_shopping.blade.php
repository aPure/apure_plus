<div data-page="confirm_shopping" class="page cached">
    
    @if(Agent::isAndroidOS())
        @include('mobile.android_toolbar')
    @endif
    @if(Agent::is('iPhone'))
        <style>
            .minus{
                font-size: 30px;
                font-weight: bold;
            }
        </style>
    @endif
    <div class="page-content mainpage" id="confirm-cart"></div>
</div>
