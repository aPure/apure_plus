<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<style>
.vip_submenu{
    position: relative;
    top: 0;
    height: 5%;
    width: 100%;
    background-color: green;
    color: white;
    font-size: 130%;
    padding: 10px 7px;
    font-weight: bold;
}
.vip_introduction{
    position: relative;
    height: 15%;
    width: 100%;
    text-align: center;
}
.chart{
    position: relative;
    padding-top: 5%;
    height: 35%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
}
.admin_buttons{
    position: relative;
    height: 25%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
    border-bottom: 15px rgba(0, 0, 0, 0.5) solid;
    background-color: green;
}
.checkout{
    position: relative;
    height: 12%;
    width: 100%;
    text-align: center;
    background-color: rgba(128, 128, 128, 0.3);
    color: white;
    font-weight: bold;
    font-size: 150%;
    padding-top: 8%;
}
.chart_conteneur{
    width: 200px;
    height: 150px;
    position: absolute;
    left: -10px;
}

.group_info_performance{
    position: absolute;
    top: 0;
    right: 0;
    width: 50%;
    height: 100%;
    text-align: center;
}
.button_left{
    position: absolute;
    left:0;
}
.button_right{
    position: absolute;
    right:0;
    width: 50%;
    height: 100%;
}
.toolbar-fixed .floating-button, .toolbar-through .floating-button{
    position: absolute;
    bottom: 10px;
    left:35%;
}
.amount_spent, amount_spent_div, items_number, items_number_div, number_buyers, number_buyers_div, minimum_amount, minimum_amount_div, deadline, deadline_div{
    position: absolute;
}
</style>

<div data-page="vip_index" class="page vip_page cached">
    <div class="page-content" >
        <div class="vip_submenu"><a class="vip_mode">團購資訊</a> <a class="buyer_mode">團購商店</a></div>
        <div class="vip_introduction"></div>
        <div class="chart">
            目前團購進度
            <div class="chart_conteneur">
                <canvas id="myChart"></canvas>
            </div>  
            <div class="group_info_performance">
                <div class="amount_spent_div">累積金額</div>
                $<span class="amount_spent"></span>
                <div class="items_number_div">累積商品</div>
                <span class="items_number"></span>件
                <div class="number_buyers_div">群組人數</div>
                <span class="number_buyers"></span>人

                <div class="minimum_amount_div">目標金額</div>
                $<span class="minimum_amount"></span>

                <div class="deadline_div">截止日期</div>
                <span class="deadline"></span>
                    
                </div>      
        </div>
        <div class="admin_buttons">
            <div class="button_left">
                群組管理
                目前有4人在群組
                3人已消費
                
            </div>

            <div class="button_right">
                訂單管理 <br>
                第一期團購
                2018/5/1 - 2018/5/31
            </div>
   
            <a href="#" class="floating-button color-white to_customers_list">
                <i class="icon icon-plus" style="color:green">+</i>
            </a>

        </div>
        <div class="checkout">立即結帳</div>
    </div>
</div>

<script>
var ctx = document.getElementById("myChart").getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [3000, 7000], // [rest_to_order, amount_ordered]
            backgroundColor: [
                'rgba(0, 0, 0, 0.5)',
                'rgba(0, 255, 0, 0.5)'
            ],
            borderColor: [
                'rgba(0, 0, 0, 0.5)',
                'rgba(0, 255, 0, 0.5)'
            ]
        }]
    },
    options: {
        tooltips: {
            enabled: false
        }
    }
});
</script>
