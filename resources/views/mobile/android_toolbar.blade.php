<div class="toolbar toolbar-bottom theme-white">
    <div class="toolbar-inner toolbarmenu">
    
        <a href="#" class="house link">
            <img class="icon_apure" src="http://apure.com.tw/img/group/icons/house_green.png" />
            <div class="down_text">找團購</div>
        </a>
        
        <a href="#" class="cart link">
            <img class="icon_apure" src="http://apure.com.tw/img/group/icons/cart_gray.png" />
            <span class="badge choosen_items color-red android_badge"></span>
            <div class="down_text">購物車</div>
        </a>
        <a href="#" class="user link">
            <img class="icon_apure"src="http://apure.com.tw/img/group/icons/user_gray.png" />
            <span class="badge color-red android_badge if_password_not_changed">N</span>
            <div class="down_text">個人資料</div>
        </a>
    </div>

    <div class="toolbar-inner toolbarpurchasenext" style="display:none;">
        <a class="go_to_info_shopping"> <img src="img/next_step.png" /></a>
    </div>

    <div class="toolbar-inner toolbarfinish" style="display:none;">
        <a class="go_to_finish_shopping"> <img src="img/confirm_button.png" /> </a>
    </div>

    <div class="toolbar-inner toolbargoback" style="display:none;">
                <a class="go_back_home"><img src="img/go_back_to_main_page.png" /></a>
    </div>

</div>