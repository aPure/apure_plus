<html class="with-statusbar-overlay">
    <head>
        <title>aPure 機能性纖維 </title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no, minimal-ui">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

        <link rel="icon" href="http://diz36nn4q02zr.cloudfront.net/webapi/images/o/16/16/ShopFavicon/1987/1987favicon?v=201604191809">
        @yield('specific_style')
        <!-- <link rel="stylesheet" href="{{asset('css/common.css')}}"> -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.2.7/css/swiper.min.css">
        <link rel="stylesheet" href="{{asset('css/mobile/welcomescreen.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/mobile_layout.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/products_list_ip6_7_8.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/products_list_ip6_7_8_plus.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/products_list_media_320_370.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/products_list_media_200_320.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile/products_list_height_control.css')}}">

        <style>
            .go_to_info_shopping img, .go_to_finish_shopping img, .go_back_home img{
                width: 100%;
                height: 100%;
            }
            .down_text{
                font-size: 11px;
                bottom: -15px;
                color: gray;
            }
            .house .down_text{
                position: absolute;
                left: 30px;
            }
            .user .down_text{
                position: absolute;
                left: -7px;
                padding-left: 8px;
            } 
            .cart .down_text{
                position: absolute;
                left: 4px;
            }
            .if_password_not_changed{
                margin-left: -30px;
            }

           
        </style>

        <!-- Hotjar Tracking Code for http://vip.apure.com.tw -->
        <script>
            (function(h,o,t,j,a,r){
                h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
                h._hjSettings={hjid:908015,hjsv:6};
                a=o.getElementsByTagName('head')[0];
                r=o.createElement('script');r.async=1;
                r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
                a.appendChild(r);
            })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <!-- <script src="http://10.24.2.41:8080/target/target-script-min.js#anonymous"></script> -->
    </head>

    <body>
        
   

        <div class="statusbar-overlay"></div>
        @include('mobile.panel')
        <div class="views">
            <div class="view view-main"> 
                @if(Agent::is('iPhone'))
                    
                    @include('mobile.iphone_navbar')
                    @include('mobile.iphone_toolbar')
                
                    <div class="pages navbar-fixed toolbar-fixed theme-black">
                        @include('mobile.login')
                        @include('mobile.special_current_customers')
                        @include('mobile.user_center')
                        @include('mobile.confirm_shopping')
                        @include('mobile.info_shopping')
                        @include('mobile.finish_shopping')
                        @include('mobile.listing_products')
                        @include('mobile.product_popup')
                        @include('mobile.summary_shopping')
                        
                    </div>
                @endif

                
                @if(Agent::isAndroidOS())
                    <style>
                        .toolbar{
                            display: block;
                        }
                        .android_center{
                            position: absolute;
                            left: 150px;
                        }
                        .android_left{
                            position: absolute;
                            top: -3px;
                        }
                        .navbar .left {
                            margin-left: 5px;
                            margin-top: 5px;
                        }
                        .navbar .right {
                            margin-right: 10px;
                        }
                        .cart .down_text {
                            left: 20px;
                        }
                        .user .down_text {
                            left: 11px;
                        }
                        .house .down_text {
                            left: 46px;
                        }
                        .android_badge{
                            line-height: 20px;
                            margin-top: -25px;
                        }
                        
                        @media screen and (min-width: 350px) and (max-width: 370px) {
                            .right_add_to_cart {
                                left: 58%;
                            }
                            .right_description {
                                right: 1%;
                            }
                        }
                    </style>
                    <div class="pages navbar-fixed toolbar-fixed">
                        @include('mobile.android_navbar')
                        @include('mobile.special_current_customers')
                        @include('mobile.login')
                        @include('mobile.user_center')
                        @include('mobile.confirm_shopping')
                        @include('mobile.info_shopping')
                        @include('mobile.finish_shopping')
                        @include('mobile.listing_products')
                        @include('mobile.product_popup')
                        @include('mobile.summary_shopping')
                        
                    </div>

                @endif

            </div>

        </div>

    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{asset('js/shoppingCart.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/v1.7framework7.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/welcomescreen.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/welcomescreen_slides.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/colors.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/products_display_html_generation.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app1.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app_new_design.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app2.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app3.js')}}"></script>
       
</html>
