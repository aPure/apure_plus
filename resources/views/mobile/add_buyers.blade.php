<style>
.vip_submenu{
    position: relative;
    top: 0;
    height: 5%;
    width: 100%;
    background-color: green;
    color: white;
    font-size: 130%;
    padding: 10px 7px;
    font-weight: bold;
}
.vip_introduction{
    position: relative;
    height: 15%;
    width: 100%;
    text-align: center;
}
.chart{
    position: relative;
    padding-top: 5%;
    height: 35%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
}
.admin_buttons{
    position: relative;
    height: 25%;
    width: 100%;
    border-top: 15px rgba(0, 0, 0, 0.5) solid;
    border-bottom: 15px rgba(0, 0, 0, 0.5) solid;
    background-color: green;
}
.checkout{
    position: relative;
    height: 12%;
    width: 100%;
    text-align: center;
    background-color: rgba(128, 128, 128, 0.3);
    color: white;
    font-weight: bold;
    font-size: 150%;
    padding-top: 8%;
}
.group_info_performance{
    position: absolute;
    top: 0;
    right: 0;
    width: 50%;
    height: 100%;
}
.button_left{
    position: absolute;
    left:0;
}
.button_right{
    position: absolute;
    right:0;
}
.toolbar-fixed .floating-button, .toolbar-through .floating-button{
    position: absolute;
    bottom: 10px;
    left:35%;
}
</style>

<div data-page="add_buyers" class="page cached">
    <div class="page-content" >
        <div class="vip_submenu">團購資訊 團購商店</div>
        <div class="vip_introduction">基本資料</div>
        <a class="go_back_vip">Go back</a>
        <form class="form-horizontal" id="newbuyer" >
            {{ csrf_field() }}
            <input name="mobile" type="hidden" value="1">
            <div class="list-block">
                <ul>
                    
                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title">
                                姓名 <input class="add_customer_name" name="name" type="text" placeholder="">
                            </div>
                        </div>
                    </li>

                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title">
                                電話 <input name="phone" class="add_customer_phone" type="text" placeholder="">
                            </div>
                        </div>
                    </li>

                    <li class="item-content">
                        <div class="item-inner">
                            <div class="item-title">
                                <span>密碼預設為電話號碼</span> 
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
    
        </form>
    </div>
</div>