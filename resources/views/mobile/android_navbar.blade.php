<div class="navbar">
    <div class="navbar-inner homepage cached" data-page="special_current_customers">
    
        <div class="left ">
            <a href="#" class="open-left-panel" style="width: 30px; height: 30px;"><i class="icon icon-bars"></i></a>
        </div>
        <div class="android_center">
            <img class="navbarlogo" src='img/logo_white_without_slogan.png' />
        </div>

        <div class="right"></div>
        
    </div>

    <div class="navbar-inner confirm_shopping_page cached" data-page="confirm_shopping">
        
        <div class="android_left ">
            
        </div>
        <div class="android_center" style="color:white">
            購物車
        </div>
        <div class="right">
            <a href="#" class="go_back_home" style="color:white">X</a>
        </div>
        
    </div>

    <div class="navbar-inner info_shopping_page cached" data-page="info_shopping">
        
        <div class="android_left ">
            <a href="#" class="back link toolbarfinish"><i class="icon icon-back"></i></a>
        </div>
        <div class="android_center" style="color:white">
            購物車
        </div>
        <div class="right">
            <a href="#" class="go_back_home toolbarfinish" style="color:white">X</a>
        </div>
        
    </div>

    <div class="navbar-inner user_center_page cached" data-page="user_center">
        
        <div class="android_left ">
            <a href="#" class="link back"><i class="icon icon-back"></i></a>
        </div>
        <div class="android_center" style="color:white">
            訂單查詢
        </div>
        
    </div>

</div>

<script>    

</script>