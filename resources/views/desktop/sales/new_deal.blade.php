@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                <h1 class="pull-left"> 新增團購</h1>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <form class="form-horizontal" action="{{route('create_new_deal')}}" method='post' enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">起始時間</label>  
                                    <div class="col-md-4">
                                        <input name="from_when" type="date" required placeholder="" class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">結束時間</label>  
                                    <div class="col-md-4">
                                        <input name="until_when" type="date" required placeholder="" class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">最少累積商品件數</label>  
                                    <div class="col-md-4">
                                        <input name="minimum_amount_of_items" required type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">最少累積金額</label>  
                                    <div class="col-md-4">
                                        <input name="minimum_amount_of_money" required type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">Products</label>  
                                    <div class="col-md-4">
                                        <select class="form-control form-control-lg product_to_choose">
                                            @include('desktop.sales.products_select_options')
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">折扣</label>  
                                    <div class="col-md-4">
                                        <input type="text" placeholder="1-9折" class="form-control input-md discount">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput"></label>  
                                    <div class="col-md-4">
                                       <p class="add_discount" style="display:none;color:red">Add the discount here</p>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label> 
                                    <div class="col-md-4">
                                        <input type="button" class="form-control btn btn-primary input-md add_product" value="Add Product"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label>
                                    <div class="card col-md-4" style="width: 35rem;"> 
                                        <ul class="list-group list-group-flush list_chosen">
                                            <!-- <li class="list-group-item">Cras justo odio</li>
                                            <li class="list-group-item">Dapibus ac facilisis in</li>
                                            <li class="list-group-item">Vestibulum at eros</li> -->
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label"></label> 
                                    <div class="col-md-4">
                                        <input type="file" name="products_availaibilities" />
                                    </div>
                                </div>
                                
                                <input name="discount" class="products_chosen" type="hidden">
                                <input name="deal_products" class="products_chosen_id" type="hidden">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">啟動</label>  
                                    <div class="col-md-1">
                                        <input name="activate" type="checkbox" placeholder="" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton">開新團購</label>
                                    <div class="col-md-4">
                                        <button id="singlebutton" name="singlebutton" class="btn btn-primary">確認送出</button>
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    if("{{$result}}" == "no_discount"){
        alert("Add discounts");
    }
</script>
@endsection