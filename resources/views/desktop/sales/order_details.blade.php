@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                <h1 class="pull-left">訂單明細</h1>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                            <a href="{{route('order_history')}}">Back</a>
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th><span>產品名稱</span></th>
                                        <th><span>單價</span></th>
                                        <th><span>數量</span></th>
                                        <th><span>尺寸</span></th>
                                        <th><span></span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($order_details as $order)
                                            <tr>
                                                <td>
                                                    {{$order->productName}}
                                                </td>
                                                <td>
                                                    {{$order->price}}
                                                </td>
                                                <td>
                                                    {{$order->number}}
                                                </td>
                                                <td>
                                                    {{$order->size}}
                                                </td>
                                                <td>
                                                    <a href="{{route('remove_order_item', ['idofitem' =>  $order->id, 'buyerId' => $buyerId ])}}" onclick="return confirm('Sure ?');" class="table-link danger">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                            <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection