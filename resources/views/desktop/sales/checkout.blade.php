@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                <h1 class="pull-left">Checkout</h1>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        @if(Session::has('is_vip') && Session::has('is_paying'))
                            <form class="form-horizontal" action="{{route('vip_checkout_to_ctbk')}}" method='post'>
                                {{ csrf_field() }}
                                    <fieldset>

                                        <!-- <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">付款方式</label>  
                                            <div class="col-md-4">
                                                <input type="radio" id="twPaymentCash" name="twPaymentInput" value="快遞貨到付款" checked="true"> <label for="twPaymentCash">快遞貨到付款</label> <br />
                                                <input type="radio" id="twPaymentCard" name="twPaymentInput" value="信用卡線上付款"> <label for="twPaymentCard">信用卡線上付款（使用中國信託線上金流系統，交易安全有保障）</label>
                                            </div>
                                        </div> -->
                                        <input type="hidden" id="twPaymentCard" name="twPaymentInput" value="信用卡線上付款">

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">送貨地址</label>  
                                            <div class="col-md-4">
                                                <input id="textinput" name="address" type="text" placeholder="" required class="form-control input-md">
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">取貨時間</label>  
                                            <div class="col-md-4">
                                                <select class="form-control input-md" name="delivery_time">
                                                    <option value="平日上午">平日上午</option>
                                                    <option value="平日下午">平日下午</option>
                                                    <option value="週六上午">週六上午</option>
                                                    <option value="週六下午">週六下午</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton"></label>
                                            <div class="col-md-4">
                                            Your spending <b>{{$amount_to_pay}} NT</b>. <br>
                                            But after the discount, you are paying <b>{{$after_discount}} NT</b>
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" name="amount_to_pay" value="{{$after_discount}}"> 

                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="singlebutton">Order</label>
                                            <div class="col-md-4">
                                                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>
                                        
                                </fieldset>
                            </form>
                        @else
                            You are in the wrong place my friend
                        @endif
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection