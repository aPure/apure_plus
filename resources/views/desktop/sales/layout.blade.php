<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>aPure Sales Manager</title>
	
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- libraries -->
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>

    <!-- global styles -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/sales/layout.css')}}">
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('css/sales/element.css')}}"> -->

    <!-- this page specific styles -->

    <!-- google font libraries -->
	<!-- <link href="./SuperheroAdmin - Bootstrap Admin Template_files/css" rel="stylesheet" type="text/css"> -->
    
</head>
<body style="">
	<header class="navbar" id="header-navbar">
		<div class="container">
			<a href="{{route('admin_index')}}" id="logo" class="navbar-brand col-md-3 col-sm-3">
				@if(Session::has('is_vip'))
					<span>歡迎VIP {{Session::get('vip_name')}}</span> 
				@elseif(!Session::has('is_super'))
					<span>歡迎業務 {{Session::get('admin_code')}}</span>
				@else
					<span>Hi Simon</span> 
				@endif
			</a>
			
			<div class="nav-no-collapse pull-right" id="header-nav">
				<ul class="nav navbar-nav pull-right">
					<li>
						<a href="{{route('admin_index')}}" class="btn">
							<i class="fa fa-user"></i>
							
						</a>
						<span>群組名單</span>
					</li>
					<li class="item">
						@if(Session::has('is_vip'))
							<a href="{{route('order_history')}}">
								<i class="fa fa-shopping-cart"></i>
								<!-- <span class="content">New purchase</span>
								<span class="time"><i class="fa fa-clock-o"></i>13 min.</span> -->
							</a>
							<span>訂單管理</span>
						@elseif(!Session::has('is_super'))
							
							<a href="{{route('group_buy')}}">
								<i class="fa fa-shopping-cart"></i>
								<!-- <span class="content">New purchase</span>
								<span class="time"><i class="fa fa-clock-o"></i>13 min.</span> -->
							</a>
							<span>團購交易</span>
						@endif
					</li>
					<li class="item">
						@if(!Session::has('is_super'))
							<a href="{{route('vip_payments')}}">
								<i class="fa fa-money green"></i>
							</a>
							<span>付款紀錄</span>
						@endif
					</li>
					
					<li>
						<a href="{{route('renew')}}" class="btn">
							<i class="fa fa-lock"></i>
						</a>
						<span>更換密碼</span>
					</li>

					<li>
						<a href="{{route('logout', ['from_sales' => 1])}}" class="btn">
							<i class="fa fa-power-off"></i>
						</a>
						<span>登出</span>
					</li>
				</ul>
			</div>
		</div>
	</header>
	<div class="container">
		<div class="row">
			
			@if(Session::has('is_vip'))
				@if(!Request::is('sales/renew') 
				&& !Request::is('sales/vip_checkout') 
				&& !Request::is('sales/addvip')
				&& !Request::is('sales/add_group_buyers'))

					<div class="col-lg-3 col-sm-6">
					<div class="main-box infographic-box">
						<i class="fa fa-user red"></i>
						<span class="headline">群組人數:</span>
						<span class="value">{{$number_my_customers}}人</span>
					</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="main-box infographic-box">
							<i class="fa fa-shopping-cart emerald"></i>
							<span class="headline">累積商品: {{$itemsNumber}}件 </span>
							<span class="value"></span>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="main-box infographic-box">
							<i class="fa fa-money green"></i>
							<span class="headline">{{$amount_spent}} NT </span>
							<span class="value"></span>
								@if($progress == 100)
									<span id="checkAmount" class="headline">已達成成團條件! 請前往結帳</span>
									<!-- <span class="value">Bonus: {{floor($amount_spent * 0.06)}} NT</span> -->
								@endif
						</div>
					</div>
					<div class="col-lg-3 col-sm-6">
						<div class="main-box infographic-box">
							<i class="fa fa-clock-o yellow"></i>
							<span class="headline" id="checkTime">
								@if($time_left[0]==0 && $time_left[1]==0 && $time_left[2]==0 && $time_left[3]==0)
									期限已到
								@else
									團購剩餘:
								@endif
							</span>
							<span class="value">
								@if($time_left[0]!=0)
									{{$time_left[0]}} 天,
								@endif 
								@if($time_left[1]!=0)
									{{$time_left[1]}} 小時,
								@endif
								@if($time_left[2]!=0)
									{{$time_left[2]}} 分鐘
								@endif  
							</span>
						</div>
					</div>

				@endif
			@endif

			@yield('content')
		</div>
	</div>
	<footer id="footer-bar">
		<p id="footer-copyright">
			© <a href="#" target="_blank"></a>aPure
		</p>
	</footer>
	
	<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
	@if(Session::has('is_vip'))
		@if(!Request::is('sales/renew') && !Request::is('sales/vip_checkout') && !Request::is('sales/addvip'))
			<script>
				function checkIfCanCheckout(){
					var checktime = $('#checkTime').text();
					var checkamount = $('#checkAmount').text();
					if(checkamount == "已達成成團條件! 請前往結帳" 
						&& parseInt("{{$itemsNumber}}") >= parseInt("{{$itemsNumberShouldBe}}")){
							return true;
						}else{
							if(parseInt("{{$itemsNumber}}") < parseInt("{{$itemsNumberShouldBe}}")){
								alert("你需要訂購至少{{$itemsNumberShouldBe}}件");
							}else{
								alert("你需要訂購更多商品");
							}
							
							return false;
						}
				}

			</script>
		@endif
	@endif


		<script>
			$(".confirm_action").click(function(){
				var to_confirm = $(".confirm_vip:checked");
				//console.log(to_confirm);
				var ids_to_confirm = "";
				for(var i=0; i<to_confirm.length; i++){
					if(i!=0){
						ids_to_confirm+="-"
					}
					ids_to_confirm+=$(to_confirm[i]).attr('data-id');
				}
				//console.log(ids_to_confirm);
				$.ajax({
					type: "GET",
					url: "{{route('confirm_vip')}}",
					data: {ids: ids_to_confirm},
					success: function(data){
						location.reload();
						//console.log(data);
					}
				});
			});
			$(".add_product").on("click", function(){
				var discount = $('.discount').val();
				if(discount == ""){
					$(".add_discount").show();
					return false;
				}
				$(".add_discount").hide();
				var product_to_choose = $('.product_to_choose').val();
				var name_product_to_choose = $('.product_to_choose option:selected').text();
				$('.product_to_choose option:selected').remove();
				$('.list_chosen').append("<li class='list-group-item'>"+name_product_to_choose+"-"+discount+"折"+"</li>");
				var products_already_chosen = $('.products_chosen').val();
				var products_already_chosen_id = $('.products_chosen_id').val();
				var current_product = ""+name_product_to_choose+","+discount+"折";
				products_already_chosen_id+=""+product_to_choose+","+discount+";";
				products_already_chosen+=current_product+";";
				$('.products_chosen').val(products_already_chosen);
				$('.products_chosen_id').val(products_already_chosen_id);
				$('.discount').val("");
				//console.log(products_already_chosen_id);
			});
		</script>

		<script src="{{asset('js/pdfmake.min.js')}}"></script>
		<script src="{{asset('js/vfs_fonts.js')}}"></script>
		<script src="{{asset('js/webqrcode.js')}}"></script>
		<script src="{{asset('js/lineqrcode.js')}}"></script>
		<script src="{{asset('js/logo.js')}}"></script>
		<script>
	    	function print(paymentNumber, vip){
				event.preventDefault();
				var url = window.location.href;
				var server = url.replace("#", "");
				server = server.replace("vip_payments", "");
				server += "print/"+paymentNumber+"/"+vip; 

				$.get(server)
				.done(function( data ) {
				
					pdfMake.fonts = {
						Yahei: {
							normal: 'Microsoft YaHei.ttf',
							bold: 'Microsoft YaHei.ttf'
						}
					};
				
					var line ='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAo0AAAAGCAIAAAApJzMoAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABASURBVGhD7dixDQAwCANBnP13Jk1WiOTiroENEJ/dHQCg0nkTAOjz659O8jYAaNUflXVvAOilewNAL3caAFrNXFwbDAHPmRWOAAAAAElFTkSuQmCC';
						
					var docDefinition = {
						content: [
							
						],
						defaultStyle: {
							font: 'Yahei'
						},
						styles: {
							header: {
								fontSize: 22,
								bold: true,
								alignment: 'center'
							}
						}
						// ,
						// pageSize: 'A4'
					};
					//console.log(data);
					var contentToAppend = [];
					var buyerNames = [];
					var number_items = [];
					var buyer_index = [];
					for(var i in data){
						buyerNames.push(data[i]['buyerName']);
						number_items.push(data[i]['itemsBought'].length);
						buyer_index.push(i);
					}
					//console.log(buyerNames, number_items);
					count = 0;
					for(var i in data){
						count++;
						var max = Math.max(...number_items);
						var max_index = number_items.indexOf(max);
						
						var down_page = [
								
								{
									text: "aPure 感謝您的訂購",
									margin: [ 230, 0, 0, 0 ],
									fontSize: 10,
									bold: true 
								},
								{
									text: "暖心提醒",
									fontSize: 10,
									bold: true
								},
								{
									text: "1.團購屬於優惠活動，無法進行退換貨",
									fontSize: 8 
								},
								{
									text: "2.如有商品瑕疵、缺件等問題，請團購主於收到商品的七日內主動與aPure客服聯繫",
									fontSize: 8 
								},
								{
									text: "3.建議清洗時，使用中性洗劑清洗，對肌膚和商品都是多一層保護",
									fontSize: 8  
								},
								{
									text: "aPure 客服",
									fontSize: 10,
									bold: true 
								},
								{
									text: "電話客服時間: 周一 ~ 周五 09:00 ~ 18:00",
									fontSize: 8 
								},
								{
									text: "電話客服專線: (02)2267-8900 #113",
									fontSize: 8
								},
								{
									text: "電話客服信箱: customer@apuremail.com",
									fontSize: 8
								},
								{
									image: lineqrcode,
									width: 50,
									margin: [ 400,-50,0,0 ]
								}
						];
						
						contentToAppend.push([
								{
									image: logo,
									width: 100
								},{
									text: "出貨明細表",
									margin: [ 230, -40, 0, 0 ],
									fontSize: 18
								},
								{
									text: "團購期限:"+ data[buyer_index[max_index]]['until_when'],
									margin: [ 0,20,0,0 ]
								},
								{
									text: "編號:"+ paymentNumber,
									margin: [ 0,0,0,0 ]
								},
								{
									text: "訂購人 "+data[buyer_index[max_index]]['buyerName'], 
									margin: [400, -30,0, 0]
								},
								{
									text: "團購主 "+data[buyer_index[max_index]]['vipName'],
									margin: [ 400,0,0,0 ]
								},{
									text: "品名",
									margin: [ 0,30,0,0 ]    
								},{
									text: "數量",
									margin: [ 280,-15,0,0 ]
								},{
									text: "單價",
									margin: [ 340,-15,0,0 ]
								},{
									text: "金額",
									margin: [ 420,-15,0,0 ]
								},{
									text: "備註",
									margin: [ 480,-17,0,0 ]
								},
								{
									image: line,
									margin: [ -50,0,0,0 ]
								}
							
						]);
						
						var items = data[buyer_index[max_index]]['itemsBought'];
						var products_sku = [];
						var products_index = [];
						var products_sku_index = new Object();
						for(var item in items){
							products_sku.push(items[item]['productId']);
							//products_index.push(items[item]['id']);
							products_sku_index[items[item]['productId']] = items[item]['id'];
						}
						//console.log(products_sku_index);
						products_sku.sort();
						for(var sku in products_sku){
							products_index.push(products_sku_index[products_sku[sku]]);
						}
						var line_top = 0;
						var total_price = 0;
						var total_items = 0;
						for(var id in products_index){
							//console.log(products_sku[id]);
							for(var i in items){
								
								if(products_index[id] == items[i]['id']){
									//console.log(items[i]);
									line_top+=1;
									var discount = parseFloat(items[i]['discount']);
									var item_price = Math.ceil(parseInt(items[i]['price'])*discount*0.1);
									var price = item_price*parseInt(items[i]['number']);
									total_price+=price;
									total_items+=parseInt(items[i]['number']);
									contentToAppend.push([{
										text: items[i]['productId'] +" "+items[i]['productName']+"-"+items[i]['size'],
										margin: [ 0,10,0,0 ],
										fontSize: 10   
									},{
										text: items[i]['number'],
										margin: [ 290,-15,0,0 ],
										fontSize: 10    
									},{
										text: item_price,
										margin: [ 345,-15,0,0 ],
										fontSize: 10    
									},{
										text: price,
										margin: [ 425,-15,0,0 ],
										fontSize: 10    
									}]);
									break;
								}
							}
							
						}
						if(number_items[max_index] < 4){
							line_top+=2;
						}
						contentToAppend.push([{
								image: line,
								margin: [ -50,line_top,0,0 ]
						},
						{
							text: total_items+" 件商品",
							margin: [ 0,0,0,0 ]
						},
						{
							text: "$"+total_price,
							margin: [ 425,-15,0,0 ]
						}
						]);
						if(number_items[max_index] > 5 && count != buyerNames.length){
							down_page.push({
								image: webqrcode,
								width: 50,
								margin: [480,-60,0,0 ],
								pageBreak: 'after'
							});
						}else if(count%2==1){
							down_page.push({
								image: webqrcode,
								width: 50,
								margin: [480,-60,0,50 ],
							});
						}else if(number_items[max_index] < 5 && count%2==0 && count != buyerNames.length){
							down_page.push({
								image: webqrcode,
								width: 50,
								margin: [480,-61,0,50 ],
								pageBreak: 'after'
							});
						}else{
							down_page.push({
								image: webqrcode,
								width: 50,
								margin: [480,-60,0,0 ],
							});
						}
						
						contentToAppend.push(down_page);
					
						number_items[max_index] = 0;
					}
					docDefinition['content'].push(contentToAppend);
					pdfMake.createPdf(docDefinition).download('payment_'+paymentNumber+'.pdf');
				});
				
				
			}
		</script>


   </body>

</html>