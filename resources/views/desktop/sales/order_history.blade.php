@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                @if(Session::has('is_vip'))
                    <h1 class="pull-left">訂單管理</h1>
                    <div class="pull-right top-page-ui">
                        <a href="{{route('vip_checkout', ['totalVIP' => $totals[0], 'totalBuyer'=> $totals[1]])}}" onclick="return checkIfCanCheckout();" class="btn btn-primary pull-right">
                            <i class="fa fa-shopping-cart fa-lg"></i> 前往結帳
                        </a>
                    </div>
                @else
                    <h1 class="pull-left">團購交易</h1>
                    
                    <div class="pull-right top-page-ui">
                        <a href="{{route('new_deal')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 開新團購
                        </a>
                    </div>
                @endif
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                        @if(count($order_history) >= 1)
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th><span>電話號碼</span></th>
                                        <th><span>成員名稱</span></th>
                                        <th><span>總金額</span></th>
                                        <th><span>折扣後總金額</span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                        @foreach($order_history as $order)
                                            <tr>
                                                <td>
                                                    <a href="{{route('order_details', ['buyerId' => $order->id ])}}">{{$order->cel}}</a>
                                                </td>
                                                <td>
                                                    {{$order->name}}
                                                </td>
                                                <td>
                                                    {{$order->totalForVIP}}
                                                </td>
                                                <td>
                                                    {{$order->totalForBuyer}}
                                                </td>
                                                
                                            </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                            @else
                                <div style="text-align:center">No Deal</div>
                            @endif
                        </div>
                        <!-- <ul class="pagination pull-right">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection