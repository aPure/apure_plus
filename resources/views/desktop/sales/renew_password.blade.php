@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
             
                <h1 class="pull-left">更新密碼</h1>
               
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                    
                        <form class="form-horizontal" action="{{route('renewpass')}}" method='post'>
                            {{ csrf_field() }}
                            <input name="isspecialuser" type="hidden" value="1">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="textinput">請輸入舊的密碼</label>  
                                        <div class="col-md-4">
                                            <input id="textinput" name="old_pass" type="password" placeholder="" class="form-control input-md">
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                    <label class="col-md-4 control-label" for="textinput">請輸入新的密碼</label>  
                                    <div class="col-md-4">
                                    <input id="textinput" name="new_pass" type="password" placeholder="" class="form-control input-md">
                                    </div>
                                    </div>

                                    <div class="form-group">
                                    <label class="col-md-4 control-label" for="singlebutton">更新</label>
                                    <div class="col-md-4">
                                        <button id="singlebutton" name="singlebutton" class="btn btn-primary">確認送出</button>
                                    </div>
                                    </div>

                                </fieldset>
                            </form>
                            @if($resp == -1)
                                <span class="help-block" style="color:red">Old password is wrong</span> 
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection