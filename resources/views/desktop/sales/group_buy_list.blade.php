@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
            
            <div class="clearfix">
                @if(Session::has('is_vip'))
                    <h1 class="pull-left">訂單管理</h1>
                @else
                    <h1 class="pull-left">團購交易</h1>
                    
                    <!-- <div class="pull-right top-page-ui">
                        <form action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                <input type="file" name="import_file" />
                                <button class="btn btn-primary">Change </button>
                        </form>
                    </div> -->
                    <div class="pull-right top-page-ui">
                        <a style="visibility:hidden" class="btn btn-primary pull-right"></a>
                    </div>
                    <div class="pull-right top-page-ui">
                        <a href="{{route('new_deal')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 開新團購
                        </a>
                    </div>
                @endif
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                        @if(count($group_buy_deals) >= 1)
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th><span>開始時間</span></th>
                                        <th><span>結束時間</span></th>
                                        <th><span>建立時間</span></th>
                                        <th><span>最少累積商品件數</span></th>
                                        <th><span>最少累積金額</span></th>
                                        <th><span>折扣</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>

                                        @foreach($group_buy_deals as $group)
                                            <tr>
                                                
                                                <td>
                                                    {{$group->from_when}}
                                                </td>
                                                <td>
                                                    {{$group->until_when}}
                                                </td>
                                                <td>
                                                    {{$group->created_at}}
                                                </td>
                                                <td>
                                                    {{$group->minimum_amount_of_items}}
                                                </td>
                                                <td>
                                                    {{$group->minimum_amount_of_money}}
                                                </td>
                                                <td>
                                                    {{$group->discount}}
                                                </td>
                                                <td style="width: 20%;">
                                                    <!-- <a href="#" class="table-link">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                            <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </a> -->
                                                    <a href="{{route('change_deal_activated', ['idofvip' => $group->id, 'current_state' => $group->activated ])}}" class="table-link">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                            @if($group->activated == "1")
                                                                <i class="fa fa-toggle-on fa-stack-1x fa-inverse"></i>
                                                            @else
                                                                <i class="fa fa-toggle-off fa-stack-1x fa-inverse"></i>
                                                            @endif
                                                        </span>
                                                    </a>
                                                    <a href="{{route('remove_group_deal_vip', ['idofvip' => $group->id ])}}" onclick="return confirm('Sure ?');" class="table-link danger">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                            <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                            @else
                                <div style="text-align:center">No Deal</div>
                            @endif
                        </div>
                        <!-- <ul class="pagination pull-right">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection