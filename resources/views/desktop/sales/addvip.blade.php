@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                @if(Session::has('is_vip')) 
                    <h1 class="pull-left">Add Customers</h1>
                @else
                    <h1 class="pull-left">新增一位VIP會員</h1>
                @endif
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        @if(Session::has('is_vip')) 
                            <form class="form-horizontal" action="{{route('add_group_buyers')}}" method='post'>
                                {{ csrf_field() }}
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">手機號碼</label>  
                                            <div class="col-md-4">
                                                <input id="textinput" name="phone" required type="tel" pattern="[0-9.]*" placeholder="0989******" class="form-control input-md">
                                            </div>
                                        </div>
                                    
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="textinput">姓名</label>  
                                        <div class="col-md-4">
                                        <input id="textinput" name="name" type="text" required placeholder="" class="form-control input-md">
                                        </div>
                                        </div>

                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton">新增為組員</label>
                                        <div class="col-md-4">
                                            <button id="singlebutton" name="singlebutton" class="btn btn-primary">確認送出</button>
                                        </div>
                                        </div>

                                    </fieldset>
                                </form>
                            @else
                                <form class="form-horizontal" action="{{route('submitnewvip')}}" method='post'>
                                    {{ csrf_field() }}
                                    <fieldset>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">姓名</label>  
                                            <div class="col-md-4">
                                                <input name="name" required type="text" placeholder="" class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">手機號碼</label>  
                                            <div class="col-md-4">
                                                <input name="phone" required type="text" placeholder="" class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label" for="textinput">Email</label>  
                                            <div class="col-md-4">
                                                <input name="email" type="email" required placeholder="" class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="singlebutton">新增為VIP</label>
                                        <div class="col-md-4">
                                            <button id="singlebutton" name="singlebutton" class="btn btn-primary">Submit</button>
                                        </div>
                                        </div>

                                    </fieldset>
                                </form>
                            @endif
                        
                        @if($resp == -4)
                            @if(Session::has('is_vip'))
                                <span class="help-block" style="color:red">Did you type something ? Enter some values please</span> 
                            @endif
                        @endif
                        @if($resp == -3)
                            @if(Session::has('is_vip'))
                                <span class="help-block" style="color:red">He is already group buyer</span> 
                            @endif
                        @endif
                        @if($resp == -2)
                            @if(Session::has('is_vip'))
                                <span class="help-block" style="color:red">You reached your maximum number of customers you can add</span> 
                            @else
                                <span class="help-block" style="color:red">He is already a VIP Customer</span> 
                            @endif
                        @endif
                        @if($resp == -1)
                            @if(Session::has('is_vip'))
                                <span class="help-block" style="color:red">Already under another VIP Customer</span> 
                            @else
                                <span class="help-block" style="color:red">This is NOT an Old Customer</span> 
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection