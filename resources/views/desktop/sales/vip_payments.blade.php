@extends('desktop.sales.layout')

@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
            
            <div class="clearfix">
                
                <h1 class="pull-left">
                @if(!Session::has('is_vip')) VIP @endif 付款紀錄
                </h1>
                @if(!Session::has('is_vip'))
                    <div class="pull-right top-page-ui">
                        <a href="{{route('excel_summary')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 下載本期團購訂單Excel檔
                        </a>
                    </div>
                @endif
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                        @if(count($vip_payments) >= 1)
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th><span>編號</span></th>
                                        <th><span>姓名</span></th>
                                        <th><span>付款日期</span></th>
                                        <th><span>地址</span></th>
                                        @if(Session::has('is_vip')) 
                                        <th><span>Before discount</span></th>
                                        @endif
                                        <th><span>After discount</span></th>
                                        <th><span>VIP Bonus</span></th>
                                        <th><span>宅配指定時段</span></th>
                                        <th><span>Print</span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                        @foreach($vip_payments as $pay)
                                            <tr>
                                                
                                                <td>
                                                    <a href="{{route('paymentOrder', ['paymentOrderNumber' => $pay->paymentNumber ])}}">{{$pay->paymentNumber}}</a>
                                                </td>
                                                <td>
                                                    {{$pay->vip}}
                                                </td>
                                                <td>
                                                    {{$pay->created_at}}
                                                </td>
                                                <td>
                                                    {{$pay->address}}
                                                </td>
                                                @if(Session::has('is_vip')) 
                                                <td> {{$pay->before_discount}}</td>
                                                @endif
                                                <td>
                                                    {{$pay->howmuch}}
                                                </td>
                                                <td>
                                                    {{floor($pay->howmuch * 0.06)}}
                                                </td>
                                                <td>
                                                    {{$pay->delivery_time}}
                                                </td>     
                                                
                                                <td>
                                                    <a onclick="print('{{$pay->paymentNumber}}', '{{$pay->vip}}');" href="#">
                                                        <span class="fa-stack">
                                                            <i class="fa fa-square fa-stack-2x"></i>
                                                            <i class="fa fa-print fa-stack-1x fa-inverse"></i>
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                            @else
                                <div style="text-align:center">No Payment yet</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

