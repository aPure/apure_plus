@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
            
            <div class="clearfix">
                @if(Session::has('is_vip'))
                    <h1 class="pull-left">群組名單</h1>
                    
                    <div class="pull-right top-page-ui">
                        <a href="{{route('admin_addvip')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 新增成員
                        </a>
                    </div>
                @elseif(Session::has('is_super'))    
                    <h1 class="pull-left">VIP會員申請名單</h1>
                    
                    <a class="btn btn-primary pull-right confirm_action">
                        <i class="fa fa-plus-circle fa-lg"></i> 確認核可
                    </a>
                    
                @else
                
                    <h1 class="pull-left">VIP會員管理</h1>
                    
                    <div class="pull-right top-page-ui" style="margin-left: 10px;">
                        <a href="{{route('list_customers')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> All customers
                        </a>
                    </div>
                    
                    <div class="pull-right top-page-ui">
                        <a href="{{route('admin_addvip')}}" class="btn btn-primary pull-right">
                            <i class="fa fa-plus-circle fa-lg"></i> 新增VIP
                        </a>
                    </div>
                    
                    

                @endif
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                        @if(count($customers) >= 1)
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th><span>姓名</span></th>
                                        @if(!Session::has('is_super'))
                                            <th><span>加入時間</span></th>
                                        @endif
                                        <th><span>電話號碼</span></th>
                                        @if(!Session::has('is_vip') && !Session::has('is_super'))
                                            <th><span>申請狀態</span></th>
                                        @endif
                                        @if(Session::has('is_super'))
                                            <!-- <th><span>Address</span></th>
                                            <th><span>Company</span></th> -->
                                        @endif
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>

                                        @foreach($customers as $cus)
                                            <tr>
                                                
                                                <td>
                                                    @if(Session::has('is_vip'))
                                                        {{$cus->name}}
                                                    @else
                                                        {{$cus->customer_name}}
                                                    @endif
                                                </td>
                                                @if(!Session::has('is_super'))
                                                    <td>
                                                        {{$cus->created_at}}
                                                    </td>
                                                @endif
                                                <td>
                                                    @if(Session::has('is_vip'))
                                                        {{$cus->cel}}
                                                    @else
                                                        {{$cus->username}} 
                                                    @endif
                                                </td>
                                                @if(!Session::has('is_vip') && !Session::has('is_super'))
                                                    <td>
                                                        @if($cus->confirmed == "0")
                                                            尚未通過
                                                        @else
                                                            核可
                                                        @endif
                                                    </td>
                                                @endif
                                                
                                                @if(!Session::has('is_super')) 
                                                    <td style="width: 20%;">
                                                        <a href="{{route('removevip', ['idofvip' => $cus->id ])}}" onclick="return confirm('Sure ?');" class="table-link danger">
                                                            <span class="fa-stack">
                                                                <i class="fa fa-square fa-stack-2x"></i>
                                                                <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>
                                                    </td>
                                                @else
                                                    <td style="width: 20%;">
                                                        <input type="checkbox" class="confirm_vip" data-id="{{$cus->id}}" />
                                                    </td>
                                                @endif
                                            </tr>
                                        
                                        @endforeach
                                </tbody>
                            </table>
                            @else
                                <div style="text-align:center">No VIP Customer</div>
                            @endif
                        </div>
                        <!-- <ul class="pagination pull-right">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection