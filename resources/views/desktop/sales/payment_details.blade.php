@extends('desktop.sales.layout')


@section('content')
<div class="col-md-12" id="content-wrapper">
    <div class="row" style="opacity: 1; transform: translateY(0px);">
        <div class="col-lg-12">
        
            <div class="clearfix">
                <h1 class="pull-left">Payment Details</h1>
            </div>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-box clearfix">
                        <div class="table-responsive">
                            <a href="{{route('vip_payments')}}">Back</a>
                            <table class="table user-list">

                            </table>
                            
                            <table class="table user-list">
                                <thead>
                                    <tr>
                                        <th  style="text-align:center"><span>Buyer name</span></th>
                                        <th  style="width:20%;text-align:center"><span>產品名稱</span></th>
                                        <th  style="width:20%;text-align:center"><span>編號</span></th>
                                        <th  style="width:20%;text-align:center"><span>單價</span></th>
                                        <th  style="width:20%;text-align:center"><span>數量</span></th>
                                        <th  style="width:20%;text-align:center"><span>尺寸</span></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($orderedItems as $buyer)
                                            <tr rowspan="{{count($buyer['itemsBought'])}}">
                                                <td style="text-align:center;padding-top:20px;">
                                                    {{$buyer['buyerName']}}
                                                </td>
                                                <td colspan="5">
                                                    <table  style="width:100%;text-align:center">
                                                    @foreach($buyer['itemsBought'] as $item)
                                                        <tr>
                                                            <td style="width:20%;text-align:center;padding-top:20px;">
                                                                {{$item->productName}}
                                                            </td>
                                                            <td style="width:20%;text-align:center">
                                                                {{$item->productId}}
                                                            </td>
                                                            <td style="width:20%;text-align:center">
                                                                {{floatval($item->price)*floatval($item->discount)*0.1}}
                                                            </td>
                                                            <td style="width:20%;text-align:center">
                                                                {{$item->number}}
                                                            </td>
                                                            <td style="width:20%;text-align:center">
                                                                {{$item->size}}
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </table>
                                                </td>
                                            </tr>
                                        
                                    @endforeach

                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection