<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=500, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel="icon" href="http://diz36nn4q02zr.cloudfront.net/webapi/images/o/16/16/ShopFavicon/1987/1987favicon?v=201604191809">
        <link rel='stylesheet prefetch' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,700italic,400italic'>
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('css/common.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery.jqzoom.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
		<link rel="stylesheet" href="{{asset('css/owl.theme.default.css')}}">
        <!-- <link rel="stylesheet" href="css/style.css"> -->

        <title>aPure 機能性纖維</title>
    </head>
    <body>
        <div id="fb-root"></div>
                <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.10&appId=468548860196394";
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>


                <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&appId=304578509695678&version=v2.0";
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

        <div class="container">

            @include('desktop.miniCart')

            @include('desktop.header')

            @yield('content')

            @include('desktop.footer')

        </div>
    </body>
    <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
    <script src="{{asset('js/bowser.min.js')}}" type="text/javascript">
        // https://github.com/lancedikson/bowser
    </script>
    <script src="{{asset('js/jquery.jqzoom-core.js')}}" type="text/javascript"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function(){
            $("img").lazyload();
        });
    </script> -->
    <script src="{{asset('js/flaps_shoppingCart_UI.js')}}"></script>
    <script src="{{asset('js/shoppingCart.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/products.js')}}"></script>
    <script src="{{asset('js/owl.carousel.js')}}" data-cover></script>
    <script>
        $(document).ready(function() {
            $('.itemPicture').jqzoom({
                    zoomType: 'standard',
                    lens:true,
                    preloadImages: false,
                    alwaysOn:false
                });

        });
    </script>
    <script>
        $('.thumbnail li').hide();

        $('.colorsList li a').click(function(){
            var id = $(this).attr('id');
            $('.thumbnail li').hide();
            $('.'+id).show();
        });

        $('.colorsList li:first a').trigger('click');
    </script>

<script>
            function addingToCart(id, name, img, price, number, size, color, type){
                    shoppingCart.addItemToCart(id, name, img, price, number, size, color, type);
                    displayCart();
                    // Flaps mini cart UI
                    miniCartUnfold();
                    miniCartFold();
            }

            $(".add-to-cart").click(function(event){
                event.preventDefault();
                var id = $(this).attr("data-id");
                var price = Number($(this).attr("data-price"));
                var name = $(this).attr("data-name");
                var img = $(this).attr("data-img");
                var size = $(this).attr("data-size");
                //console.log(size);
                var color = $(this).attr("data-color");
                var type = $(this).attr("data-type");

                var rootParentClass = $(this)
                .parent().parent().parent().parent().parent().parent()
                .prop('className');
                rootParentClass = rootParentClass.split(" ");
                if(rootParentClass.includes("B")){
                    var cartArray = shoppingCart.listCart();
                    var itemTypes = [];
                    var number_A = 0;
                    var number_B = 0;
                    for (var i in cartArray) {
                        itemTypes.push(cartArray[i].type);
                        if(cartArray[i].type == "A") number_A+=cartArray[i].count;
                        if(cartArray[i].type == "B") number_B+=cartArray[i].count;
                    }
                    //Check if at A choice are more than B choices. If yes
                    if(number_A >= number_B + 1){
                        // Can buy
                        addingToCart(id+size, name, img, price, 1, size, color, type);
                    }else{
                        // Cannot buy
                        alert("請增加發熱衣數量才可以選擇贈送的內褲");
                    }

                }else{
                    addingToCart(id+size, name, img, price, 1, size, color, type);
                }

            });

            $("#clear-cart").click(function(event){
                shoppingCart.clearCart();
                displayCart();
            });

            function displayCart() {
                var cartArray = shoppingCart.listCart();
                var output = "";
                var order_table = "";

                for (var i in cartArray) {
                    var img_link = cartArray[i].img;
                    img_link = img_link.replace(/_m_/g,'_cs_');
                    //console.log(img_link);
                    output += "<li><div class='itemPic'><img src='"+img_link+"'></div><div class='itemName'>"
                        +cartArray[i].name
                        +"</div><div class='itemTxt'><span class='itemQuantity item-count'>"
                        +cartArray[i].count+
                        "</span>x<span class='itemPrice'>"
                        +cartArray[i].price
                        +"</span> = "
                        +cartArray[i].total
                        +" </div><div class='plus-item itemTxt' data-id='"
                        +cartArray[i].id+"'></div>"
                        +" <div class='subtract-item' data-id='"
                        +cartArray[i].id+"'></div>"
                        +" <div class='delete-item delete' data-id='"
                        +cartArray[i].id+"'></div>"
                        +"</li>";

                    order_table += "<tr><td>"
                                + cartArray[i].name + "-"
                                + cartArray[i].color+"-"
                                +cartArray[i].size
                                +"</td><td>"
                                +cartArray[i].price
                                +"</td><td>"
                                +cartArray[i].count
                                +"</td><td>"
                                +cartArray[i].total
                                +"</td></tr>";
                }

                $("#show-cart").html(output);
                $("#count-cart").html( shoppingCart.countCart() );
                $("#total-cart").html( shoppingCart.totalCart() );
                $("#order-total").html( shoppingCart.totalCart() );
                $("#list-cart").html( order_table );
            }

            $("#show-cart").on("click", ".delete-item", function(event){
                var id = $(this).attr("data-id");
                shoppingCart.removeItemFromCartAll(id);
                displayCart();
            });

            $("#show-cart").on("click", ".subtract-item", function(event){
                var id = $(this).attr("data-id");
                shoppingCart.removeItemFromCart(id);
                displayCart();
            });

            // $("#show-cart").on("click", ".plus-item", function(event){
            //     var id = $(this).attr("data-id");
            //     var name = $(this).attr("data-name");
            //     var img = $(this).attr("data-img");
            //     shoppingCart.addItemToCart(id, name, img, 0, 1);
            //     displayCart();
            // });

            // $("#show-cart").on("change", ".item-count", function(event){
            //     var id = $(this).attr("data-id");
            //     var count = Number($(this).val());
            //     shoppingCart.setCountForItem(id, count);
            //     displayCart();
            // });

            $(".size_selection").on("change", "", function(event){
                var size = $(this).val();
                $(".add-to-cart").attr("data-size", size);
                //alert(size);
            });

            displayCart();

        </script>

			<script>
				$(document).ready(function(){
                    var owlA = $('.A');
                    var owlB = $('.B');
                    var config = {
                        nav: true,
                        dots: false,
                        items: 4,
                        onChanged: itemPosition
                    };
					owlA.owlCarousel(config);
                    owlB.owlCarousel(config);

                    function itemPosition(event){
                        var element = event.target.className.split(" ");
                        var item  = event.item.index;
                        //console.log(item);
                        // if(item>10){
                        //     if(element.includes("A")){
                        //         owlA.trigger('next.owl.carousel', [300]);
                        //     }
                        //     if(element.includes("B")){
                        //         owlB.trigger('next.owl.carousel', [300]);
                        //     }
                        // }

                    }

                });


		    </script>

        <script>
            var address = window.location.href;
            address = address.split("/");
            if(address[address.length -1] == "checkout" || address[address.length -1] == "confirm"){
                document.getElementById("mainFooter").style.marginTop = 0;
                document.getElementById("miniCart").style.display = "none";
            }
            if(address[address.length -1] == "checkout" || address[address.length -1] == "vip_checkout_to_ctbk"){
                $("#pay").submit();
            }

            $(".confirm_finalCheck").click(function(){
                // var name = $("#confirm_checkform input[name=name]").val();
                // var payment = $("#confirm_checkform input[name=payment]").val();
                // var tel = $("#confirm_checkform input[name=tel]").val();
                // var address = $("#confirm_checkform input[name=address]").val();

                // var jsontosend = {
                //     name: name,
                //     payment: payment,
                //     tel: tel,
                //     address: address,
                //     cart: cart
                // };
                var cart = shoppingCart.listCart();
                cart = JSON.stringify(cart);
                $("#confirm_checkform input[name=order]").attr("value", cart);
                $("#confirm_checkform input[name=total_amount]").attr("value", shoppingCart.totalCart());            
                
                $("#confirm_checkform").submit();
                
                

                //alert(jsontosend);
                // $.ajax({
                //     type: "GET",
                //     url: "{{route('checkout')}}",
                //     data: jsontosend,
                //     success: function(){
                //         alert("ok");
                //     },
                //     error: function(err){
                //         alert(err);
                //     }
                // });

                shoppingCart.clearCart();
            });

            $('.logout').click(function(){
                shoppingCart.clearCart();
            });
            // $("input[name='twPaymentInput']").on("change", "", function(){
            //     if($(this).val() == "card"){
            //         //$(".send_to_someone").show();
            //     }else{
            //         //$(".send_to_someone").hide();
            //     }
            // })


        </script>

</html>
