<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">

  <link rel="stylesheet" href="css/style.css">


</head>

<body>
  <div class="form">

        <div id="login">
        @if (\Request::is('sales'))  
          <h1>aPure Sales 系統</h1>
          <form action="{{route('login')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="isSalesID" value="1"/>
            @if(Agent::is('iPhone') || Agent::isAndroidOS())
              <input type="hidden" name="mobile" value="1"/>
            @endif
            <div class="field-wrap">
              <input type="text" required autocomplete="off" name="salesID" placeholder="salesID*"/>
            </div>
            <div class="field-wrap">
              <input type="password" required autocomplete="off" name="password" placeholder="密碼" />
            </div>
            <!-- <p class="forgot"><a href="#">Forgot password</a></p> -->
            <button class="button button-block"/>登入</button>
            <!-- <p class=""><a href="">回到首頁</a></p> -->
          </form>
        @else
          <h1>aPure Sales 系統</h1>
          <form action="{{route('login')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="isSalesID" value="1"/>
            @if(Agent::is('iPhone') || Agent::isAndroidOS())
              <input type="hidden" name="mobile" value="1"/>
            @endif
            <div class="field-wrap">
              <input type="text" required autocomplete="off" name="salesID" placeholder="salesID*"/>
            </div>
            <div class="field-wrap">
              <input type="password" required autocomplete="off" name="password" placeholder="密碼" />
            </div>
            <!-- <p class="forgot"><a href="#">Forgot password</a></p> -->
            <button class="button button-block"/>登入</button>
            <!-- <p class=""><a href="">回到首頁</a></p> -->
          </form>
          <!-- <h1>歡迎光臨aPure購物系統</h1>
          <form action="{{route('login')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="isSalesID" value="0"/>
            <div class="field-wrap">
              <input type="text" required autocomplete="off" name="phone" placeholder="手機號碼*"/>
            </div>
           
            <div class="field-wrap">
              <input type="password" required autocomplete="off" name="password" placeholder="密碼(首次登入為手機號碼)" />
            </div>
            <button class="button button-block"/>登入</button>
          </form> -->
        <!-- @endif -->
        </div>

      </div><!-- tab-content -->

</div> <!-- /form -->
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

    <script  src="js/index1.js"></script>
    <script>
      
      var result = "{{$result}}";
      if(result == 1){
        var phoneInDB = "{{$phoneInDB}}";
        var passwordNotCorrect = "{{$passwordNotCorrect}}";

        if(phoneInDB == 0){
          alert("You are not yet allowed to log into this system");
        }
        if(phoneInDB == 1 && passwordNotCorrect == 0){
          alert("Hey your password is wrong");
        }
        
      }
      if(result == 2){
        alert("Welcome back my friend. But you cannot login anymore. You will be informed when you can");
      }

      if(result == 3){
        alert("You are not yet confirmed as VIP");
      }

      var address = window.location.href;
      address = address.split("/");
      
      var isDesktop = "{{$isDesktop}}";
      
      if(isDesktop != 1){
        if(address[address.length -1] == "login" || address[address.length -1] == "special_current_customers"){
          address.pop();
          location.href = address.join("/");
          console.log(address.join("/"));
        }
      }
    </script>

</body>
</html>
