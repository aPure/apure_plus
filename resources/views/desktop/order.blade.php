@extends('desktop.layout')
@section('content')

	<div id="mainContent">
		<div class="layout-978">
			<div class="row">
				<div class="col12">
                <div id="fillout">
					<div class="checkoutFlow"> </div>
					<div class="sellInfo"><div>

					<table class="productList">
						<thead>
							<tr>
								<td>商品</td>
								<td>價格(NT)</td>
								<td>數量</td>
								<td>總價(NT)</td>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="3" align="right">購買金額</td>
								<td >NT <b id="order-total"></b></td>
							</tr>
						</tfoot>
						<tbody id="list-cart"></tbody>
					</table>
            
              <form action="{{route('confirm')}}" method="post" id="checkform">
			  			{{ csrf_field() }}	
						<!-- 台灣本島 BEGIN -->
						<div class="customerInfo" id="tw">
							<ul>
								@if(Session::has('is_vip'))
								<li class="payment">
									<div class="pryName"><span class="star">*</span>付款方式</div>
									<div class="paymentCash"><div class="icon"></div><input type="radio" id="twPaymentCash" name="twPaymentInput" value="快遞貨到付款" checked="true"> <label for="twPaymentCash">快遞貨到付款</label></div>		
								</li>
								<li class="infoTitle">收貨人資料</li>
								<li class="same" style="display: none;"><input type="checkbox" checked="true" name="twSameAsPayer"> 同訂購人</li>
								<li class="recipients "><div class="pryName"><span class="star">*</span>收貨人姓名</div>
									<input id="twRpInput" class="recipientsInput" disabled value="{{Session::get('vip_name')}}"  size="10">
									<input type="hidden" name="twRpInput" value="{{Session::get('vip_name')}}" >
								</li>
								<li class="tel "><div class="pryName">聯絡電話</div>
									<input min="0" type="tel" id="twRpTelInput" class="telInput" disabled value="{{Session::get('vip')}}" onkeyup="value=value.replace(/[^\d]/g,'') " size="20">
									<input min="0" name="twRpTelInput" type="hidden" value="{{Session::get('vip')}}">
								</li>
								<li class="address" id="addressR"><div class="pryName"><span class="star">*</span>送貨地址</div>
									<input type="text" name="twRecipientsAddressInput" disabled value="Put your address in the VIP panel" id="twRpAddInput" class="addressInput">
								</li>	
								@else
								<li class="payment">
									<div class="pryName"><span class="star">*</span>付款方式</div>
									<div class="paymentCash"><div class="icon"></div><input type="radio" id="twPaymentCash" name="twPaymentInput" value="快遞貨到付款" checked="true"> <label for="twPaymentCash">快遞貨到付款</label></div>
								</li>
								<li class="infoTitle">收貨人資料</li>
								<li class="same" style="display: none;"><input type="checkbox" checked="true" name="twSameAsPayer"> 同訂購人</li>
								<li class="recipients "><div class="pryName"><span class="star">*</span>收貨人姓名</div>
									<input id="twRpInput" class="recipientsInput" disabled value="{{Session::get('vip_name')}}"  size="10">
									<input type="hidden" name="twRpInput" value="{{Session::get('vip_name')}}" >
								</li>
								<li class="tel "><div class="pryName">聯絡電話</div>
									<input min="0" type="tel" id="twRpTelInput" class="telInput" disabled value="{{Session::get('vip')}}" onkeyup="value=value.replace(/[^\d]/g,'') " size="20">
									<input min="0" name="twRpTelInput" type="hidden" value="{{Session::get('vip')}}">
								</li>
								<!-- <li class="" ><span class="star">*</span>只能輸入數字，不接受數字以外字元，包括+886、-、全形數字、英文、*號其他訊息，輸入有誤本公司將主動刪除訂單並去電通知重訂。</li> -->
								<li class="address" id="addressR"><div class="pryName"><span class="star">*</span>送貨地址</div>
									<input type="text" id="twRpAddInput" class="addressInput" disabled value="Address of {{Session::get('vip_name')}}" />
									<input type="hidden" name="twRecipientsAddressInput" id="twRpAddInput" value="Address of {{Session::get('vip_name')}}" />
									
								</li>
								@endif
	
							</ul>
						</div>
						<!-- 台灣本島 END -->

									<div class="actions">
										<!-- <div class="note"><span class="star">*</span>為必填項目</div> -->
										<!-- <div class="back"><a href="/shopping/toPage?id=cartlist"><div class="icon"></div>上一步，購物清單</a></div> -->
										<input type="submit"  class="finalCheck" value="確認訂單">
										
									</div>
							</form>
						</div>
					</div>
				<div class="row-end">&nbsp;</div>
                

                </div>
            </div>
        </div>				          
	</div>

@endsection
