@extends('desktop.layout')

@section('content')

	<!-- bread crumb 路徑  BEGIN -->
	<div id="breadcrumb"><a href="/">HOME</a> / <a href="#">機能衣</a>: 現在買feGood氣爽衣全系列，2件1000帶回家！</div>
	<!-- bread crumb 路徑  END -->
	<div id="mainContent">
		<div class="layout-978">
			<div class="row">
				<div class="col3">
                    <aside id="sidebar">
                        @include('desktop.sidebar')
                    </aside>	
				</div>
            
				<div id='list' class="col9">
					<div class="description"></div>
					<div class="prdList">
                        <!-- 商品單列  BEGIN -->
                        <div class="row">
                            <product 
                                image="img/product/T0901545_m_1.jpg" 
                                title="女圓領休閒氣爽衣" 
                                number-avail="" 
                                size-avail=""
                                itemlink= "item/"></product>
                            <product 
                            image="img/product/T0901545_m_1.jpg" 
                            title="女圓領休閒氣爽衣" 
                            number-avail="" 
                            size-avail=""
                            itemlink= "item/"></product>
                            <product 
                                image="img/product/T0901545_m_1.jpg" 
                                title="女圓領休閒氣爽衣" 
                                number-avail="" 
                                size-avail=""
                                itemlink= "item/"></product>
                            <div class="row-end"></div>
                        </div>
                        <!-- 商品單列  END -->
                        
					</div>
          <ul class="pagination">
            <li><a href="/filter.ftl?p=1&category=${category}" class="first">第一頁</a></li>
            <li><a href="/filter.ftl?p=${BackPage}&category=${category}" class="previous">上一頁</a></li>
        
            <li><a href="#"class="current">1</a></li>
           
            <li><a href="/filter.ftl?p=${p}&category=${category}">2</a></li>
            <li><a href="filter.ftl?p=${NextPage}&category=${category}" class="next">下一頁</a></li>
            <li><a href="filter.ftl?p=${PageTotal}&category=${category}" class="last">最末頁</a></li>
          </ul>
				</div>
				<div class="row-end">&nbsp;</div>
			</div>
		</div>
	</div>

@endsection

