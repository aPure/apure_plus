@extends('desktop.layout')

@section('content')
<div id="mainContent">
		<div class="layout-978">
			<div class="row">
				<div id='list' class="col12">
			<p>感謝您訂購
			
			您已完成訂購，商品會在2個工作天內配送到府。 &hearts;</p>
			@if($from_sales == 1)
				<p><a href="{{route('admin_index')}}"> 回到首頁。 </a></p>
			@else
				<p><a href="{{route('shop')}}"> 回到首頁。 </a></p>
			@endif
		</div>  
            
      </div>
            
		</div>
	</div>

@endsection
