@extends('desktop.layout')
@section('content')
	<div id="mainContent">
	
		<div class="layout-978">
			<div class="row">
			
				<h2>Seems like you are lost. You can go back to the homepage <a href="{{route('index')}}">here</a></h2>
            
            </div>
		</div>
	</div>

@endsection