@extends('desktop.layout')
@section('content')

	<div id="breadcrumb"><a href="/">HOME</a> / <a href="#" title="${CategoryInfo.Name}">fegood</a> / <a href="#" title="">女圓領休閒氣爽衣</a>
</div>
	<!-- bread crumb 路徑  END -->
	<div id="mainContent">
		<div id="floatBar">
			<!-- 共用側欄 BEGIN -->
			<div class="tag">商品目錄</div>
            <aside id="sidebar">
                @include('desktop.sidebar')
            </aside>
			<!-- 共用側欄 END -->
		</div>
		<div class="layout-978">
			<div class="row">
				<div class="col12">
					<div id="product">
                        <!-- <div style="margin-bottom: 15px;"><a href="../">
                            <img  src="http://www.apure.com.tw/img/banner/event_1027_977x83.jpg')}}" width="980" /></a>
                        </div> -->
                        <div class="photoFrame">
				            <div class="mainPhoto" itemscope itemtype="http://schema.org/Thing">
					            <div style="display:none" itemprop="name"></div>
                                <a href="{{asset('img/product/T09018_1_l_1.jpg')}}"  class="itemPicture" rel="pictures" title>
                                    <img  src="{{asset('img/product/T09018_1_b_1.jpg')}}" title>
                                </a>
              	            </div>
                            <ul class="picZoom thumbs thumbnail">
                                <li class="1"><a href="javascript:void(0);" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_1_b_1.jpg',largeimage: 'img/product/T09018_1_l_1.jpg'}"><img src="{{asset('img/product/T09018_1_s_1.jpg')}}"></a></li>
                                <li class="1"><a href="javascript:void(0);" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_1_b_2.jpg',largeimage: 'img/product/T09018_1_l_2.jpg'}"><img src="{{asset('img/product/T09018_1_s_2.jpg')}}"></a></li>
                                <li class="1"><a href="javascript:void(0);" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_1_b_3.jpg',largeimage: 'img/product/T09018_1_l_3.jpg'}"><img src="{{asset('img/product/T09018_1_s_3.jpg')}}"></a></li>
                                <li class="2"><a href="javascript:void(0);" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_2_b_1.jpg',largeimage: 'img/product/T09018_2_l_1.jpg'}"><img src="{{asset('img/product/T09018_2_s_1.jpg')}}"></a></li>
                            </ul>

						</div>
						<div class="features" itemscope itemtype="http://schema.org/Offer">
							<div class="name" itemprop="name"><h1>女圓領休閒氣爽衣</h1></div>
							<div class="price" itemprop="price">NT$ <strong>1080 </strong></div>
							<div class="promo" itemprop="description">買2件享多彩發熱衣450元加價購</div>
							<div class="colors">
								<div class="pryName"><div class="icon"> </div>顏色</div>
								<ul class="picZoom thumbs colorsList">
                                    <li><a href="javascript:void(0);" id="1" class="color" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_1_b_1.jpg',largeimage: 'img/product/T09018_1_l_1.jpg'}"><img src="{{asset('img/color/1.jpg')}}" /></a></li>
                                    <li><a href="javascript:void(0);" id="2" class="color" rel="{gallery: 'pictures', smallimage: 'img/product/T09018_2_b_1.jpg',largeimage: 'img/product/T09018_2_l_1.jpg'}"><img src="{{asset('img/color/2.jpg')}}" /></a></li>
                                </ul>
							</div>
							<!-- <div class="ruler"><a href="/img/product/${StyleInfo.SizeinfoPicture}" class="example-image-link" data-lightbox="example-1"><img src="/img/ruler.png"></a></div> -->
							<div class="summary">

                                <div style="font-size:16px; color:#FF0004; font-weight:bolder; line-height:2em;">feGood氣爽衣雙11普天同慶特價：2件$900</div>
                                <br />
                               
                                    穿上feGood氣爽衣，立即解決六種負擔：
                                    <ol>
                                    <li> 解決大量汗水、衣服殘留異味問題</li>
                                    <li>抑制壞菌數、解決肌膚過敏問題</li>
                                    <li>隔離強烈紫外線，避免肌膚曬傷黑色素沉澱問題</li>
                                    <li>擺脫肌膚乾燥，解決接觸衣服的摩擦感</li>
                                    <li>速乾排汗設計、擺脫衣服黏背不舒適感</li>
                                    <li>滋潤肌膚，解決靜電問題</li>
                                    <li>想要解決問題，選擇穿上feGood氣爽衣，改變你會感覺到！</li>
                                </ol>
                                <div style="font-size:10px; color:#999;">
                                    產品編號：T0901545
                                </div>
							</div>
							<div class="share">
                                <div class="fb-share-button" data-href="http://www.apure.com.tw/product.ftl?styleCode=${styleCode}&colorCode=${colorCode}" data-width="120" data-type="button_count"></div>
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="order">
                        <div class="actions"><a class="addToCart add-to-cart" href="#" data-id={{$id}}></a><a class="buyNow add-to-cart" data-id={{$id}} href="checkout/"></a></div>
                            </div>
						          
		</div>
@endsection
