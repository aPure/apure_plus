 <!-- 共用頁腳 BEGIN -->
 <footer id="mainFooter">
                <nav id="footNav">
                <ul>
                    <li class="topic">商品總覽</li>
                    <li><span style="color:#888889">PureSocks</span><a href="/sort.ftl?category=puresocks" title="除臭襪">除臭襪</a></li>
                    <li><span style="color:#888889">Pure5.5酸鹼平衡</span><a href="/promo_pure55.html" title="內褲">內褲</a></li>
                    <li><span style="color:#888889">feGood氣爽</span><a href="/fegood/index.html" title="排汗衣">吸濕排汗衣</a></li>
                    <li><span style="color:#888889">feHot</span><a href="/feHot/index.html" title="發熱衣">發熱衣</a></li>
                    <li><a href="/promo_cotton.html" title="有機棉">aPure純棉天然衣</a></li>
                    <li><a href="/promo_puresun.html" title="POLO衫">PureSun保潔POLO衫</a></li>
                    </ul>
                <div class="spacer"> </div>
                <ul>
                    <li class="topic">關於aPure</li>
                    <li><a href="/brand.html" title="富達康科技有限公司">品牌故事</a></li>
                    <li><a href="http://www.104.com.tw/jobbank/custjob/index.php?r=cust&j=4470426e5636402230323c1d1d1d1d5f2443a363189j52&jobsource=n104bank1" target="_blank">人才招募</a></li>
                    <li><a href="https://www.youtube.com/playlist?list=PL0HkXfq922GrJBmuenVM8IOSGo5T3lw2q" target="_blank">電視廣告</a></li>
                    <li><a href="https://www.youtube.com/playlist?list=PL0HkXfq922GoJWxWiPuB4bBFFIvM75otu" target="_blank">新聞報導</a></li>
                    <!--
                    <li><a href="/media.html">Media</a></li>
                    <li><a href="/model.html">素人麻豆招募</a></li>
                    <li><a href="/eventsinstore.html">店鋪活動</a></li>
                    -->
                    <li><a href="/promise.html">服務承諾</a></li>
                </ul>
                <div class="spacer"> </div>
                <ul>
                    <li class="topic">客服中心</li>
                    <li><a href="/howto.html" title="購物說明">購物說明</a></li>
                    <li><a href="/delivery.html" title="運費說明">運費說明</a></li>
                    <li><a href="/faq.html" title="常見問題">常見問題Q &amp; A</a></li>
                    <!--
                    <li><a href="/howto.html">購物流程</a></li>
                    <li><a href="/welfare.html">會員福利</a></li>
                    <li><a href="/return.html">退換貨流程</a></li>
                    <li><a href="/buyoversea.html">海外購買</a></li>
                    -->
                    <li><a href="/privacy.html" title="隱私權保護">隱私權保護政策</a></li>
                    <li><a href="/interests.html" title="會員權益">會員權益</a></li>
                </ul>
                <!-- div class="spacer"> </div>
                <ul>
                    <li class="topic">機能性纖維</li>
                    <li><a href="/fibers.html" title="台灣機能性紡織品">什麼是機能性纖維</a></li>
                </ul -->
                <div class="spacer"> </div>
                <div class="brand"> </div>
                <div style="float:left; font-size: 12px; padding-left: 30px;">免責聲明：<span style="color:#666">本站證言皆為aPure商品購買者使用經驗與感想，不代表aPure本站之言論。</span></div>
            </nav>
            </footer>
            <!-- 共用頁腳 END -->