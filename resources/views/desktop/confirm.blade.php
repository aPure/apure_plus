@extends('desktop.layout')
@section('content')

	<div id="mainContent">
		<div class="layout-978">
			<div class="row">
				<div class="col12">
                <div id="fillout">
					<div class="checkoutFlow"> </div>
					<div class="sellInfo"><div>

					<table class="productList">
						<thead>
							<tr>
								<td>商品</td>
								<td>價格(NT)</td>
								<td>數量</td>
								<td>總價(NT)</td>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<td colspan="3" align="right">購買金額</td>
								<td >NT <b id="order-total"></b></td>
							</tr>
						</tfoot>
						<tbody id="list-cart"></tbody>
					</table>
            
              <form id="confirm_checkform" action="{{route('checkout')}}" method="post" >
			  			{{ csrf_field() }}
			  			<input type="hidden" name="name" value="{{$name}}" />
                        <input type="hidden" name="tel" value="{{$tel}}" />
                        <input type="hidden" name="address" value="{{$address}}" />
						<input type="hidden" name="payment" value="{{$payment}}" />
						<!-- <input type="hidden" name="delivery" value="" /> -->
						<input type="hidden" name="order" value="" />
						<input type="hidden" name="total_amount" value="" />
						<input type="hidden" name="mobile" value="0" />
                        <div id=""></div>
						<!-- 台灣本島 BEGIN -->
						<div class="customerInfo" id="tw">
							
                            <table class="sellDetail">
								<thead>
									<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
								</thead>
								<tbody>
									<tr>
										<td class="pryName">收貨人姓名</td>
										<td> {{$name}} </td>
										<td class="pryName">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td class="pryName">行動電話</td>
										<td>{{$tel}}</td>
                                    </tr>
                                    
									<tr>
										<td class="pryName">送貨地址</td>
										<td>{{$address}}</td>
										<td class="pryName">&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
                
									<tr>
										<td class="pryName">付款方式</td>
										<td>{{$payment}}</td>
									</tr>
									<!-- <tr>
										<td class="pryName">取貨時間</td>
										<td></td>
									</tr> -->
									<!-- <tr>
										<td class="pryName">VIP Customer</td>
										<td>{{$vip}}</td>
									</tr> -->
								</tbody>
							</table>

						</div>
						<!-- 台灣本島 END -->
						<!-- 外島地區 BEGIN -->
						<div class="customerInfo" id="outIsland" style="display: none;">
							<ul>
								<!-- <li class="payment">
									<div class="pryName"><span class="star">*</span>付款方式</div>
									<div class="paymentCash"><div class="icon"></div><input type="radio" id="olPaymentCash" name="olPaymentInput" value="cash"> <label for="olPaymentCash">快遞貨到付款</label></div>
									<div class="paymentCard"><div class="icon"></div><input type="radio" id="olPaymentCard" name="olPaymentInput" value="card"> <label for="olPaymentCard">信用卡線上付款（使用中國信託線上金流系統，交易安全有保障）</label></div>
								</li> -->
								<li class="infoTitle">訂購人資料</li>
                <li class="email"><div class="pryName"><span class="star">*</span>email帳號</div><input name="olEmailInput" id="olEmailInput" class="emailInput" type="email" value="${olEmailInput}" readonly></li>
								<li class="payer"><div class="pryName"><span class="star">*</span>訂購人姓名 </div><input name="olPayerInput" id="olPayerInput" class="payerInput" type="text" value="${olPayerInput}"></li>
								<li class="sex"><div class="pryName">稱謂</div><input type="radio" name="olSexInput" id="olFemale" value="0" <#if (olSexInput=="0")>checked</#if>> <label for="olFemale">小姐</label> <input type="radio" name="olSexInput" id="olMale" value="1" <#if (olSexInput=="1")>checked</#if>> <label for="olMale">先生</label></li>

								<li class="mobile"><div class="pryName"><span class="star">*</span>行動電話</div><input name="olMobileInput" id="olMobileInput" class="mobileInput" type="tel" onkeyup="value=value.replace(/[^\d]/g,'') " value="${olMobileInput}"></li>
								<li class="tel"><div class="pryName">聯絡電話</div><input type="tel" name="olTelInput" id="olTelInput" class="telInput" value="${olTelInput}" onkeyup="value=value.replace(/[^\d]/g,'') " size="20"> <input type="checkbox" name="olSameAsMobile" onclick="sameAsMobile(this,'${olTelInput}')"> 同行動電話</li>
								<li class="postzip">
                  					<span id="homeAddressArea_ol">
										<div class="pryName"><span class="star">*</span>發票寄送郵遞區號</div>
										<select id="olCity" name="olCity" onchange="" size="1">
											<#--<option value="">請選擇縣市別</option>-->
										</select>
										<select id="olArea" name="olArea" onchange="" size="1">
											<#--<option value="">請選擇鄉鎮區別</option>-->
										</select>
										<input id="olPostzipInput" name="olPostzipInput" readonly="readonly" size="7"/>
									</span>
								</li>
								<li class="address"><div class="pryName"><span class="star">*</span>發票寄送地址</div><input name="olAddressInput" id="olAddressInput" class="addressInput" value="${olAddressInput}"></li>
								
								<li class="infoTitle">收貨人資料</li>
								<li class="same"><input type="checkbox" name="olSameAsPayer" onclick="sameAddress(this,'${invoiceTitle}','${uniqueID}')"> 同訂購人</li>
								<li class="recipients"><div class="pryName"><span class="star">*</span>收貨人姓名</div><input name="olRpInput" id="olRpInput" class="recipientsInput" value="${olRpInput}" size="10"></li>
								<li class="sex"><div class="pryName">稱謂</div><input type="radio" name="olRpSexInput" id="olRpFemale" value="0" <#if (olRpSexInput=="0")>checked</#if>> <label for="olRpFemale">小姐</label> <input type="radio" name="olRpSexInput" id="olRpMale" value="1" <#if (olRpSexInput=="0")>checked</#if>> <label for="olRpMale">先生</label></li>
								<li class="tel"><div class="pryName">聯絡電話</div><input type="tel" name="olRpTelInput" id="olRpTelInput" class="telInput" onkeyup="value=value.replace(/[^\d]/g,'') " value="${olRpTelInput}" size="20"></li>
								<li class="postzip">
                  				<span id="homeAddressAreaR_ol">
									<div class="pryName"><span class="star">*</span>郵遞區號</div>
									<select id="olRpCity" name="olRpCity" onchange="" size="1">
										<#--<option value="">請選擇縣市別</option>-->
									</select>
									<select id="olRpArea" name="olRpArea" onchange="" size="1">
										<#--<option value="">請選擇鄉鎮區別</option>-->
									</select>
									<input id="olRpPostzipInput" name="olRpPostzipInput" readonly="readonly" size="7" />
                  				</span>
								</li>
								<li class="address"><div class="pryName"><span class="star">*</span>送貨地址</div><input name="olRecipientsAddressInput" id="olRecipientsAddressInput" class="addressInput" value="${olRecipientsAddressInput}"></li>
								<li class="infoTitle">其他資訊</li>
								<li class="bid"><div class="pryName">統一編號</div><input name="olBidInput" id="olBidInput" class="bidInput" value="${olBidInput}"></li>
								<li class="title"><div class="pryName">統編抬頭</div><input name="olTitleInput" id="olTitleInput" class="titleInput" value="${olTitleInput}"></li>
								<li class="message"><a name="m1"></a><div class="pryName">備註欄</div><textarea name="olMessageText" id="olMessageText" class="messageText">${olMessageText} </textarea></li>
							</ul>
						</div>
						<!-- 外島地區 END -->
						<!-- 國外 BEGIN -->
						<div class="customerInfo" id="eastSouthAsia" style="display: none;">
							<ul>
								<!-- <li class="payment">
									<div class="pryName"><span class="star">*</span>付款方式</div>
									<div class="paymentCard"><div class="icon"></div><input type="radio" id="esPaymentCard" name="esPaymentInput" value="card" checked="checked"> <label for="esPaymentCard">信用卡線上付款（使用中國信託線上金流系統，交易安全有保障）</label></div>
								</li> -->
								<li class="infoTitle">訂購人資料</li>
								<li class="payer"><div class="pryName"><span class="star">*</span>訂購人姓名 </div><input name="esPayerInput" id="esPayerInput" class="payerInput" type="text" value="${esPayerInput}"></li>
								<li class="sex"><div class="pryName">稱謂</div><input type="radio" name="esSexInput" id="esFemale" <#if (esSexInput=="0")>checked</#if>> <label for="esFemale">小姐</label> <input type="radio" name="esSexInput" id="esMale" value="1" <#if (esSexInput=="0")>checked</#if>> <label for="esMale">先生</label></li>
								<li class="email"><div class="pryName"><span class="star">*</span>email帳號</div><input name="esEmailInput" id="esEmailInput" class="emailInput" type="email" value="${esEmailInput}" readonly></li>
								<li class="mobile"><div class="pryName"><span class="star">*</span>行動電話</div><input min="0" name="esMobileInput" id="esMobileInput" onkeyup="value=value.replace(/[^\d]/g,'') " class="mobileInput" type="tel" value="${esMobileInput}"></li>
								<li class="tel"><div class="pryName">聯絡電話</div><input min="0" type="tel" name="esTelInput" id="esTelInput" class="telInput" onkeyup="value=value.replace(/[^\d]/g,'') " value="${esTelInput}" size="20"> <input type="checkbox" name="esSameAsMobile"> 同行動電話</li>
								<li class="postzip">
								<span id="homeAddressArea_es">
								<div class="pryName"><span class="star">*</span>發票寄送郵遞區號</div>
								<select id="esCity" name="esCity" onchange="" size="1">
									<option value="">請選擇縣市別</option>
								</select>
								<select id="esArea" name="esArea" onchange="" size="1">
									<option value="">請選擇鄉鎮區別</option>
								</select>
								<input id="esPostzipInput" name="esPostzipInput" class="postzipInput" size="7" value="${esPostzipInput}" />
								</span>
											(海外顧客郵遞區號請隨意填寫，購物明細隨貨寄出)</li>
											<li class="address"><div class="pryName"><span class="star">*</span>發票寄送地址</div><input name="esAddressInput" id="esAddressInput" class="addressInput" value="${esAddressInput}"></li>
											
											<li class="infoTitle">收貨人資料</li>
											<li class="same"><input type="checkbox" name="esSameAsPayer" onclick="sameAddress(this,'${invoiceTitle}','${uniqueID}')"> 同訂購人</li>
											<li class="recipients"><div class="pryName"><span class="star">*</span>收貨人姓名</div><input name="esRpInput" id="esRpInput" class="recipientsInput"  size="10" value="${esRpInput}"></li>
											<li class="sex"><div class="pryName">稱謂</div><input type="radio" name="esRpSexInput" id="esRpFemale" checked="checked"> <label for="esRpFemale">小姐</label> <input type="radio" name="esRpSexInput" id="esRpMale"> <label for="esRpMale">先生</label></li>
											<li class="tel"><div class="pryName">聯絡電話</div><input type="tel" min="0" name="esRpTelInput" id="esRpTelInput" class="telInput" onkeyup="value=value.replace(/[^\d]/g,'') " value="${esRpTelInput}" size="20"></li>
											<li class="postzip">
												<div class="pryName"><span class="star">*</span>郵遞區號</div>
												<input id="esRpPostzipInput" name="esRpPostzipInput" class="postzipInput" size="7" value="${esRpPostzipInput}" />
											</li>
											<li class="address"><div class="pryName"><span class="star">*</span>送貨地址</div><input name="esRecipientsAddressInput" id="esRecipientsAddressInput" class="addressInput" value="${esRecipientsAddressInput}"></li>
											<li class="infoTitle">其他資訊</li>
											<li class="bid"><div class="pryName">統一編號</div><input name="esBidInput" id="esBidInput" class="bidInput" value="${esBidInput}"></li>
											<li class="title"><div class="pryName">統編抬頭</div><input name="esTitleInput" id="esTitleInput" class="titleInput" value="${esTitleInput}"></li>
											<li class="message"><a name="m1"></a><div class="pryName">備註欄</div><textarea name="esMessageText" id="esMessageText" class="messageText">${esMessageText} </textarea></li>
										</ul>
									</div>
									<!-- 國外 END -->
									<!-- <div class="sync">
										<div class="syncData"><input type="checkbox" name="syncAllow" checked="checked" value="syncAllow"> 同步更新訂購人會員資料</div>
									</div> 
									<div class="requirement">
										<div class="term"><span class="star">*</span><input type="checkbox" name="termAllow"> 我同意 <a href="/term.html?ajax=true" target="_blank">aPure 消費者隱私條款</a></div>
										 未登入會員者顯示設定密碼欄位 
									</div>-->

									<div class="actions">
										<!--  <div class="note"><span class="star">*</span>為必填項目</div>
										<div class="back"><a href="/shopping/toPage?id=cartlist"><div class="icon"></div>上一步，購物清單</a></div> -->
										<input type="button"  class="confirm_finalCheck" value="確認訂單">
										
									</div>
            					</form>
							</div>
						</div>
                	<div class="row-end">&nbsp;</div>
                

                </div>
            </div>
        </div>				          
	</div>
@endsection
