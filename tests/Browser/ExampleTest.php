<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testRoutesWithoutLogin(){
        $this->browse(function (Browser $browser) {
            //1
            $browser->visit('/')
                    ->assertSee('歡迎光臨aPure購物系統')
                    ->assertSee('登入');
            //2
            $browser->visit('/login')
                    ->assertSee('歡迎光臨aPure購物系統')
                    ->assertSee('登入');
            //3        
            $browser->visit('/special_current_customers')
                    ->assertSee('歡迎光臨aPure購物系統')
                    ->assertSee('登入');
            //4        
            $browser->visit('/order')
                    ->assertSee('歡迎光臨aPure購物系統')
                    ->assertSee('登入');
            //5
            $browser->visit('/sales')
                    ->assertSee('aPure Sales Manager')
                    ->assertSee('登入');
            //6
            $browser->visit('/sales/vip_payments')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            //7
            $browser->visit('/sales/vip_checkout')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            //8    
            $browser->visit('/sales/order_history')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            //9    
            $browser->visit('/sales/new_deal')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            //10    
            $browser->visit('/sales/renew')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            //11    
            $browser->visit('/sales/addvip')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');
            
            //12    
            $browser->visit('/sales/order_history/{order_details}')
                ->assertSee('aPure Sales Manager')
                ->assertSee('登入');

            //13
            //$browser->visit('/lost')
              //   ->assertSee('Seems like you are lost. You can go back to the homepage here');
        
        });
    }

    // public function testRoutesWithLogin(){
        // $browser->visit('/sales/vip_checkout_to_ctbk')
        // ->assertSee('aPure Sales Manager')
        // ->assertSee('登入');
    // }
}
