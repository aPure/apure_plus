## aPure shop
It is developed under Laravel and has two interfaces: mobile and desktop

## Features
It is written in the format <Type of user(default is User)>, can <What the user can do(required)>, to <Reason to perform the action(optional)>
*User can Login to See Products List 
*User can Select Product size
*User can Add/Remove Products In/From Shopping Cart
*User can Reach Chinatrust Web URL for the payment
*User can Logout 
*User can Select Product number (Mobile Only)
*Sales Admin can Login (Desktop Only)
*Sales Admin can Logout (Desktop Only)
*Sales Admin can List his VIP Customers (Desktop Only)
*Sales Admin can Add new VIP Customers (Desktop Only)
*Sales Admin can Remove VIP Customers (Desktop Only)
*VIP Customer can Add other Customers (Desktop Only) -- maximum 10 customers
*VIP Customer can Remove other Customers (Desktop Only)
*Sales and VIP Customer can Change password (Desktop Only)
*Sales can Create new group buy deal (Desktop Only)
*Sales can Delete group buy deal (Desktop Only)
*Sales can List all group deal he created (Desktop Only)
*Sales can activate or disactivate a group buy deal he created (Desktop Only)
*Customer can Login for purchase when group buy deal is active or period is appropriate
*Customer can Logout
*VIP Customer can List orders history of his attached Customers (Desktop Only)
*VIP can make payment from admin panel (Desktop Only)

## Versoning
Format for versioning is vx.y.z
x: Change of orientation, of technology, of objectif; or A lot of new features;
y: Addition/Improvement of features; or Change of interface design; or Improvement of user interface/experience; 
z: Fixes of bugs; Refactoring;

## Testing