<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* 
  Display pages
*/
//Route::get('/gee', 'ProductForMobileController@index');
//Route::get('/testemail', 'MailController@goal_reached');

Route::get('/', function(){
  return view('welcome');
})->name('index');

Route::get('/sales', 'AdminController@index')->name('admin_index');

Route::get('/sales/addvip', 'AdminController@addvip')->name('admin_addvip');

Route::get('/sales/renew', 'AdminController@renew')->name('renew');

Route::get('/sales/new_deal', 'AdminController@new_deal')->name('new_deal');

Route::get('/sales/order_history', 'AdminController@order_history')->name('order_history');

Route::get('/sales/vip_checkout', 'AdminController@vip_checkout')->name('vip_checkout');

Route::get('/sales/vip_payments', 'AdminController@vip_payments')->name('vip_payments');

Route::get('/sales/vip_payments/{paymentOrderNumber}', 'AdminController@paymentOrder')->name('paymentOrder');

Route::get('/sales/order_history/{buyerId}', 'AdminController@order_details')->name('order_details');

Route::get('/sales/removeOrderItem/', 'AdminController@remove_order_item')->name('remove_order_item');

Route::get('/sales/print/{paymentId}/{vip}', 'AdminController@print')->name('print');

//Route::get('/fegood', 'ProductController@fegood')->name('fegood');

//Route::get('/item/{item_id}', 'ProductController@item')->name('item');

Route::get('/special_current_customers', 'SpecialCustomersProductsController@index')->name('shop');

Route::get('/order', 'SpecialCustomersProductsController@order')->name('order');

Route::get('/login', function(){
    return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
})->name('login');

Route::get('/lost', function (){
    return view('desktop.lost', ['isDesktop' => 1]);
})->name('lost');

// Route::get('/thankyou', function () {
//     return view('desktop.thankyou');
// })->name('thankyou');


// Route::get('/testpay', function(){
//     return view('desktop.form2bank');
// });

/* 
  Data Processing before display
*/

Route::get('/mobile_progress_bar', 'AdminController@mobile_progress_bar')->name('mobile_progress_bar');

Route::get('/mobile_data_fetch', 'AdminController@mobile_data_fetch')->name('mobile_data_fetch');

Route::get('/sales/removevip', 'AdminController@removevip')->name('removevip');

Route::get('/sales/group_buy', 'AdminController@group_buy_list')->name('group_buy');

Route::get('/sales/change_deal_activated', 'AdminController@change_deal_activated')->name('change_deal_activated');

Route::get('/sales/remove_group_deal_vip', 'AdminController@remove_group_deal_vip')->name('remove_group_deal_vip');

Route::get('/logout', 'SpecialCustomersProductsController@logout')->name('logout');

Route::get('/mobile', 'SpecialCustomersProductsController@checkout');

Route::get('/mobile/renew', 'AdminController@renewpass')->name('renewpassmobile');

Route::post('/sales/vip_checkout_to_ctbk', 'AdminController@vip_checkout_to_ctbk')->name('vip_checkout_to_ctbk');

Route::post('/sales/addvip', 'AdminController@submitnewvip')->name('submitnewvip');

Route::post('/sales/add_group_buyers', 'AdminController@add_group_buyers')->name('add_group_buyers');

Route::post('/sales/renew', 'AdminController@renewpass')->name('renewpass');

Route::post('/sales/new_deal', 'AdminController@create_new_deal')->name('create_new_deal');

Route::post('/confirm', 'SpecialCustomersProductsController@confirm')->name('confirm');

Route::post('/checkout', 'SpecialCustomersProductsController@checkout')->name('checkout');

Route::post('/login', 'SpecialCustomersProductsController@login');

Route::get('/sales/confirm_vip', 'AdminController@confirm_vip')->name('confirm_vip');

Route::get('/sales/group_deal_id/{vip}', 'WorkerController@group_deal_id');

Route::get('/sales/excel_summary', 'AdminController@download_excel_summary')->name('excel_summary');
Route::get('/sales/list_customers', 'AdminController@download_list_customers')->name('list_customers');
// Resources
//Route::resource('users','SpecialCustomersProductsController');