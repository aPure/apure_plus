<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Laravel Pdf',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('../temp/'),
	'font_path' => base_path('resources/fonts'),
    'font_data' => [
        'chinese' => [
            'R'  => 'SourceHanSerifTC-Regular.ttf',    // regular font
            'B'  => 'SourceHanSerifTC-Bold.ttf',       // optional: bold font
            'I'  => 'SourceHanSerifTC-Regular.ttf',     // optional: italic font
            'BI' => 'SourceHanSerifTC-Bold.ttf', // optional: bold-italic font
            'useOTL' => 0x82,    
            'useKashida' => 75, 
		]
	]
];
