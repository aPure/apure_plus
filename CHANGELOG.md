##v1.6.2
**Feature
*Sales manager can download list of all customers 

##v1.6.1 (August 10th 2018)
**Feature
*When goal is reached, VIP receives a mail from customer service
**Bug Fix
*When creating new deal, the group buyer with new id was not created. Now solved

##v1.6 (August 1rst 2018)
**Features
*VIP add customers only for specific group deal
**Bug Fixes
*Some customers could not send purchase to server (buy)

##v1.5 (May 24th 2018)
**Features
*Sales manager can select products for group buy and the discount for each group of product
*VIP can delete orders of customers under him
*Sales manager can print pdf of payments
**Bug Fix
*XL size problem fixed

##v1.4.1 (March 23rd 2018)
**Bug Fixes
*New deal creation require discount input - On UI it was not set to be required
*VIP Customer has to be confirmed as VIP by root before he can login -- Check has been made
*Login on mobile server set to window location of the browser otherwise could not login on production site

##v1.4.0 (March 8th 2018)
**Feature
*Super admin can confirm VIP Customers before they have access to plateform 

##v1.3.4 (February 7th 2018)
**Improvement
*Sales manager create VIP make VIP group buyer automatically

##v1.3.3 (February 7th 2018)
**Bug Fix
*Customer checkout was not working -- DB insert Error

##v1.3.2 (February 6th 2018)
**Improvements
*Added field Discount to the creation of a new deal
*Customer can Login for purchase when group buy deal is active or period is appropriate - Done for mobile part now
*VIP Customers can see each order details

** Bug Fixes
*Added hours, minutes, seconds in add new deal datetime
*Added control to check if there is group deal and return the appropriate list of customers
*Customer on Mobile could not login, now it's ok I guess
*Customer logout but Price on top of mobile was not updated to zero, it's ok
*Mobile Back button was not working well for confirm page
*Added robustness because there were errors when there was no deal created by Sales 

##v1.3.1 (January 26th 2018)
**Feature
*VIP can make payment from admin panel (Desktop Only)

**Improvements
*When adding new co-buyers, when click on submit and there is no input, show laravel error. Just had to make it robust for this part
*VIP Customer can add maximum 10 other Customers under him
*If not logged in, cannot read page order 
*Order History UI with more info
*VIP Checkout checks if can proceed  

##v1.2.1 (January 22nd 2018)
**Features
*Sales Admin can Login (Desktop Only)
*Sales Admin can Logout (Desktop Only)
*Sales Admin can List his VIP Customers (Desktop Only)
*Sales Admin can Add new VIP Customers (Desktop Only)
*Sales Admin can Remove VIP Customers (Desktop Only)
*VIP Customer can Add other Customers (Desktop Only)
*VIP Customer can Remove other Customers (Desktop Only)
*Sales and VIP Customer can Change password (Desktop Only)
*Sales can Create new group buy deal (Desktop Only)
*Sales can Delete group buy deal (Desktop Only)
*Sales can List all group deal he created (Desktop Only)
*Sales can activate or disactivate a group buy deal he created (Desktop Only)
*Customer can Login for purchase when group buy deal is active or period is appropriate (Desktop Only)
*Customer can Logout (Desktop Only)
*VIP Customer can List orders history of his attached Customers(Desktop Only)

**Removal
*Customer do not need to check "agree terms" before checking out to buy products

**Bug Fixes
*When wrong user login or correct user put wrong password on desktop, error message on javascript is working now
*Sales Logout was using VIP Customers logout session, which was wrong
*Session was phone, I am changing to username

##v1.1.3 (January 2nd 2018)
**Improvement
*User can Reach Chinatrust Web URL for the payment

##v1.1.2 (December 22nd 2017)
**Improvement
*User can select delivery period preference when ordering

##v1.1.1 (December 21rst 2017)
**Bug Fix
*Android navbar can work like iPhone navbar

##v1.1.0 (December 15th 2017)
**Feature
*User can Select Product number (Mobile Only)

##v1.0.0 (December 14th 2017)
**Features
*User can Login to See Products List 
*User can Add/Remove Products In/From Shopping Cart
*User can Select Product size
*User can Pay the Amount of Cart via Chinatrust API (Desktop only)
*User can Logout 