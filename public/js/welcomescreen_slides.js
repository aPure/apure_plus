var welcomescreen_slides = [
    {
      id: 'slide0',
      picture: '<img src="img/welcome.jpg" />',
      text: 'Welcome to aPure <br/> <div class="loader"><img style="width:100%;height:100%;" src="img/loader.gif" /></div>'
    }
  ];

  var tutorial_slides =[
    {
      id: 'slide0',
      picture: '<img src="http://www.apure.com.tw/img/product/groupbuy/tutorial/1.jpg" />',
      text: ''
    },
    {
      id: 'slide1',
      picture: '<img src="http://www.apure.com.tw/img/product/groupbuy/tutorial/2.jpg" />',
      text: ''
    },
    {
      id: 'slide2',
      picture: '<img src="http://www.apure.com.tw/img/product/groupbuy/tutorial/3.jpg" />',
      text: ''
    }
  ];

  var options = {
    'bgcolor': '#8ABA2A',
    'fontcolor': '#fff',
    'pagination': false
  }

  var tutorial_options = {
    'bgcolor': '#8ABA2A',
    'fontcolor': '#fff',
    'pagination': true,
    'closeButtonText': 'Close',
    'closeButton': true
  }

  var welcomescreen = new Welcomescreen(welcomescreen_slides, options);

  var tutorialscreen = new Welcomescreen(tutorial_slides, tutorial_options);
