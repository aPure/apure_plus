var carlistOldAmount;
$(document).ready(function(){
  //$("div.itemStocks").hide();
  $("select[name=amount]").click(function() {
    carlistOldAmount = $(this).val();
  });
});
function selectSizeFromListPage(goodsSerNo,row) {
  $("input[name=goodsSerNo]").eq(row).val("");
  $("select[name=amount]").eq(row).empty();
  $("div#stockWord").eq(row).hide();
  var _postData = {clazz:"com.flaps.web.ajax.GetLastAmountByGoodsSerNo",goodsSerNo:goodsSerNo};
  $.ajax({
    type: "POST",
    url: "/ajax.json",
    data: _postData,
    cache: false,
    dataType: "json",
    error: function (xhr){
      //alert('Ajax request 發生錯誤');
    },
    success: function (response){
      if(typeof(response[0].LastAmount) != "undefined") {
        $("input[name=goodsSerNo]").eq(row).val(goodsSerNo);
        if(response[0].LastAmount.length > 0){
          $("select[name=amount]").eq(row).show();
          var lastAmount=parseInt(response[0].LastAmount);
          for(var i=1; i<=lastAmount && i<=10; i++)
            $("select[name=amount]").eq(row).append('<option value="'+i+'">'+i+'</option>');
          if(lastAmount>20)
            $("div.itemStocks").eq(row).text('庫存 - 足夠');
          else if(lastAmount>0)
            $("div.itemStocks").eq(row).text('只剩 '+lastAmount+' 件');
          else  {
            $("div.itemStocks").eq(row).text('該尺寸無庫存，請選擇其他尺寸');
            $("select[name=amount]").eq(row).append('<option>無</option>');
            $("select[name=amount]").eq(row).hide();
          }
        }else {
          $("select[name=amount]").eq(row).hide();
          $("div.itemStocks").eq(row).text('該尺寸無庫存，請選擇其他尺寸');
        }
      }
    },
    async: true
  });
}
function addToCartFromList(row) {
  var serNo = $("input[name=goodsSerNo]").eq(row).val();
  if(serNo.length ==0) {
    alert("您尚未選擇尺寸");
    return;
  }
  var amount = FN($("select[name=amount]").eq(row).val(),0);
  if(amount == 0){
    alert("您尚未選擇數量");
    return;
  }
  var _postData = {clazz:"com.flaps.web.ajax.AddGoodsToShoppingCar",serNo:serNo,amount:amount};
  $.ajax({
    type: "POST",
    url: "/ajax.json",
    data: _postData,
    cache: false,
    dataType: "json",
    error: function (xhr){
      //alert('Ajax request 發生錯誤');
    },
    success: function (response){
      setCartInfo(response);
      miniCartUnfold();
      miniCartFold();
    },
    async: true
  });
}
function deleteCar(row,goodsSerNo,total,mainGoodsStyleCode){
  var _postData={clazz: "com.flaps.web.ajax.RemoveGoodsFromShoppingCar",serNo: goodsSerNo,mainGoodsStyleCode: mainGoodsStyleCode};
  $.ajax({
    type: "POST",
    url: "/ajax.json",
    data: _postData,
    cache: false,
    dataType: "json",
    error: function (xhr){
      //alert('Ajax request 發生錯誤');
    },
    success: function (response){
      setCartInfo(response);
      miniCartUnfold();
//      $(row).parent().remove();
//      $("#miniCart header div.total").text(DCZ(FN($("#miniCart header div.total").text(),0)-FN(total,0),0));
//      if(response.length>0 && typeof(response[0].promotionName) != "undefined"){
//        setPromotionName(response[0].promotionName);
//      }else
//        setPromotionName(null);
    },
    async: true
  });
}

function setPromotionName(promotionName) {
  var inform=$("div.miniCartInform ul.inform");
  $(inform).empty();
  if(promotionName != null){
    $(promotionName).each(function (i){//arrays
      $(inform).append('<li>'+promotionName[i].PromotionName+'</li>');
    });
  }
}
function deleteGoods(elem,goodsSerNo,mainGoodsStyleCode){
  if($("div.cNum").text().length >0) {
    alert('請先取消折價劵');
    return;
  }
  var _postData={clazz: "com.flaps.web.ajax.RemoveGoodsFromShoppingCar",serNo: goodsSerNo,mainGoodsStyleCode: mainGoodsStyleCode};
  $.ajax({
    type: "POST",
    url: "/ajax.json",
    data: _postData,
    cache: false,
    dataType: "json",
    error: function (xhr){
      //alert('Ajax request 發生錯誤');
    },
    success: function (response){
      $(elem).parent().parent().remove();
      //alert(hasCarList())
      if(!hasCarList()) {
        alert('購物清單中,無任何商品');
        location.replace('/');
      }else
        location.reload();//因為數量促銷原因
    },
    async: true
  });
}

function updateToCart(elem,goodsSerNo,mainGoodsStyleCode) {
  if($("div.cNum").text().length >0) {
    alert('請先取消折價劵');
    elem.value = carlistOldAmount;
    return;
  }
  if(goodsSerNo.length ==0) return;
  elem.disabled = true;
  var _postData={clazz: "com.flaps.web.ajax.UpdateGoodsShoppingCar",serNo: goodsSerNo,amount:elem.value,mainGoodsStyleCode: mainGoodsStyleCode};
  $.ajax({
    type: "POST",
    url: "/ajax.json",
    data: _postData,
    cache: false,
    dataType: "json",
    error: function (xhr){
      //alert('Ajax request 發生錯誤');
    },
    success: function (response){
      location.reload();
    },
    async: true
  });
}

function addToCart(checkOut) {
  var amountVal="";
  var serNoVal="";
  var serNo=$("input[name=goodsSerNo_picked]");
  $("select[name=amount_picked]").each(function(i){
        if(Number(this.value) > 0) {
          amountVal +="&amount="+this.value;
          serNoVal +="&serNo="+$(serNo).eq(i).val();
        }
      }
  );
  if(amountVal.length > 0){
    var addPriceAmountVal="";
    var addPriceSerNoVal="";
    var addPriceUnitVal="";
    var groupAddPrice=$("input[name=groupAddPrice]");
    $(groupAddPrice).each(function (i){
      if($(groupAddPrice).eq(i).attr("checked")){
        var addPriceSerNo=$("input[name="+$(groupAddPrice).eq(i).val()+"AddPriceSerNo]");
        var addPriceUnit=$("input[name="+$(groupAddPrice).eq(i).val()+"AddPriceUnit]");
        $("select[name="+$(groupAddPrice).eq(i).val()+"AddPriceAmount]").each(function (i){
          if(Number(this.value)>0){
            addPriceAmountVal+="&addPriceAmount="+this.value;
            addPriceSerNoVal+="&addPriceSerNo="+$(addPriceSerNo).eq(i).val();
            addPriceUnitVal+="&addPriceUnit="+$(addPriceUnit).eq(i).val();
          }
        });
      }
    });

    var _postData={clazz: "com.flaps.web.ajax.AddGoodsToShoppingCar"};
    var _data = $.param(_postData) +amountVal+serNoVal+addPriceAmountVal+addPriceSerNoVal+addPriceUnitVal;
    $.ajax({
      type: "POST",
      url: "/ajax.json",
      data: _data,
      cache: false,
      dataType: "json",
      error: function (xhr){
        //alert('Ajax request 發生錯誤');
      },
      success: function (response){
        if(response.length>0){
          setCartInfo(response,checkOut);
        }
      },
      async: true
    });
  }else
    alert("您尚未選擇數量");
}
function setCartInfo(response,checkOut){
  var picked=$("div.miniCartPicked ul.picked");
  $(picked).empty();
  if(response.length>0 && response[0].shpInfo.length >0){
    var shpInfo = response[0].shpInfo;
    var total=0;
    $(shpInfo).each(function (i){//arrays
      if(typeof(shpInfo[i].Amount) != "undefined"){
        if(shpInfo[i].MainGoodsStyleCode.length ==0)
          $(picked).append('<li><div class="itemPic">'+shpInfo[i].CarPicture+'</div><div class="itemName">'+shpInfo[i].InternetName+'</div><div class="itemTxt"><span class="itemQuantity">X'+shpInfo[i].Amount+'</span><span class="itemPrice">$ '+shpInfo[i].Total+'</span></div><div class="delete" onclick="deleteCar(this,\''+shpInfo[i].GoodsSerNo+'\',\''+shpInfo[i].Total+'\',\'\');"></div></li>');
        else
          $(picked).append('<li class="extra"><div class="itemPic">'+shpInfo[i].CarPicture+'</div><div class="mark">加價購商品</div><div class="itemName">'+shpInfo[i].InternetName+'</div><div class="itemTxt"><span class="itemQuantity">X'+shpInfo[i].Amount+'</span><span class="itemPrice">$ '+shpInfo[i].Total+'</span></div><div class="delete" onclick="deleteCar(this,\''+shpInfo[i].GoodsSerNo+'\',\''+shpInfo[i].Total+'\',\''+shpInfo[i].MainGoodsStyleCode+'\');"></div></li>');
        total+=FN(shpInfo[i].Total,0);
      }
    });
    if(checkOut)
      location.replace('/shopping/toPage?id=cartlist');
    else {
      var inform=$("div.miniCartInform ul.inform");
      setPromotionName(response[0].promotionName);
      $("#miniCart header div.total").text('$ '+DCZ(total,0));
    }
  }else {
    setPromotionName(null);
    $("#miniCart header div.total").text('$ 0');
  }
  try{
    //miniCartUnfold();//展開購物車
  }catch(e){}
}
function hasCarList() {
//  if($('tbody').eq(0).children('tr').length == 0){
//    alert('購物清單中,無任何商品');
//    return false;
//  }
  var length = 0;
  $('tbody').eq(0).children('tr').each(function (i){//
    //alert($('tbody').eq(0).children('tr').eq(i).attr('class'));
    if($('tbody').eq(0).children('tr').eq(i).attr('class') !='tr-end') {
      length++;
    }
  });
  return length>0;
}
function addToCouposList(elm,row) {
  if($('tbody').eq(0).children('tr').length>0) {
    $('tbody').eq(0).append('<tr class="tr-end"><td><div class="cNum">折價券 - '+row.CouponNo+'</div></td><td></td><td> </td><td>NT -'+row.CouponValue+'</td><td> </td><td><a href="javascript:void(0);" class="cancel" onclick="delCouposList(this,\''+row.CouponNo+'\');return false;"><div class="icon"></div>取消</a></td></tr>');
    $(elm).val('');

  }
}
function delCouposList(elem,coupon) {
  if(coupon != null && coupon.length >0){
    var _postData={clazz: "com.flaps.web.ajax.RemoveCouponFromShoppingCar",coupon: coupon};
    $.ajax({
      type: "POST",
      url: "/ajax.json",
      data: _postData,
      cache: false,
      dataType: "json",
      error: function (xhr){
        //alert('Ajax request 發生錯誤');
      },
      async: true,
      success: function (response){
        //alert(response[0].RemoveCoupon)
        if(response != null && "true" == response[0].RemoveCoupon)
          $(elem).parent().parent().remove();
      }
    });
  }
}
