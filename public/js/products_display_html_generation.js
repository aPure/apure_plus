Object.defineProperty(Array.prototype, '_flat', {
  value: function(depth = 1) {
    return this.reduce(function (_flat, toFlatten) {
      return _flat.concat((Array.isArray(toFlatten) && (depth-1)) ? toFlatten._flat(depth-1) : toFlatten);
    }, []);
  }
});

function templateHtml(e, i){
    if(i%2 == 0){
        return `
            <div class='listing_container'>
              <div class='left_image product_image' id='${e[0]}'>
                <img src='${"http://www.apure.com.tw/img/product/groupbuy/"+e[0]+"_l_1.jpg"}'>
              </div>
              <div data-popup='.popup-blur' class='open-popup left_add_to_cart ${e[0]}' data-number='1' data-discount='${e[5]}' data-id='${e[0]}' data-price='${e[4]}' data-name='${e[1]}' data-img='${"http://www.apure.com.tw/img/product/groupbuy/"+e[0]+"_l_1.jpg"}' data-size='${e[3]}' data-color='${e[2]}' >
                <img src='img/order_product.png'>
              </div>
              <div class='left_description'>${e[1]+"-"+e[2]}</div>
              <div class='left_price'>$${e[4]}</div>
              <div class='left_group_price'>$${Math.ceil(parseInt(e[4])*parseFloat(e[5])*0.1)}</div>
        `;
      }else{
        return `
              <div class='right_image product_image' id='${e[0]}'>
                <img src='${"http://www.apure.com.tw/img/product/groupbuy/"+e[0]+"_l_1.jpg"}'>
              </div>
              <div data-popup='.popup-blur' class='open-popup right_add_to_cart ${e[0]}' data-number='1' data-discount='${e[5]}' data-id='${e[0]}' data-price='${e[4]}' data-name='${e[1]}' data-img='${"http://www.apure.com.tw/img/product/groupbuy/"+e[0]+"_l_1.jpg"}' data-size='${e[3]}' data-color='${e[2]}' >
                <img src='img/order_product.png'>
              </div>
              <div class='right_description'>${e[1]+"-"+e[2]}</div>
              <div class='right_price'>$${e[4]}</div>
              <div class='right_group_price'>$${Math.ceil(parseInt(e[4])*parseFloat(e[5])*0.1)}</div>
              </div>
        `;
      }
}

function getHtml(products){
        
    var products_info = products.map(function(value){
      var discount = value[6];
      var sizes = value[3];
      var type = value[5];
      var price = value[4];
      var colors = value[2];
      var name = value[1];
      var code = value[0];

      colors = colors.split(",");
      colors = colors.map(function(e){
        return e.replace(/\s+/gi, '');
      });
      
      var code_plus_color = colors.map(function(e){
        return code+e;
      });
      
      sizes = sizes.split(",");
      sizes = sizes.map(function(e){
        return e.replace(/\s+/gi, '');
      });
      var size_options = sizes.map(function(e, i){
          return `
          <option ${i == 0 ? 'selected' : ''}  value="${e}">${e}</option>
          `;
      }).join("");

      return colors.map(function(e){
        return [code+e, name, color_code[0][e], sizes, price, discount];
      });

    });
    
    var typeAhtml = products_info._flat().map(templateHtml).join("");
        
  return typeAhtml;
}