$('#miniCart header .unfold').bind('click', function(event) {
  miniCartUnfold();
});

$('.sizeSelect li').click(pickSize); // 商品框尺寸選擇
$('.order').itemPicker(); // 商品頁各色數量選擇器
$('.order').itemPicker('show');
// $('#picZoom').jqzoom({
// 	title:false,
// 	zoomType:'innerzoom'
// });
$('input[name="deliverySelect"]').click(function(e) {
	delivery(e.currentTarget);
});

$('#floatBar .tag').bind({'mouseover':function(){$('#floatBar .tag').css('left',204)},'mouseout':function(){$('#floatBar .tag').css('left',202)},'click' :function() {
	showFloatBar();
}});
$('#datepicker').Zebra_DatePicker({
	view: 'years',
	offset:[20,200],
	direction:[false,  '1900-01-01'] ,
	select_other_months:true,
	start_date:'1980-01-01',
	months:['一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月']
});


// 展開購物車預覽
function miniCartUnfold(){
	$('#miniCart header .unfold').unbind('click');
	$('#miniCart .miniCartMid').slideDown();
	$('#miniCart header .unfold').addClass('fold').removeClass('unfold');
	$('#miniCart header .fold').bind('click', function(event) {
	  miniCartFold();
	});
}
// 收合購物車預覽
function miniCartFold(){
	$('#miniCart header .fold').unbind('click');
	$('#miniCart .miniCartMid').slideUp('fast');
	$('#miniCart .miniCartMid .miniCartPicked .picked').scrollTop(0);
	$('#miniCart header .fold').addClass('unfold').removeClass('fold');
	$('#miniCart header .unfold').bind('click', function(event) {
	  miniCartUnfold();
	});
}
//選擇尺寸
function pickSize(){
	$(this).parent().children('.active').removeClass('active');
	$(this).addClass('active');
}

//展開側欄
function showFloatBar(){
	$('#floatBar').animate({left: 0}, 150);
	$('#floatBar .tag').addClass('action').unbind('click');
	$('#floatBar .tag').bind('click',function(){ hideFloatBar() });
}
function hideFloatBar(){
	$('#floatBar').animate({left: -212}, 100);
	$('#floatBar .tag').removeClass('action').unbind('click');
	$('#floatBar .tag').bind('click',function(){ showFloatBar() })
}
//選擇送貨地區
function delivery(t){
	$('.customerInfo').hide();
	$('#'+$(t).attr('value')).fadeIn(300);
	console.log($(t).attr('value'));
}
//捲回頁首
$('.goTop a').click(function() {
	$('html, body').animate({scrollTop: 0}, 500);
});


$(function(){
  $("#slides").slidesjs({
    width: 978,
    height: 640,
    start: 1,
    navigation:{
    	active: true,
    	effect: "fade"
    },
    pagination:{
    	active: false
    },
    play: {
    	active: false,
    	auto: true,
    	interval: 6000,
    	effect: "fade",
    	pauseOnHover: false
    }
  });
});
$(function(){
	  $("#miniSlides").slidesjs({
	    width: 726,
	    height: 220,
	    start: 1,
	    navigation:{
	    	active: true,
	    	effect: "fade"
	    },
	    play: {
	    	active: false,
	    	auto: true,
	    	interval: 6000,
	    	effect: "fade",
	    	pauseOnHover: false
	    }
	  });
	});
$('table.tableSorter').tableSort();
$("a[rel^='prettyPhoto']").prettyPhoto({
	show_title:false,
	social_tools:false,
	default_width:600,
	default_height:300
});
$(".ruler a").prettyPhoto({
	show_title:false,
	social_tools:false,
	default_width:697,
	allow_resize: false
});

$('.requirement .term a').prettyPhoto({
	show_title:false,
	social_tools:false,
	default_width:697,
	default_height:350
});

// 側欄連結 edm
$(document).ready(function(){
	var sortLink = ['clothcool','clothuv','clothcott'];
	var linkTarget = ['promo_2013fecool.html','promo_puresun.html','promo_cotton.html'];
	for(var i=0; i < sortLink.length; i++){
		$('#sidebar .category .'+sortLink[i]).append(' <a href="/'+linkTarget[i]+'" class="linkToPromo">(more)</a>');
	}
});
