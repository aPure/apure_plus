/*
* code, name, colors, sizes, price, type
*/

var products = [
    ["U17001","女圓領柔感發熱衣","42, 45, 58","L, M, S","890","A"],
    ["U17002","女V領柔感發熱衣","42, 45, 58","L, M, S","890","A"],
    ["U23001","女U領多彩發熱衣","21, 23","L, M, S, XL","690","A"],
    ["U23001","女U領多彩發熱衣","31, 58","L, M, XL","690","A"],
    ["U23001","女U領多彩發熱衣","64, 71","L, M, S, XL","690","A"],
    ["V0700","Pure5.5-性感美臀低腰女三角褲","121,123,138, 142,143,144,145, 152,154,162,172,174,183,185","L, M, XL","340","B"]
];

var color_code = {
    42: "芥末黃",
    45: "杏仁白",
    58: "湖水綠",
    21: "熱情紅",
    23: "櫻桃紅",
    31: "活力橘",
    64: "天空藍",
    71: "流蘇紫",
    28: "橙果橘",
    143: "淡淺黃",
    144: "粉嫩橘",
    142: "淺鵝黃",
    138: "橙果橘",
    123: "靚桃紅",
    121: "玫瑰紅",
    145: "淺米白",
    152: "甜粉綠",
    154: "蘋果綠",
    162: "繽紛藍",
    172: "薰衣紫",
    174: "時尚紫",
    183: "淡藍綠",
    185: "靚亮橘"
};

var all_products = {};
var typeAhtml = "";
var typeBhtml = "";
for (var i in products){
    var sizes = products[i][3];
    var type = products[i][5];
    var price = products[i][4];
    var colors = products[i][2];
    var name = products[i][1];
    var code = products[i][0];
    colors = colors.split(",");
    colors = colors.map(x=>x.replace(/\s/g,''));
    var code_plus_color = colors.map(x => code+x);
    sizes= sizes.split(",");
    sizes = sizes.map(x=>x.replace(/\s/g,''));
    // var id = sizes.map(x=>code_plus_color+x.replace(/\s/g,''));
    //console.log(code_plus_color);
    //var typeA =
    //console.log("ok");
    //console.log(products[i][2]);
    //type A
    if(i!=5){
            var size_options = "";
            for(s in sizes){
                if(s==0){
                    size_options +="<option selected value='"+sizes[s]+"'>"+sizes[s]+"</option>";
                }else{
                    size_options +="<option value='"+sizes[s]+"'>"+sizes[s]+"</option>";
                }

            }
            for(var c in colors){
                typeAhtml+="<div class='swiper-slide'><div class='itemPic'><a href='#'><img src='http://www.apure.com.tw/img/product/U2300181_b_1.jpg'></a></div><div class='itemName' itemprop='name'><a href='item/"+code+colors[c]+"'>"+name+"-"+color_code[colors[c]]+"</a></div><div class='itemPromo' itemprop='description'></div><div class='itemQty'>選擇尺寸<select name='amount' class='size_selection'>"+size_options+"</select> </div><div class='buy'><button class='addToCart add-to-cart' data-name='"+name+"' data-color='"+color_code[colors[c]]+"' data-id='"+code+colors[c]+"' data-size='"+sizes[0]+"' data-price='"+price+"' data-img='http://www.apure.com.tw/img/product/"+code+colors[c]+"_m_1.jpg' data-type='A' id='toCart'>加入購物車</button></div></div>";
            }

    }else{
        for(var c in colors){
            typeBhtml+="<div class='item col3 swiper-slide'>\
            <div class='itemPic'> \
                <a href='#' title=''> \
                    <img src='http://www.apure.com.tw/img/product/"+code+colors[c]+"_m_1.jpg'> \
                </a> \
            </div> \
            <div class='itemName' itemprop='name'> \
                <a href='item/"+code+colors[c]+"'>"+name+"-"+color_code[colors[c]]+"</a> \
            </div> \
        <div class='itemPromo' itemprop='description'></div> \
        <div class='itemQty'>選擇尺寸 \
            <select name='amount' class='size_selection'>"+size_options+" \
            </select><input type='hidden' name='goodsSerNo' value='${g.SizeInfo[0].GoodsSerNo}'> \
            <div class='itemStocks'>庫存 - 足夠</div> \
        </div> \
        <div class='buy'> \
            <button class='addToCart add-to-cart' data-name='"+name+"' data-color='"+color_code[colors[c]]+"' data-id='"+code+colors[c]+"' data-size='"+sizes[0]+"' data-price='"+price+"' data-img='http://www.apure.com.tw/img/product/"+code+colors[c]+"_m_1.jpg' data-type='B' id='toCart'>加入購物車</button> \
        </div> \
    </div>";
        }
    }
}

//$$(".A").html(typeAhtml);

//$(".B").html(typeBhtml);
