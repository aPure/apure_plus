
var $$ = Dom7;
var server = window.location.href;
server = server.replace("#", "");
//console.log(server);
var myApp = new Framework7({
  //swipePanel: 'left',
  //animateNavBackIcon: true
  //fastClicks: false
});
var mainView = myApp.addView('.view-main', {
  domCache: true,
  dynamicNavbar: true
});

function checkbrowser(){
  var chrome   = navigator.userAgent.indexOf('Chrome') > -1;
  var google = navigator.vendor.indexOf('Google Inc.') > -1;
  //var explorer = navigator.userAgent.indexOf('MSIE') > -1;
  //var firefox  = navigator.userAgent.indexOf('Firefox') > -1;
  var safari   = navigator.userAgent.indexOf("Safari") > -1;
  //var camino   = navigator.userAgent.indexOf("Camino") > -1;
  //var opera    = navigator.userAgent.toLowerCase().indexOf("op") > -1;
  if ((chrome) && (safari)) safari = false;
  //if ((chrome) && (opera)) chrome = false;
  //console.log(chrome, safari, google);
  //alert(navigator.userAgent);
  if(!chrome && !safari){
    $$('.views').hide();
      myApp.alert("Use Chrome or Safari.", "Don't use that", function(){
        checkbrowser();
    });
  }else if(chrome && !google){
    $$('.views').hide();
      myApp.alert("Use Chrome or Safari.", "Don't use that", function(){
        checkbrowser();
    });
  }else{
    $$('.views').show();
  }
  
}

checkbrowser();

var shopDatas = myApp.formGetData('shopper');

if(!shopDatas || shopDatas.phone == ""){
  welcomescreen.open();
  
  setTimeout(function(){
    welcomescreen.close();
    mainView.hideNavbar();
    mainView.hideToolbar();
    mainView.router.load({pageName: 'login'});
  }, 3000);
}else{
    $$('.user_name').val(shopDatas.name);
    $$('.user_phone').val(shopDatas.phone);
    $$('.vip_name').val(shopDatas.vip_name);
    $$('.vip_phone').html("&nbsp;&nbsp;&nbsp;"+shopDatas.vip_phone);
    $$('.vip_phone').attr({href: 'tel:'+shopDatas.vip_phone});
    $$('.vip_address').val(shopDatas.vip_name+" de address");
    $$('.mainpage').append(getHtml(shopDatas.products));
    $$('.categories').append(`<li>
    <div class="item-content button_block">
        <div class="item-inner">
            <a href="#" class="external menu_text">個人資料fsdfs</a>
        </div>
    </div>
</li>`);
    var apureGroup = myApp.formGetData('aPureGroup');
    var login = myApp.formGetData('Login');
    if(!apureGroup || !apureGroup.visited && login.afterlogin){
      tutorialscreen.open();
      
    }
    
            
}       

function restructure(cartArray){
  
  var newStructureArray = [];
  var output = "";
  var order_table = "";
  var confirm_cart = "";
  var parsedID = [];   
   
  for (var i in cartArray) {
      // cartArray[i].id became cartArray[i].productId 
      // cartArray[i].count became cartArray[i].number 
      var L_count = 0, 
      S_count = 0, 
      M_count = 0, 
      XL_count = 0, 
      ids_total_count = 0, 
      ids_total_price = 0, 
      availSize = "",
      name="",
      color="",
      ret = cartArray[i].productId.replace(cartArray[i].size, ''),
      name = cartArray[i].name,
      color = cartArray[i].color,
      price = cartArray[i].price;
      
      if(!parsedID.includes(ret)){

          availSize = cartArray[i].type;

          for(var j in cartArray){
              
              if(cartArray[j].productId.indexOf(ret) !== -1){
                  
                  if(cartArray[j].size == "L"){
                      L_count+=cartArray[j].number;
                      ids_total_count+=cartArray[j].number;
                      ids_total_price+=cartArray[j].price*cartArray[j].number;
                  }
                  if(cartArray[j].size == "M"){
                      M_count+=cartArray[j].number;
                      ids_total_count+=cartArray[j].number;
                      ids_total_price+=cartArray[j].price*cartArray[j].number;
                  }
                  if(cartArray[j].size == "XL"){
                      XL_count+=cartArray[j].number;
                      ids_total_count+=cartArray[j].number;
                      ids_total_price+=cartArray[j].price*cartArray[j].number;
                  }
                  if(cartArray[j].size == "S"){
                      S_count+=cartArray[j].number;
                      ids_total_count+=cartArray[j].number;
                      ids_total_price+=cartArray[j].price*cartArray[j].number;
                  }

                  

              }
          }
          var item_by_id = {
              id: ret,
              L_count: L_count,
              S_count: S_count, 
              M_count: M_count, 
              XL_count: XL_count, 
              ids_total_count: ids_total_count, 
              ids_total_price: ids_total_price,
              availSize: availSize,
              name: name,
              color: color,
              unit_price: price,
              link: "http://www.apure.com.tw/img/product/"+ret+"_l_1.jpg",
              S_code: ret+"S",
              L_code: ret+"L",
              M_code: ret+"M",
              XL_code: ret+"XL"
          };
          //console.log(item_by_id);
          newStructureArray.push(item_by_id);

      }
      
      parsedID.push(ret);

  }

  for(var i in newStructureArray){
    
    order_table += "<div class='buyer_cart_container' style='float:left;' id='"+newStructureArray[i].id+"'> \
    <div class='cart_image'><img src='http://www.apure.com.tw/img/product/"+newStructureArray[i].id+"_l_1.jpg' /> \
    </div>";
    if(newStructureArray[i].S_count != 0){
      order_table += "<div class='ordered_sizes' id='"+newStructureArray[i].id+"_s'> \
        <a class=''>S</a> \
        <a class='num'>"+newStructureArray[i].S_count+"</a> \
      </div> ";
    }
    if(newStructureArray[i].M_count != 0){
      order_table += "<div class='ordered_sizes' id='"+newStructureArray[i].id+"_m'> \
          <a class=''>M</a> \
          <a class='num'>"+newStructureArray[i].M_count+"</a> \
      </div>";
    }
    if(newStructureArray[i].L_count != 0){
      order_table += "<div class='ordered_sizes' id='"+newStructureArray[i].id+"_l'> \
          <a class=''>L</a> \
          <a class='num'>"+newStructureArray[i].L_count+"</a> \
      </div>";
    }
    if(newStructureArray[i].XL_count != 0){
      order_table += "<div class='ordered_sizes' id='"+newStructureArray[i].id+"_xl'> \
          <a class=''>XL</a> \
          <a class='num'>"+newStructureArray[i].XL_count+"</a> \
      </div>";
    }
    order_table += "<div class='text'>( ID: "+newStructureArray[i].id+")</div> \
    </div>";

    
  }

  return order_table;
}

function display_customers_list(){
  var shopDatas = myApp.formGetData('shopper');
  //console.log(server);
  $$.get(server+'mobile_data_fetch', {"vip": shopDatas.vip_phone}, function(data, status, xhr){
    var resp = JSON.parse(xhr.response);
    console.log(resp);
    var list_customers = "";
    for(var i in resp['customers']){
      //console.log(resp['customers'][i]['name']);
      var amount_spent_by_buyer = 0;
      var items_ordered = [];
      for(var j in resp['orders']){
        if(resp['orders'][j]['userId'] == resp['customers'][i]['id']){
          amount_spent_by_buyer+=resp['orders'][j]['totalAmount'];
          for(var k in resp['orders_details']){
            if(resp['orders'][j]['id'] == resp['orders_details'][k][0]['orderId']){
              for(var m in resp['orders_details'][k]){
                items_ordered.push(resp['orders_details'][k][m]);
              }  
            }
          }
        }
      }

      list_customers += "<li class='accordion-item'> \
      <a href='#' class='item-link item-content'> \
          <div class='item-inner'> \
              <div class='item-title'>"+resp['customers'][i]['name']+"</div> \
              <div class='item-after'>$"+amount_spent_by_buyer+"</div> \
          </div> </a> \
          <div class='accordion-item-content'> \
              <div class='content-block'>"+restructure(items_ordered)+"</div> \
          </div></li>";
    }
    var deadline = new Date(resp['group_buying'][0]['until_when']);
    $$('.all_buyers ul').html(list_customers);
    $$('.amount_spent').html(resp['amount_spent']);
    $$('.items_number').html(resp['itemsNumber']);
    $$('.number_buyers').html(resp['customers'].length);
    $$('.minimum_amount').html(resp['group_buying'][0]['minimum_amount_of_money']);
    $$('.deadline').html(deadline.getFullYear() + '/' +(deadline.getMonth() + 1) + '/' + deadline.getDate());

  });
}


var mySwiper1 = myApp.swiper('.swiper-1', {
  pagination:'.swiper-1 .swiper-pagination',
  paginationHide: false,
  paginationClickable: false,
  spaceBetween: 50,
  slidesPerView: 2,
  autoplay: false,
  loop: false
});

var mySwiper2 = myApp.swiper('.swiper-2', {
  paginationHide: true,
  spaceBetween: 10,
  slidesPerView: 2,
  speed: 300,
  autoplay: false,
  loop: false
});

var mySwiper3 = myApp.swiper('.swiper-3', {
  paginationHide: true,
  nextButton: '.next2',
  prevButton: '.prev2',
  spaceBetween: 10,
  slidesPerView: 2,
  speed: 300,
  autoplay: false,
  loop: true
});

if(shoppingCart.totalCart() == 0){
  mainView.router.load({pageName: 'special_current_customers'});
  $$('.index_navbar').removeClass('cached');
}

$$('.logout').on('click', function(){
  myApp.confirm("登出 ?", "", function(){
    var deletion = myApp.formDeleteData('shopper');
    shoppingCart.clearCart();
    mainView.hideNavbar();
    mainView.hideToolbar();
    myApp.closePanel();
    mainView.router.load({pageName: 'login'});
  });
  
});