Vue.component('product', {
    
    props:['image', 'title', 'itemlink', 'number-avail', 'size', 'price', 'id', 'type'],

    template: "\
    <div class='item col3' itemscope itemtype='http://schema.org/Product'>\
        <div class='itemPic'> \
            <a href='#' title=''> \
                <img :src='image'> \
            </a> \
        </div> \
        <div class='itemName' itemprop='name'> \
            <a :href='itemlink'>{{title}}</a> \
        </div> \
    <div class='itemPromo' itemprop='description'></div> \
    <div class='itemQty'>選擇尺寸 \
        <select name='amount'> \
            <option value=''>{{size}}</option> \
        </select><input type='hidden' name='goodsSerNo' value='${g.SizeInfo[0].GoodsSerNo}'> \
        <div class='itemStocks'>庫存 - 足夠</div> \
    </div> \
    <div class='buy'> \
        <button class='addToCart add-to-cart' :data-name='title' :data-id='id' :data-size='size' :data-price='price' :data-img='image' :data-type='type' id='toCart'>加入購物車</button> \
    </div> \
</div>"
});

new Vue({
    data: {
        message: 'hello World'
    },
    el: '#list'
});