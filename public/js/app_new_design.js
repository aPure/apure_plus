$$('#submitLogin').on('click', function(){
    var login_info = myApp.formToData('#login_info');
    if(login_info.phone != "" && login_info.password != ""){
      server = server.replace(/\#/g, "");
      var url = server+"login";
      //console.log(server);
      $$.post(url, login_info, function(data, status, xhr){
        var resp = JSON.parse(xhr.response);
        //console.log(xhr.response);
        if(resp["result"] == '1'){
          
          myApp.formStoreData('shopper', 
              {products: resp['productshtml'], 
              version: resp['version'], 
              phone: login_info.phone, 
              pwd: login_info.password, 
              vip_phone: resp["vip"], 
              vip_name: resp["vip_name"], 
              name: resp['user_name'], 
              group_id: resp['group_id']});
     
          mainView.showNavbar();
          mainView.showToolbar();
       
          myApp.formStoreData('Login', {afterlogin: true});
         
          mainView.router.load({pageName: 'special_current_customers'});
          window.location.reload();
        
          $$('.collective_money').show();
          $$('.vip_submenu').hide();
          
          $$('.index_navbar').removeClass('cached');
        }else if(resp["result"] == '0'){
          $$(".pwd").val("");
          myApp.alert("Wrong password", "Sorry");
        }else if(resp["result"] == '-1'){
            $$(".phone").val("");
            $$(".pwd").val("");
            myApp.alert("You are not our customer yet", "Oups");
        }else{
          $$(".phone").val("");
          $$(".pwd").val("");
          myApp.alert("You are not our customer yet", "Oups");
        }
        
       }, function(xhr, status){
          //window.location.reload();
       });
      
    }else{
      myApp.alert("Man, please. Put something", "Are you our beloved customer ?");
    }
    
});

function gotohouse(){
  $$('.house img').attr({src: 'http://apure.com.tw/img/group/icons/house_green.png'});
  $$('.cart img').attr({src: 'http://apure.com.tw/img/group/icons/cart_gray.png'});
  $$('.user img').attr({src: 'http://apure.com.tw/img/group/icons/user_gray.png'});
  $$('.user_center_page').addClass('cached');
  // $$('.mainpage').show();
  mainView.router.load({pageName: 'special_current_customers'});
}

function gotouser(){
  // $$('.mainpage').hide();
  $$('.house img').attr({src: 'http://apure.com.tw/img/group/icons/house_gray.png'});
  $$('.cart img').attr({src: 'http://apure.com.tw/img/group/icons/cart_gray.png'});
  $$('.user img').attr({src: 'http://apure.com.tw/img/group/icons/user_green.png'});
  $$('.homepage').addClass('cached');
  mainView.router.load({pageName: 'user_center'});
}

function gotocart(){
  // $$('.mainpage').hide();
  if(shoppingCart.countCart() != 0){
    $$('.house img').attr({src: 'http://apure.com.tw/img/group/icons/house_gray.png'});
    $$('.cart img').attr({src: 'http://apure.com.tw/img/group/icons/cart_green.png'});
    $$('.user img').attr({src: 'http://apure.com.tw/img/group/icons/user_gray.png'});
    //displayCart();
    $$('.toolbarmenu').hide();
    $$('.toolbarpurchasenext').show();
    $$('.confirm_button').css({color: '#ff5d55'});
    $$('.homepage').addClass("cached");
    mainView.router.load({pageName: 'confirm_shopping'});
  }else{
    myApp.alert("您沒有訂購任何商品", "抱歉");
  }
}

$$('.house_from_panel').on('click', function(){
  gotohouse();
  myApp.closePanel();
  //myApp.openPanel('right');
});

$$('.cart_from_panel').on('click', function(){
  gotocart();
  myApp.closePanel();
});

$$('.user_from_panel').on('click', function(){
  gotouser();
  myApp.closePanel();
});

$$('.house').on('click', function(){
  gotohouse();
});

$$('.cart').on('click', function(){
  gotocart();
});

$$('.user').on('click', function(){
  gotouser();
});
$$('.go_to_summary_shopping').on('click', function(){
  mainView.router.load({pageName: 'summary_shopping'});
});

$$('.go_to_info_shopping').on('click', function(){
  if(shoppingCart.totalCart() == 0){
    return false;
  }
  $$('.toolbarpurchasenext').hide();
  $$('.toolbarfinish').show();
  $$('.info_button').css({color: '#ff5d55'});
  $$('.confirm_button').css({color: 'black'});
  $$('.confirm_shopping_page').addClass('cached');
  mainView.router.load({pageName: 'info_shopping'});
});

function gotofinishshopping(){
  // $$('.mainpage').hide();
  $$('.toolbarfinish').hide();
  $$('.toolbargoback').show();
  $$('.finish_button').css({color: '#ff5d55'});
  $$('.info_button').css({color: 'black'});
  $$('.info_shopping_page').addClass('cached');
  var shopDatas = myApp.formGetData('shopper');

    var payment = "cash";
    var name = $$('.user_name').val();
    var tel = $$('.user_phone').val();
    var address = "VIP address";
    var delivery = "VIP delivery type";
    var buyerAmountToPay = $$('.price_final').html();
    //console.log(buyerAmountToPay);
  var orders = JSON.stringify(shoppingCart.listCart());
  
  var to_send = {
    name: name, 
    tel: tel, 
    address: address, 
    payment: payment, 
    buyerAmountToPay: buyerAmountToPay.substr(1), 
    order: orders, 
    mobile: "1", 
    account_phone: shopDatas.phone, 
    total_amount: shoppingCart.totalCart(), 
    delivery: delivery,
    vip: shopDatas.vip_phone};
  var url = server+"mobile";
  //console.log('step #1');
  $$.get(url, to_send, function(data, status, xhr){
    var resp = JSON.parse(xhr.response);
    //console.log('step #2');
    //console.log(xhr);
    if(resp["result"] == '1'){
      shoppingCart.clearCart();
      $$(".accept_terms").attr({'checked': false});
      progress_bar();
      displayCart();
      //console.log('#2_1');
      
    }else if(resp["result"] == '-2'){
          shoppingCart.clearCart();
          mainView.router.load({pageName: 'special_current_customers'});
          myApp.alert("Sorry. There is no group buy now.", "No Group Buy Now", function(){
            location.reload();
          });
    }
    
  }, function(xhr, status){
    console("step #3");
    console.log(xhr);
  });

  mainView.router.load({pageName: 'finish_shopping'});
}

$$('.go_back_home').on('click', function(){
  $$('.toolbargoback').hide();
  $$('.toolbarpurchasenext').hide();
  $$('.toolbarfinish').hide();
  $$('.toolbarmenu').show();
  $$(".accept_terms").attr({'checked': false});
  $$('.finish_button').css({color: 'black'});
  $$('.info_shopping_page').addClass('cached');
  $$('.confirm_shopping_page').addClass('cached');
  
  $$('.house img').attr({src: 'http://apure.com.tw/img/group/icons/house_green.png'});
  $$('.cart img').attr({src: 'http://apure.com.tw/img/group/icons/cart_gray.png'});
  $$('.user img').attr({src: 'http://apure.com.tw/img/group/icons/user_gray.png'});
  mainView.router.load({pageName: 'special_current_customers'});
});

$$('.go_back_vip').on('click', function(){
  mainView.router.load({pageName: 'vip_index'});
});

$$('.left_add_to_cart, .right_add_to_cart').on('click', function(){
  var id = $$(this).attr('data-id');
  //console.log(id);
  var img = $$(this).attr('data-img');
  var price = $$(this).attr('data-price');
  var color = $$(this).attr('data-color');
  var size = $$(this).attr('data-size');
  var name = $$(this).attr('data-name');
  var sku = $$(this).attr('data-id');
  var discount = $$(this).attr('data-discount');
  $$('.product_add_to_cart').attr('data-id',id);
  $$('.product_add_to_cart').attr('data-price',price);
  $$('.product_add_to_cart').attr('data-name',name);
  $$('.product_add_to_cart').attr('data-color',color);
  $$('.product_add_to_cart').attr('data-img',img);
  $$('.product_add_to_cart').attr('data-availsize', size);
  $$('.product_add_to_cart').attr('data-discount', discount);
  $$('.product_add_to_cart').attr('data-size', '');
  $$('.product_add_to_cart').attr('data-number', '1');
  $$('.single_product_image img').attr({src: img});
  $$('.product_description').text(name+"-"+color);
  $$('.product_price').text("$"+Math.ceil(price*discount*0.1));
  //console.log(price, discount, price*discount);
  $$('.product_original_price').text("$"+price);
  size = size.split(",");
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  // S, M, L, XL
  // console.log(size);
  //for(var i=0; i<size.length; i++){
    //console.log(size.includes("S"));
    if(!size.includes("S")){
      $$('.one').hide();
      //console.log("S");
    }else{
      $$('.one').show();
    }
    if(!size.includes("M")){
      $$('.two').hide();
      //console.log("M");
    }else{
      $$('.two').show();
    }
    if(!size.includes("L")){
      $$('.three').hide();
      //console.log("L");
    }else{
      $$('.three').show();
    }
    if(!size.includes("XL")){
      $$('.four').hide();
      //console.log("XL");
    }else{
      $$('.four').show();
    }
    if(!size.includes("XXL")){
      $$('.five').hide();
      //console.log("XXL");
    }else{
      $$('.five').show();
    }
    if(!size.includes("C130")){
      $$('.six').hide();
      //console.log("XXL");
    }else{
      $$('.six').show();
    }
    if(!size.includes("C140")){
      $$('.seven').hide();
      //console.log("XXL");
    }else{
      $$('.seven').show();
    }
  //}
  myApp.popup(".popup-product");
});

$$('.product_number_choice select').on('change', function(){
  $$('.product_add_to_cart').attr('data-number',$$(this).val());
});

$$('.one').on('click', function(){
  $$('.one').css('background-color', 'gray');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'S');
});

$$('.two').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'gray');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'M');
});

$$('.three').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'gray');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'L');
});

$$('.four').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'gray');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'XL');
}); 
$$('.five').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'gray');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'XXL');
}); 
$$('.six').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'gray');
  $$('.seven').css('background-color', 'white');
  $$('.product_add_to_cart').attr('data-size', 'C130');
}); 
$$('.seven').on('click', function(){
  $$('.one').css('background-color', 'white');
  $$('.two').css('background-color', 'white');
  $$('.three').css('background-color', 'white');
  $$('.four').css('background-color', 'white');
  $$('.five').css('background-color', 'white');
  $$('.six').css('background-color', 'white');
  $$('.seven').css('background-color', 'gray');
  $$('.product_add_to_cart').attr('data-size', 'C140');
}); 
function addItemNumberSize(data){
  data = data.split("__");
  // console.log(data);
  // data[0], data[1], data[2], data[3], 1, data[4], data[5], data[6]
  // id, name, color, price, size, availSize, img
  shoppingCart.addItemToCart(data[0], data[1], data[6], Number(data[3]), 1, data[4], data[2], data[5], data[7]);
  displayCart();

}

function reduceItemNumberSize(id){
  //console.log(shoppingCart.numberOfItem(id))
  shoppingCart.removeItemFromCart(id);
  displayCart();
  if(shoppingCart.totalCart() == 0){
      $$('.go_back_home').click();
  }
  
}

function deleteItemWithAllSizes(data){
  myApp.confirm("確定要刪除嗎?", "刪除", function(){
    data = data.split("__");
    var sizes = data[1].split(",");
    for(var i in sizes){
      shoppingCart.removeItemFromCartAll(data[0]+sizes[i]);
    }
    //shoppingCart.removeItemFromCartAll(id);
    displayCart();
    if(shoppingCart.totalCart() == 0){
      $$('.go_back_home').click();
    }
  });
  
}

$$('.open-left-panel').on('click', function(){
  myApp.openPanel('left');
});

$$('.to_customers_list').on('click', function(){
  mainView.router.load({pageName: 'buyers_list'});
});

$$('.add_buyers').on('click', function(){
  $$('.toolbarmenu').hide();
  $$('.toolbaraddbuyers').show();
  mainView.router.load({pageName: 'add_buyers'});
});

$$('.button_purchase_summary').on('click', function(){
  //$$('.toolbarmenu').show();
  mainView.router.load({pageName: 'summary_shopping'});
});

$$('.submitbuyer').on('click', function(){
  var newbuyerinfo = myApp.formToData('#newbuyer');
  var shopDatas = myApp.formGetData('shopper');
  newbuyerinfo['mobile'] = '1';
  newbuyerinfo['vip_phone'] = shopDatas.vip_phone; 
  $$('.toolbaraddbuyers').hide();
  //$$('.toolbarmenu').show();
  
  //console.log(server+'sales/add_group_buyers');
  $$.post(server+'sales/add_group_buyers', newbuyerinfo, function(data, status, xhr){
    var resp = JSON.parse(xhr.response);
    //console.log(xhr.response);
    $$('.add_customer_name').val("");
    $$('.add_customer_phone').val("");
    $$('.new_added_name').html(newbuyerinfo.name);
    $$('.new_added_phone').html(newbuyerinfo.phone);
    
    if(resp['success'] == '1'){
      display_customers_list();
      myApp.alert(newbuyerinfo.name+' '+newbuyerinfo.phone, "確定新增此會員嗎?", function(){
        $$('.toolbargoback').show();
        mainView.router.load({pageName: 'finish_adding_buyer'});
      });
      
      
    }else if(resp['reason'] == '-2'){
      myApp.alert("You reached your maximum number of group buyers", "Cannot add new customer");
      mainView.router.load({pageName: 'buyers_list'});
    }
  });
  
});

function progress_bar(){
  var shopDatas = myApp.formGetData('shopper');
  //console.log(shopDatas);
  if(shopDatas){
    $$.get(server+'mobile_progress_bar', {"vip": shopDatas.vip_phone, "buyer": shopDatas.phone}, function(data, status, xhr){
      var resp = JSON.parse(xhr.response);
      //console.log(resp);
      if(shopDatas.phone != shopDatas.pwd){
        $('.if_password_not_changed').hide(); 
        $('.if_password_not_changed_control').hide();
      }
      
      if(status == 500 || resp['version'] != shopDatas.version){
        var deletion = myApp.formDeleteData('shopper');
        mainView.router.load({pageName: 'login'});
        shoppingCart.clearCart();
        mainView.hideNavbar();
        mainView.hideToolbar();
        window.location.reload();
      }
      $$('.percent').html('$'+resp['amount_spent']+'&nbsp;&nbsp;&nbsp;&nbsp;'+resp['progress']+'%&nbsp;');
      var progressbar = $$('.progressbar');
      myApp.setProgressbar(progressbar, resp['progress']);
      $$('.thisgroupbuy').html("本期團購期限: "+resp['from_when']+ " - "+resp['until_when']);
      $$('.period_menu').html("本期團購期限: "+resp['from_when']+ " - "+resp['until_when']);
      productSummary(resp['orders_items']);
      if(resp['buyer_payment'][0]['totalForBuyer'] != null){
        $$('.amount_to_pay').html('$'+resp['buyer_payment'][0]['totalForBuyer']);
      }else{
        $$('.amount_to_pay').html('$0');
      }
      
    }, function(xhr, status){
      var deletion = myApp.formDeleteData('shopper');
      mainView.router.load({pageName: 'login'});
      shoppingCart.clearCart();
      mainView.hideNavbar();
      mainView.hideToolbar();
      window.location.reload();
    });
  }
}
$$('.close-welcomescreen').on('click', function(){
  myApp.formDeleteData('Login');
  myApp.formStoreData('aPureGroup', {visited: true});
  tutorialscreen.close();
})

$$('.apure_contact').on('click', function(){
  myApp.alert('customer@apuremail.com', '聯絡客服');
});

function productSummary(items){
  //var total_cost = 0;
  //console.log(items);
  parsedID = [];
  summary_table = "";
  var summaryArray = [];
  if(items == []){
    summary_table +="<div style='text-align:center'>No item ordered</div>";
  }
  for(var i in items){
    var L_num = 0, 
        S_num = 0, 
        M_num = 0, 
        XL_num = 0,
        XXL_num = 0,
        C130_num = 0,
        C140_num = 0,
        retu = reserveString(reserveString(items[i].productId).replace(reserveString(items[i].size), '')),
        name_summary = items[i].productName,
        price = items[i].price;
    
        //console.log(retu);
        if(!parsedID.includes(retu)){
            
            for(var j in items){
                
                if(items[j].productId.indexOf(retu) !== -1){
                    
                    if(items[j].size == "L"){
                        L_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "M"){
                        M_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "XL"){
                        XL_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "S"){
                        S_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "XXL"){
                        XXL_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "C130"){
                        C130_num+=parseInt(items[j].number);
                    }
                    if(items[j].size == "C140"){
                        C140_num+=parseInt(items[j].number);
                    }
                }
            }
            var item_by_id = {
                id: retu,
                L_count: L_num,
                S_count: S_num, 
                M_count: M_num, 
                XL_count: XL_num, 
                XXL_count: XXL_num,
                C130_count: C130_num,
                C140_count: C140_num,
                name: name_summary,
                unit_price: price,
                link: "http://www.apure.com.tw/img/product/"+retu+"_l_1.jpg",
                S_code: retu+"S",
                L_code: retu+"L",
                M_code: retu+"M",
                XL_code: retu+"XL",
                XXL_code: retu+"XXL",
                C130_code: retu+"C130",
                C140_code: retu+"C140"
            };
            //console.log(item_by_id);
            summaryArray.push(item_by_id);

        }
        
        parsedID.push(retu);
  }

  for(var i in summaryArray){
    var measure_word = "件";
    if(summaryArray[i].id[0] == "S"){
      measure_word = "雙";
    }
    summary_table += "<div class='summary_cartcontainer' id='"+summaryArray[i].id+"'> \
    <div class='cart_image'><img src='http://www.apure.com.tw/img/product/groupbuy/"+summaryArray[i].id+"_l_1.jpg' /> \
    </div>";
    if(summaryArray[i].S_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>S</span> \
        <span>"+summaryArray[i].S_count+measure_word+"</span> \
      </div>";
    }
    if(summaryArray[i].M_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>M</span> \
        <span>"+summaryArray[i].M_count+measure_word+"</span> \
      </div>";
    }
    
    if(summaryArray[i].L_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>L</span> \
        <span>"+summaryArray[i].L_count+measure_word+"</span> \
      </div>";
    }

    if(summaryArray[i].XL_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>XL</span> \
        <span>"+summaryArray[i].XL_count+measure_word+"</span> \
      </div>";
    }
    
    if(summaryArray[i].XXL_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>XL</span> \
        <span>"+summaryArray[i].XXL_count+measure_word+"</span> \
      </div>";
    }

    if(summaryArray[i].C130_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>XL</span> \
        <span>"+summaryArray[i].C130_count+measure_word+"</span> \
      </div>";
    }

    if(summaryArray[i].C140_count != 0){
      summary_table +="<div class='summary_size'> \
        <span>XL</span> \
        <span>"+summaryArray[i].C140_count+measure_word+"</span> \
      </div>";
    }
    summary_table +="<div class='text'>"+summaryArray[i].name+" ( ID: "+summaryArray[i].id+")</div> \
  </div>";
    
  }
  var top_summary_table = "<div class='content-block-title user_center_titles'> \
                     訂單資訊 \
                          </div> \
                    <div class='total_payment'> \
                <span class='price_summary amount_to_pay'></span> \
              </div>";
  $$(".summary_table").html(top_summary_table+summary_table);
  //$$(".summary_table").html(summary_table);
}
$(window).on('load', function(){
  mainView.router.load({pageName: 'special_current_customers'});
  // var shopDatas = myApp.formGetData('shopper');
  // if()
});

$$(document).on('page:back', function (e) {
    var page = e.detail.page.name;
    if(page == 'special_current_customers'){

    }
    
    if(page == 'confirm_shopping'){
      $$('.toolbarfinish').hide();
      $$('.toolbargoback').hide();
      $$('.toolbarpurchasenext').hide();
      $$('.toolbarmenu').show();
    }
    if(page == 'info_shopping'){
      $$('.toolbarfinish').hide();
      $$('.toolbargoback').hide();
      $$('.toolbarpurchasenext').show();
      $$('.toolbarmenu').hide();
      $$('.info_button').css({color: 'black'});
      $$('.confirm_button').css({color: '#ff5d55'});
      $$(".accept_terms").attr({'checked': false});
      $$('.confirm_shopping_page').removeClass('cached');
    }
    if(page == 'user_center'){
    
      $$('.house img').attr({src: 'http://apure.com.tw/img/group/icons/house_green.png'});
      $$('.cart img').attr({src: 'http://apure.com.tw/img/group/icons/cart_gray.png'});
      $$('.user img').attr({src: 'http://apure.com.tw/img/group/icons/user_gray.png'});
      $$('.user_center_page').addClass('cached');
      $$('.homepage').removeClass('cached');
      mainView.router.load({pageName: 'special_current_customers'});
    }
})

function checkorientation(){
  if(window.orientation != 0){
    $$('.views').hide();
    myApp.alert("抱歉", "噢!請將手機轉回去謝謝", function(){
      checkorientation();
    });
  }else{
    $$('.views').show();
  }
}

$$('.position_panel_back').on('click', function(){
  myApp.closePanel();
});

$(window).on("orientationchange",function(){
  //alert(window.orientation);
  checkorientation();
});

$$('.change_password').on('click', function(){
  myApp.prompt("請輸入舊密碼", "更新", function(oldpass){
    myApp.prompt("請輸入新的密碼", "更新", function(newpass){
      var shopDatas = myApp.formGetData('shopper');
      $$.get(server+"mobile/renew", {"old_pass": oldpass, "new_pass": newpass, "isspecialuser": 0, "phone": shopDatas.phone, "ismobile": 1}, function(data, status, xhr){
        var resp = JSON.parse(xhr.response);

        if(resp["result"]==-1){
          myApp.alert("舊的密碼輸入錯誤", "抱歉");
        }
        if(resp["result"]==0){
          myApp.alert("更改密碼成功! 請重新登入謝謝", "Ok", function(){
            var deletion = myApp.formDeleteData('shopper');
            shoppingCart.clearCart();
            mainView.hideNavbar();
            mainView.hideToolbar();
            mainView.router.load({pageName: 'login'});
          });
        }
        //console.log(resp);
      });
    });
  });
});

// function printing(){
//   console.log("ok");
// }


if(typeof(Worker) !== "undefined") {
  if (typeof(w) == "undefined") {
    w = new Worker("js/checking_worker.js");
  }
  
  var shopDatas = myApp.formGetData('shopper');

  // worker post a message and i get it
  w.onmessage = function(e) {
    //console.log('Worker said: ', e.data);
    var cmd_data = e.data.split(" ");
    if(e.data == 'test bars'){
      //console.log("testing bars ...");
      var navbarhidden = document.getElementsByClassName("navbar-hidden").length;
      var toolbarhidden = document.getElementsByClassName("toolbar-hidden").length;
      if(mainView.activePage){
        if(mainView.activePage.name == 'special_current_customers'){
          if(navbarhidden > 0 && toolbarhidden > 0){
            window.location.reload();
          }
        }
      }
      
    }
    if(cmd_data[0] == 'group_id'){
      if(shopDatas){
        if(shopDatas.group_id == cmd_data[1]){
          console.log("same group id");
        }else{
          var deletion = myApp.formDeleteData('shopper');
          mainView.router.load({pageName: 'login'});
          shoppingCart.clearCart();
          mainView.hideNavbar();
          mainView.hideToolbar();
        }
      }
      
      //console.log(e.data);
    }
    //console.log(e.data);
  };
  // I sent a message to the worker
  w.postMessage([server, shopDatas.vip_phone]);
} else {
  console.log("No Support for workers");
}