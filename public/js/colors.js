var color_code = [{
    11 : "黑",
    12: "深灰",
    13: "灰",
    14: "淺灰",
    15: "白",
    17: "淺鐵灰",
    20: "熱情紅",
    21: "熱情紅",
    23: "櫻桃紅",
    25: "莓果紅",
    26: "蜜桃粉",
    28: "橙果橘",
    31: "活力橘",
    32: "咖啡",
    34:"淺咖",
    35: "深咖啡",
    36: "淺駝",
    38: "橙果橘",
    39: "粉橘",
    41: "黃",
    42: "芥末黃",
    43: "淺黃",
    44: "粉嫩橘",
    45: "杏仁白",
    46: "米黃",
    52: "綠",
    51: "綠",
    55: "綠",
    56: "迷彩綠",
    58: "湖水綠",
    59: "樂活綠",
    61: "深藍",
    62: "寶藍",
    63: "灰藍",
    64: "天空藍",
    65: "水藍",
    68: "紳士藍",
    71: "流蘇紫",
    72: "薰衣紫",
    73: "魅惑紫",
    76: "深栗紫",
    78: "奢華紫",
    81: "粉紅",
    82:"粉灰紫",
    83:"淡藍綠",
    85: "靚亮橙",
    86:"櫻花粉",
    87: "淺桔粉",
    111: "神秘黑", 
    113: "時尚灰", 
    114: "北極灰",
    120: "熱情紅",
    121: "玫瑰紅",
    123: "靚桃紅",
    124: "粉嫩桃",
    136: "淺駝色", 
    138: "橙果橘",
    142: "淺鵝黃",
    143: "淡淺黃",
    144: "粉嫩橘",
    145: "淺米白",
    151: "沉穩綠",
    152: "甜粉綠",
    154: "蘋果綠",
    158: "澗水綠", 
    159: "萊姆綠", 
    161: "搖滾藍",
    162: "繽紛藍",
    163: "紳士藍",
    164: "湖水藍", 
    165: "紫戀藍",  
    171: "葡萄紫",
    172: "薰衣紫",
    174: "時尚紫", 
    176: "深栗紫", 
    182: "粉灰紫",
    183: "淡藍綠",
    185: "靚亮橘",
    186: "櫻花粉",
    187: "淺桔粉", 
    188: "薔薇粉",
    211: "神秘黑",
    213: "時尚灰",
    251: "沉穩綠",
    263: "紳士藍",
    311: "神秘黑",
    313: "時尚灰", 
    314: "北極灰",
    320: "熱情紅",
    321: "玫瑰紅",
    323: "靚桃紅", 
    324: "粉嫩桃", 
    336: "淺駝色",
    338: "橙果橘", 
    342: "淺鵝黃", 
    343: "淡淺黃", 
    344: "粉嫩橘", 
    345: "淺米白", 
    352: "甜粉綠", 
    354: "蘋果綠", 
    358: "澗水綠", 
    359: "萊姆綠", 
    361: "搖滾藍", 
    362: "繽紛藍", 
    364: "湖水藍", 
    365: "紫戀藍", 
    371: "葡萄紫", 
    372: "薰衣紫", 
    374: "時尚紫", 
    376: "深栗紫",
    382: "粉灰紫",
    383: "淡藍綠", 
    385: "靚亮橘", 
    386: "櫻花粉",
    387: "淺桔粉", 
    388: "薔薇粉",
    811: "神秘黑",
    813: "時尚灰",
    851: "沉穩綠",
    863: "紳士藍"
}];