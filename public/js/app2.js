$$('.panel-overlay').on('click', function(){
    $$('.panel-left').on('panel:overlay-click', function(){
        myApp.closePanel();
    });
    
});
function reserveString(str){
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) { 
        newString += str[i]; // or newString = newString + str[i];
    }
    return newString;
}
function addingToCart(id, name, img, price, number, size, color, type, discount){
        shoppingCart.addItemToCart(id, name, img, price, number, size, color, type, discount);
        displayCart();
}

$$(".add-to-cart").on('click', function(event){
    event.preventDefault();
    var id = $(this).attr("data-id");
    var price = Number($(this).attr("data-price"));
    var name = $(this).attr("data-name");
    var img = $(this).attr("data-img");
    var size = $(this).attr("data-size");
    //console.log($(this).attr("data-size"));
    var availSize = $(this).attr("data-availsize");
    var discount = $(this).attr("data-discount");
    //console.log(discount);
    if(size==""){
        myApp.alert("請選擇一個尺寸", "選擇尺寸");
        return false;
    }
    var number = $(this).attr("data-number");
    //console.log(this);
    var color = $(this).attr("data-color");

    for(var x=0; x<number; x++){
        addingToCart(id+size, name, img, price, 1, size, color, availSize, discount);
    }

    myApp.closeModal();
    $$('.product_number_choice select').val("1");
    size = "";
});

$("#clear-cart").click(function(event){
    shoppingCart.clearCart();
    displayCart();
});
$('.qty_selection').change(function(){
    var qty = $(this).val();
    $(".add-to-cart").attr("data-number", qty);
})

function updateBadge(){
    if(shoppingCart.countCart() == 0){
        $$('.choosen_items').hide();
    }else{
        $$('.choosen_items').html(shoppingCart.countCart());
        $$('.choosen_items').show();
    }       
}

function displayCart() {
    updateBadge();
    var cartArray = shoppingCart.listCart();
    var newStructureArray = [];
    
    var output = "";
    var order_table = "";
    var confirm_cart = "";
    var parsedID = [];   
    var costumer_price = 0;
    for (var i in cartArray) {

        var L_count = 0, 
        S_count = 0, 
        M_count = 0, 
        XL_count = 0, 
        XXL_count = 0,
        C130_count = 0,
        C140_count = 0,
        ids_total_count = 0, 
        ids_total_price = 0, 
        availSize = "",
        name="",
        color="",
        ret = reserveString(reserveString(cartArray[i].id).replace(reserveString(cartArray[i].size), '')),
        name = cartArray[i].name,
        color = cartArray[i].color,
        price = cartArray[i].price,
        discount = cartArray[i].discount;
        //console.log(ret);
        if(!parsedID.includes(ret)){

            availSize = cartArray[i].type;
            
            for(var j in cartArray){
                
                if(cartArray[j].id.indexOf(ret) !== -1){
                    
                    if(cartArray[j].size == "L"){
                        L_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "M"){
                        M_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "XL"){
                        XL_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "XXL"){
                        XXL_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "S"){
                        S_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "C130"){
                        C130_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }
                    if(cartArray[j].size == "C140"){
                        C140_count+=cartArray[j].count;
                        ids_total_count+=cartArray[j].count;
                        ids_total_price+=cartArray[j].price*cartArray[j].count;
                    }

                }
            }
            var item_by_id = {
                id: ret,
                L_count: L_count,
                S_count: S_count, 
                M_count: M_count, 
                XL_count: XL_count,
                XXL_count: XXL_count, 
                C130_count: C130_count, 
                C140_count: C140_count, 
                ids_total_count: ids_total_count, 
                ids_total_price: ids_total_price,
                availSize: availSize,
                name: name,
                color: color,
                unit_price: price,
                link: "http://www.apure.com.tw/img/product/"+ret+"_l_1.jpg",
                S_code: ret+"S",
                L_code: ret+"L",
                M_code: ret+"M",
                XL_code: ret+"XL",
                XXL_code: ret+"XXL",
                C130_code: ret+"C130",
                C140_code: ret+"C140",
                discount: discount
            };
            newStructureArray.push(item_by_id);

        }
        
        parsedID.push(ret);

    }

    for(var i in newStructureArray){
        
        order_table += "<div class='cart_container' id='"+newStructureArray[i].id+"'> \
        <div class='cart_image'><img src='http://www.apure.com.tw/img/product/groupbuy/"+newStructureArray[i].id+"_l_1.jpg' /> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_s'> \
            <span class='size'>S</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].S_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"S'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].S_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].S_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'S'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_m'> \
            <span class='size'>M</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].M_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"M'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].M_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].M_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'M'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"M'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_l'> \
            <span class='size'>L</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].L_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"L'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].L_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].L_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'L'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"L'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_xl'> \
            <span class='size'>XL</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].XL_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"XL'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].XL_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].XL_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'XL'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"XL'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_xxl'> \
            <span class='size'>XXL</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].XXL_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"XXL'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].XXL_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].XXL_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'XXL'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"XXL'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_c130'> \
            <span class='size'>C130</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].C130_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"C130'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].C130_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].C130_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'C130'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"C130'>&plus;</a> \
        </div> \
        <div class='size_num_plus_minus' id='"+newStructureArray[i].id+"_c140'> \
            <span class='size'>C140</span> \
            <a onclick='reduceItemNumberSize(\""+newStructureArray[i].C140_code+"\")' class='minus' data-id='"+newStructureArray[i].id+"C140'>&minus;</a> \
            <span class='num'>"+newStructureArray[i].C140_count+"</span> \
            <a onclick='addItemNumberSize(\""+newStructureArray[i].C140_code+"__"+newStructureArray[i].name+"__"+newStructureArray[i].color+"__"+newStructureArray[i].unit_price+"__"+'C140'+"__"+newStructureArray[i].availSize+"__"+newStructureArray[i].link+"__"+newStructureArray[i].discount+"\")' class='plus' data-id='"+newStructureArray[i].id+"C140'>&plus;</a> \
        </div> \
        <div class='text'>"+newStructureArray[i].name+"-"+newStructureArray[i].color+" ( ID: "+newStructureArray[i].id+")</div><a class='close_btn' onclick='deleteItemWithAllSizes(\""+newStructureArray[i].id+"__"+newStructureArray[i].availSize+"\")'>X</a> \
        <div class='total_items'>共"+newStructureArray[i].ids_total_count+"件</div> \
        <div class='total_amount'>折扣後$"+Math.ceil(newStructureArray[i].ids_total_price*newStructureArray[i].discount*0.1)+"</div></div>";
        costumer_price += Math.ceil(newStructureArray[i].ids_total_price*newStructureArray[i].discount*0.1);
        
    }
    //console.log(newStructureArray);

    $("#show-cart").html(output);
    $("#count-cart").html( shoppingCart.countCart() );
    $("#total-cart").html( shoppingCart.totalCart() );

    $("#list-cart").html(order_table);
    //$("#confirm-cart").html(confirm_cart);
    $(".cart_container").remove();
    var current_html = "<div class='submenu'><div class='confirm_button'>確認購物車</div><div class='info_button'>訂購及取貨資訊</div><div class='finish_button'>完成</div></div> \
                        <div class='selected_bar'></div><div class='submenu_sep'></div><div class='info_confirm_shop'><ul><li>請注意:</li><li>訂購完成後請與團購主聯繫付款</li><li>為了確保客戶權益，登出後未完成訂購的商品將會於購物車內清空</li></ul></div>";

    var totals = "<div class='price_sep'></div> <div class='total_payment'>"+shoppingCart.countCart()+"件商品<br/><span style='text-decoration:line-through'>$"+ shoppingCart.totalCart() +"</span><br/> 折扣後總計:<span class='price_final'>$"+ costumer_price +"</span></div>";
    
    $("#confirm-cart").html(current_html+order_table+totals);

    // Only show available sizes
    for(var i in newStructureArray){
        //console.log(newStructureArray[i].id);
        var all_sizes = newStructureArray[i].availSize.split(",");
        //console.log(all_sizes);
        if(!all_sizes.includes("S")){
            $$('#'+newStructureArray[i].id+'_s').hide();
        }
        if(!all_sizes.includes("M")){
            $$('#'+newStructureArray[i].id+'_m').hide();
        }
        if(!all_sizes.includes("L")){
            $$('#'+newStructureArray[i].id+'_l').hide();
        }
        if(!all_sizes.includes("XL")){
            $$('#'+newStructureArray[i].id+'_xl').hide();
        }
        if(!all_sizes.includes("XXL")){
            $$('#'+newStructureArray[i].id+'_xxl').hide();
        }
        if(!all_sizes.includes("C130")){
            $$('#'+newStructureArray[i].id+'_c130').hide();
        }
        if(!all_sizes.includes("C140")){
            $$('#'+newStructureArray[i].id+'_c140').hide();
        }
    }
}


$("#show-cart").on("click", ".delete-item", function(event){
    var id = $(this).attr("data-id");
    shoppingCart.removeItemFromCartAll(id);
    displayCart();
});

$("#show-cart").on("click", ".subtract-item", function(event){
    var id = $(this).attr("data-id");
    shoppingCart.removeItemFromCart(id);
    displayCart();
});

$(".size_selection").on("change", "", function(event){
    var size = $(this).val();
    $(".add-to-cart").attr("data-size", size);
    //alert(size);
});


$('.removeItem').click(function(){
    var idToRemove = $('.itemToRemove');
    for(var i=0; i<idToRemove.length; i++){
        var id = $(idToRemove[i]).attr('data-id');
        var type = $(idToRemove[i]).attr('data-type');
        var full_name = $(idToRemove[i]).attr('data-all_name');
        deleteOne(id, type, full_name);
    }
    $('.checkout').show();
    $('.removeItem').hide();
    displayCart();
});

function deleteOne(id, type, full_name){
    // var id = $(this).attr("data-id");
    // var type = $(this).attr("data-type");
    // var full_name = $(this).attr("data-all_name");
    var cartArray = shoppingCart.listCart();
    var number_A = 0;
    var number_B = 0;
    for (var i in cartArray) {
        if(cartArray[i].type == "A") number_A+=cartArray[i].count;
        if(cartArray[i].type == "B") number_B+=cartArray[i].count;
        //console.log(cartArray[i].count);
    }
    //Check if at A choice are more than B choices. If yes
    if(number_A >= number_B + 1){
        shoppingCart.removeItemFromCart(id);
        myApp.alert(full_name, "刪除以下商品");
    }else{
        if(type=="B"){
            shoppingCart.removeItemFromCart(id);
            myApp.alert(full_name, "刪除以下商品");
        }else{
            myApp.alert("Cannot remove this. You have to remove one Pure5.5 first", "Oups");
        }
        
    }

    if(shoppingCart.totalCart() == 0){
       mainView.router.back();
       //$$('.order_navbar').addClass('cached');
        $$('.index_navbar').removeClass('cached');
    }
    
}

displayCart();