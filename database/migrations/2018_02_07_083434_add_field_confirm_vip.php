<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldConfirmVip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        if ( ! Schema::hasColumn('special_users', 'confirmed')){
            Schema::table('special_users', function($table){
                $table->string('confirmed')->default("0");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
