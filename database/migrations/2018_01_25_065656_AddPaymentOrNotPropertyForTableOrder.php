<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentOrNotPropertyForTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        if ( ! Schema::hasColumn('orders', 'payment_status')){
            Schema::table('orders', function($table){
                $table->string('payment_status')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
