<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupBuyDeal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_buy_deal', function (Blueprint $table) {
            $table->increments('id');
            $table->string('from_when');
            $table->string('until_when');
            $table->string('by_who');
            $table->string('minimum_amount_of_items');
            $table->string('minimum_amount_of_money');
            $table->string('activated');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
