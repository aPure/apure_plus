<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Http\Controllers\DBQueriesController;

use App\Http\Controllers\MailController;

use App\Http\Controllers\AdminController;

use GuzzleHttp\Client;

class SpecialCustomersProductsController extends Controller{
    // When update this, also update AdminController version
    public $version = "1.5";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        //
        if($request->session()->has('username')){
            return view('desktop.special_current_customers', ['isDesktop' => 1]);
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function confirm(Request $request){
        $paymment = $request->input('twPaymentInput');
        $name = $request->input('twRpInput');
        $tel = $request->input('twRpTelInput');
        $addr = $request->input('twRecipientsAddressInput');
        //$delivery = $request->input('twRecipientsDeliveryTime');
        
        if($request->session()->has('username')){
            return view('desktop.confirm',
            ['payment'=> $paymment, 'name'=>$name, 'tel'=> $tel, 'address'=>$addr, 'vip'=> $request->session()->get('vip')]);
        }else{
            return view('desktop.login', 
                    ['isDesktop' => 1, 
                    'result'=> 0, 
                    'phoneInDB' => 0, 
                    'passwordNotCorrect' => 0
                    // ,'confirmation' => 1,
                    // 'payment' => $payment,
                    // 'name' => $name,
                    // 'tel' => $tel,
                    // 'addr' => $addr 
                    ]);
        }
    }

    public function order(Request $request){
        if($request->session()->has('username')){
            return view('desktop.order');
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result' => 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function checkout(Request $request){
        $name = $request->input('name');
        $tel = $request->input('tel');
        $address = $request->input('address');
        $payment  = $request->input('payment');
        $delivery  = $request->input('delivery');
        $order = $request->input('order');
        $mobile = $request->input('mobile');
        $account_phone = $request->input('account_phone');
        $total_amount = $request->input('total_amount');
        $buyerAmountToPay = $request->input('buyerAmountToPay');
        $idOfSale = date("Y").date("m").date("d").date("H").date("i").date("s");
        $order_data = json_decode($order);
        $vip = $request->input('vip');
        if(empty($address)){
            $address = "VIP Address";
        }
       
        if(empty($delivery)){
            $delivery = "VIP delivery time";
        }
  
        if(empty($tel)){
            $tel = $request->session()->get('username');
        }
        
        $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0];
        $groupBuyingRequestId = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible->sales_responsible)->get();

        if($mobile == '1'){
            $userId = DB::table('group_buyers')->select('id', 'by_who')
            ->where('cel', $account_phone)
            ->where('group_deal_id', $groupBuyingRequestId[0]->id)->limit(1)->get();
        }else{
            $userId = DB::table('group_buyers')->select('id', 'by_who')
            ->where('group_deal_id', $groupBuyingRequestId[0]->id)
            ->where('cel', $request->session()->get('username'))->limit(1)->get();
        }
        
        // print($userId." and ".$tel." and ".$request->session()->get('phone'));
        $current_date = date("Y-m-d H:i:s");
        
        // $sales_responsible = DB::table('special_users')->select('sales_responsible')->where('username', $userId[0]->by_who)->get()[0];
    
        // $group_buying_id = DB::table('group_buy_deal')->select('id')
        //                             ->where('by_who', $sales_responsible->sales_responsible)
        //                             ->where('activated', '1')
        //                             ->where('from_when', '<=', $current_date)
        //                             ->where('until_when', '>=', $current_date)
        //                             ->get();
        if(count($groupBuyingRequestId) < 1){
            // -2: There is no group deal
            return response()->json([
                'result' => '-2'
            ]);
        }

        $orderId = DB::table('orders')->insertGetId([
            'userId' => $userId[0]->id,
            'orderNumber' => $idOfSale,
            'customerName' => $name,
            'paymentMethod' => $payment,
            'address'=> $address,
            'delivery_preference'=> $delivery,
            'recipientPhone' => $tel,
            'totalAmount' => $total_amount,
            'buyerAmountToPay' => ceil($buyerAmountToPay),
            'group_buying_id' => $groupBuyingRequestId[0]->id,
            'created_at' => $current_date,
            'updated_at' => $current_date
        ]);
        
   
        foreach($order_data as $key => $value){
            DB::table('order_item')->insert([
                'orderId' => $orderId,
                'productId'=> $value->id,
                'price'=> $value->price,
                'number'=>$value->count,
                'size'=>$value->size,
                'productName' => $value->name."-".$value->color,
                'discount' => $value->discount
            ]);
        }
        // Can VIP Checkout
        $percent = AdminController::progress_percent($vip);
        if($percent >= 100){
            MailController::goal_reached($sales_responsible->customer_name, $sales_responsible->email);
        }
        $products_to_check = AdminController::availaibility_check($groupBuyingRequestId[0]->id);
        //return $products_to_check;
        foreach ($products_to_check as $key => $value) {
            if($value->number >= $value->safety && $value->number < $value->max){
                // MailController::safety_reached('aPure', 'customer@apuremail.com', $value->productId, $value->number);
                MailController::safety_reached('依珊', 'suchen@apuremail.com', $value->productId, $value->number);
                MailController::safety_reached('Spencer', 'spencerlin@apuremail.com', $value->productId, $value->number);
        
            }
            if($value->number >= $value->max){
                // MailController::max_reached('aPure', 'customer@apuremail.com', $value->productId, $value->number);
                MailController::max_reached('依珊', 'suchen@apuremail.com', $value->productId, $value->number);
                MailController::max_reached('Spencer', 'spencerlin@apuremail.com', $value->productId, $value->number);
            }
        }

        // Get URLEnc
        $client = new Client();
        $res = $client->get("http://13.113.64.137/credit_card_payment_apure_api/urlenc.php?purchAmnt/".$total_amount."/lidm/".$idOfSale);
        $urlenc = explode("</pre>", $res->getBody());
        if($mobile == "0"){
            if($payment == "快遞貨到付款"){
                return view('desktop.thankyou', ['from_sales' => 0]);
            }else{
                return view('desktop.pay',
                ['total_amount'=> $total_amount, 'numOfOrder' => $idOfSale, 'urlenc' => $urlenc[1]]);
            }
        }else{
            return response()->json([
                'result' => '1',
                'idOfSale' => $idOfSale,
                'urlenc' => $urlenc[1]
            ]);
        }
        
        
    }

    public function getProductsToLoad($group_id){
        $products_to_load = DB::table('products')
                        ->join('deal_products', 'deal_products.product_id', '=', 'products.id')
                        ->select('deal_products.discount', 'products.*')
                        ->where('deal_products.group_buy_deal_id', $group_id)->get();
    
        $products = [];    
        foreach ($products_to_load as $key => $value) {
            $product = [];
            array_push($product, $value->code);
            array_push($product, $value->name);
            array_push($product, $value->colors);
            array_push($product, $value->sizes);
            array_push($product, $value->unit_price);
            array_push($product, $value->type);
            array_push($product, $value->discount);
            array_push($products, $product);
        }
        
        return $products;
    }

    public function sales_login($request, $salesID, $pwd){
        $user = DB::table('special_users')->where('username',  $salesID);
        if($user->count() >=1){
            if($user->get()[0]->password == $pwd){
                // Is VIP
                if($user->get()[0]->sales_responsible != "0" 
                    && $user->get()[0]->sales_responsible != "1"){
                    if($user->get()[0]->confirmed != "0"){
                        $request->session()->put('is_vip', $salesID);
                        $request->session()->put('vip_name',$user->get()[0]->customer_name);
                    }else{
                        return redirect()->route('admin_index', ['result' => 3]);
                    }
                    
                }
                // Is Super Manager
                if($user->get()[0]->sales_responsible == "1"){
                    $request->session()->put('is_super', $salesID);
                }
                
                // Is Sales Manager
                $request->session()->put('admin_code',$salesID);
                return redirect()->route('admin_index');

            }else{
                return view('desktop.login', ['isDesktop' => 1, 'result' => 1, 'phoneInDB' => 1, 'passwordNotCorrect' => 0]);
            }
        }
        
        return view('desktop.login', ['isDesktop' => 1,'result' => 1, 'phoneInDB' => 1, 'passwordNotCorrect' => 0]);
    
    }

    public function login(Request $request){
        $isSalesID = $request->input('isSalesID');
        $salesID = $request->input('salesID');
        $phone = $request->input('phone');
        $pwd = $request->input('password');
        $mobile = $request->input('mobile');
        
        $current_date = date("Y-m-d H:i:s");
        
        $user = DB::table('group_buyers')
        ->join('special_users', 'group_buyers.by_who', '=', 'special_users.username')
        ->join('group_buy_deal', 'group_buy_deal.id', '=', 'group_buyers.group_deal_id')
        ->select('group_buyers.*', 'special_users.customer_name')
        ->where('group_buyers.cel', $phone)
        ->where('group_buy_deal.activated', '1')
        ->where('group_buy_deal.from_when', '<=', $current_date)
        ->where('group_buy_deal.until_when', '>=', $current_date);
       
        if($mobile == "1"){
        if($isSalesID == "0"){
            if($user->count() >= 1){
                if($user->get()[0]->password == $pwd){
                    $group_id = $user->get()[0]->group_deal_id;
                    $products_html = $this->getProductsToLoad($group_id);
                    
                    return response()->json([
                        'result' => '1',
                        'vip_name' => $user->get()[0]->customer_name,
                        'vip' => $user->get()[0]->by_who,
                        'user_name' => $user->get()[0]->name,
                        'productshtml' => $products_html,
                        'version' => $this->version,
                        'group_id' => $group_id
                    ]);
                }else{
                    return response()->json([
                        'result' => '0'
                    ]);
                }
            }else{ // no user
                return response()->json([
                    'result' => '-1'
                ]);
            }
        }else{ // sales access from mobile
            return $this->sales_login($request, $salesID , $pwd);
        }
    }else{ // desktop
            if($isSalesID == "1"){ // user exists
                return $this->sales_login($request, $salesID , $pwd);
            }else{ // no sales login -- nothing will appear

            }
        }
    }
    public function logout(Request $request){
        if($request->input('from_sales') == '1'){
            $request->session()->forget('admin_code');
            
            $request->session()->forget('is_vip');
            
            $request->session()->forget('is_super');

            return redirect()->route('admin_index');
        }else{
            $request->session()->forget('admin_code');
            $request->session()->forget('username');
            $request->session()->forget('vip');
            $request->session()->forget('vip_name');
            $request->session()->forget('is_vip');
            
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
        
    }

    public function get_mobile_data(Request $request){
        $payment = $request->input('payment');
        $name = $request->input('name');
        $tel = $request->input('tel');
        $address = $request->input('address');

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
