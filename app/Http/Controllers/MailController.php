<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public static function goal_reached($name, $email){
        $data = array(
            "name" =>$name, 
            "email" => $email);

        Mail::send('mail.goal_reached', $data, function($message) use ($data){

            $message->to($data['email'], $data['name'])

                    ->subject('aPure團購成團通知');

            $message->from('customer@apuremail.com','aPure');

        });

    }

    public static function safety_reached($name, $email, $product, $number){
        $data = array(
            "name" =>$name, 
            "email" => $email,
            "product" => $product,
            "number"=> $number);

        Mail::send('mail.safety_reached', $data, function($message) use ($data){

            $message->to($data['email'], $data['name'])

                    ->subject('aPure團購成團通知');

            $message->from('customer@apuremail.com','aPure');

        });

        
    }

    public static function max_reached($name, $email, $product, $number){
        $data = array(
            "name" =>$name, 
            "email" => $email,
            "product" => $product,
            "number" => $number);

        Mail::send('mail.max_reached', $data, function($message) use ($data){

            $message->to($data['email'], $data['name'])

                    ->subject('aPure團購成團通知');

            $message->from('customer@apuremail.com','aPure');

        });

    }

}
