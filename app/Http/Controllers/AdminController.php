<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use GuzzleHttp\Client;

use Illuminate\Routing\UrlGenerator;

use App\Http\Controllers\DBQueriesController;

use Maatwebsite\Excel\Facades\Excel;

class AdminController extends Controller{
    // When update this, also update SpecialCustomersProductsController version 
    public $version = "1.5";

    public function index(Request $request){
        if($request->session()->has('admin_code')){
            if($request->session()->has('is_vip')){
                
                $vip = $request->session()->get('admin_code');
                $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
                
                $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
                $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;
                $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
                if(count($groupBuyingRequest) >=1){
                    $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
                    $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
                }else{
                    $amount_toSpend = "-";
                    $itemsNumberShouldBe = "-";
                }
                $customers = DB::table('group_buyers')->select('id', 'name', 'cel', 'created_at')
                    ->where('by_who', $request->session()->get('admin_code'))
                    ->where('group_deal_id', $groupBuyingRequest[0]->id)
                    ->orderBy('updated_at', 'desc');
                $itemsNumber = DBQueriesController::getItemsNumber($vip);
                if(count($ordersRequest->get()) >= 1){
                    return view('desktop.sales.users_list', 
                        ['isDesktop' => 1, 
                        'customers' => $customers->get(), 
                        'number_my_customers' => $customers->count(), 
                        'amount_spent' => $amount_spent, 
                        'amount_toSpend' => $amount_toSpend,
                        'progress' => $this->progress_percent($vip), 
                        'itemsNumber' => $itemsNumber,
                        'time_left' => DBQueriesController::getTimeLeft($vip),
                        'itemsNumberShouldBe' => $itemsNumberShouldBe]);
            
                }else{
                    return view('desktop.sales.users_list', 
                    ['isDesktop' => 1, 
                    'customers' => $customers->get(), 
                    'number_my_customers' => 0, 
                    'amount_spent' => $amount_spent,
                    'progress' => $this->progress_percent($vip),  
                    'amount_toSpend' => $amount_toSpend,
                    'itemsNumber' => $itemsNumber,
                    'time_left' => DBQueriesController::getTimeLeft($vip),
                    'itemsNumberShouldBe' => $itemsNumberShouldBe]);
                }
            }else if($request->session()->has('is_super')){
                $customers = DB::table('special_users')
                    ->where('confirmed', "0")
                    ->whereNotIn('sales_responsible', ["0"])
                    ->orderBy('updated_at', 'desc');
                return view('desktop.sales.users_list', ['isDesktop' => 1, 'customers' => $customers->get(), 'number_my_customers' => $customers->count()]);
            }else{
                $customers = DB::table('special_users')
                ->select('id', 'username', 'customer_name', 'created_at', 'confirmed')
                ->where('sales_responsible', $request->session()->get('admin_code'))
                ->orderBy('updated_at', 'desc');
                return view('desktop.sales.users_list', ['isDesktop' => 1, 'customers' => $customers->get(), 'number_my_customers' => $customers->count()]);
            }
            
        }else{
            if($request->input('passwordWrong') == 1){
                return view('desktop.login', ['isDesktop' => 1, 'result'=> 1, 'phoneInDB' => 1, 'passwordNotCorrect' => 0]);
            }else if($request->input('result') == "3"){
                return view('desktop.login', ['isDesktop' => 1, 'result'=> 3, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
            }else{
                return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
            }
            
        }
    }
    
    public function download_excel_summary(Request $request){
        
        $admin_code = $request->session()->get('admin_code');
        $vips = [];
        foreach(DBQueriesController::getSalesMyVIPs($admin_code)->get() as $key => $value){
            array_push($vips, $value->username);
        }
        $vip_payments = DB::table('group_buying_payments')->
            select('paymentNumber', 'vip', 'address', 'orderIds', 'customer_name')
            ->leftJoin('special_users', 'special_users.username', '=', 'group_buying_payments.vip')
            ->whereIn('vip', $vips)->orderBy('group_buying_payments.updated_at', 'desc')->get();
        
        //return $vip_payments;
        //$data = $vip_payments->toArray();
        // $data = [1, 2, 7, 12, 8, 3];
        Excel::create('payments', function($excel) use ($vip_payments) {

            $excel->sheet('sheet1', function($sheet) use ($vip_payments) {
                //$sheet->fromArray($data);
                $sheet->cell('A1', function($cell){ $cell->setValue('主單編號'); });
                $sheet->cell('B1', function($cell){ $cell->setValue('收件人'); });
                $sheet->cell('C1', function($cell){ $cell->setValue('收件人電話'); });
                $sheet->cell('D1', function($cell){ $cell->setValue('地址'); });
                $sheet->cell('E1', function($cell){ $cell->setValue('商品名稱'); });
                $sheet->cell('F1', function($cell){ $cell->setValue('商品料號'); });
                $sheet->cell('G1', function($cell){ $cell->setValue('數量'); });
                $sheet->cell('H1', function($cell){ $cell->setValue('商品單價'); });
                $sheet->cell('I1', function($cell){ $cell->setValue('銷售金額(折扣後)'); });
                //$sheet->cell('A2', '4');

                if (!empty($vip_payments)) {
                    $count_rows = 1;
                    foreach ($vip_payments as $key => $value) {
                        $i= $key + 2;
                        
                        $ordersIds = explode("-", $value->orderIds);
                        $itemsBought = DB::table('order_item')->
                                select('productName', 'productId', 'price', 'number', 'discount')->
                                whereIn('orderId', $ordersIds)->orderBy('order_item.updated_at', 'desc')->get();
                        $count_rows = $count_rows - 1;        
                        foreach ($itemsBought as $k => $val){
                            $j= $i + $count_rows;
                            $count_rows++;
                            $sheet->cell('A'.$j, $value->paymentNumber); 
                            $sheet->cell('B'.$j, $value->customer_name); 
                            $sheet->cell('C'.$j, $value->vip);
                            $sheet->cell('D'.$j, $value->address);
                            $sheet->cell('E'.$j, $val->productName); 
                            $sheet->cell('F'.$j, $val->productId); 
                            $sheet->cell('G'.$j, $val->number);
                            $sheet->cell('H'.$j, $val->price);
                            $sheet->cell('I'.$j, ceil($val->number*$val->price*$val->discount*0.1));
                        }
                    }
                }

            });

            
        })->download('xlsx');
    }

    public function download_list_customers(Request $request){
        $vip = $request->session()->get('admin_code');
        $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->username;
        $group_buying_id = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get()[0]->id;
        
        $customers = DB::table('group_buyers')->
            select('name', 'cel', 'by_who')
            ->where('group_deal_id', $group_buying_id)->get();
        
        Excel::create('customers', function($excel) use ($customers) {

            $excel->sheet('sheet1', function($sheet) use ($customers) {
                //$sheet->fromArray($data);
                $sheet->cell('A1', function($cell){ $cell->setValue('Name'); });
                $sheet->cell('B1', function($cell){ $cell->setValue('Cel'); });
                $sheet->cell('C1', function($cell){ $cell->setValue('Added by'); });

                if (!empty($customers)) {
                    foreach ($customers as $key => $value) {
                        $j= $key + 2;
                        
                        $sheet->cell('A'.$j, $value->name); 
                        $sheet->cell('B'.$j, $value->cel); 
                        $sheet->cell('C'.$j, $value->by_who);
                    
                    }
                }

            });

            
        })->download('xlsx');
    }

    public function print($paymentId, $vip){
        $vipName = DB::table('special_users')->where('username', $vip)->get()[0]->customer_name;
        $payment = DB::table('group_buying_payments')->where('paymentNumber', $paymentId)->get()[0]->orderIds;
        $oneOrder = explode("-", $payment)[0];
        $group_buying_id = DB::table('orders')->where('id', $oneOrder)->get()[0]->group_buying_id;
        $group_until_when = DB::table('group_buy_deal')->where('id', $group_buying_id)->get()[0]->until_when;
    
        $until_when = date('Ymd', strtotime($group_until_when));
        $printInfo = $this->paymentDetails($paymentId);
        foreach($printInfo as $key => $value){
            $printInfo[$key]['vipName'] = $vipName;
            $printInfo[$key]['until_when'] = $until_when;
        }
        
        //return $printInfo;
        return response()->json($printInfo);
    }
    public function mobile_data_fetch(Request $request){
        $vip = $request->input('vip');
        $customers = DB::table('group_buyers')->select('id', 'name', 'cel', 'created_at')->where('by_who', $vip)->orderBy('updated_at', 'desc');
        $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
        $order_history = $ordersRequest->orderBy('updated_at', 'desc')->get();
        $order_details = [];
        foreach ($order_history as $key => $value) {
            array_push($order_details, DB::table('order_item')
                ->where('orderId', $value->id)
                ->orderBy('updated_at', 'desc')->get());
        }
        
        $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
        $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
        
        $itemsNumber = DBQueriesController::getItemsNumber($vip);

        return response()->json([
            'customers' => $customers->get(),
            'orders' => $order_history,
            'orders_details' => $order_details,
            'amount_spent' => $amount_spent,
            'group_buying' => $groupBuyingRequest,
            'itemsNumber' => $itemsNumber
        ]);
    }
    public function vip_checkout(Request $request){
        if($request->session()->has('admin_code')){
            $previous = explode("/", url()->previous());
            $vip = $request->session()->get('admin_code');
            $totalVIP = $request->input('totalVIP');
            $totalBuyer = $request->input('totalBuyer');
            $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
            $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
            $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

            $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
            $after_discount = ceil(floatval($amount_spent) * floatval($groupBuyingRequest[0]->discount)*10/100);
            //return [$amount_spent, $after_discount, floatval($groupBuyingRequest[0]->discount)];
            if(end($previous) == "order_history"){
                $request->session()->put('is_paying', '1');
                return view('desktop.sales.checkout', 
                            ['itemsNumber'=>0, 
                            'itemsNumberShouldBe' => 0 ,
                            'progress' => $this->progress_percent($vip),  
                            'amount_to_pay' => $totalVIP, 
                            'discount' => $groupBuyingRequest[0]->discount,
                            'after_discount' => $totalBuyer, 'prev' => $previous]);
            }else{
                return redirect()->route('order_history');
            }
            
        }else{
            return redirect()->route('admin_index');
        }
    }

    public function vip_payments(Request $request){
        if($request->session()->has('admin_code')){
            if($request->session()->has('is_vip')){
                $customers = DB::table('group_buyers')->select('id', 'name', 'cel', 'created_at')->where('by_who', $request->session()->get('admin_code'))->orderBy('updated_at', 'desc');
                $vip = $request->session()->get('admin_code');
                $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
                
                $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
                $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

                $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
                if(count($groupBuyingRequest) >=1){
                    $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
                    $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
                }else{
                    $amount_toSpend = "-";
                    $itemsNumberShouldBe = "-";
                }
                
                $vip_payments = DB::table('group_buying_payments')
                    ->where('vip', $vip)->orderBy('updated_at', 'desc')->get();
                    
                return view('desktop.sales.vip_payments', [
                    'vip_payments' => $vip_payments,
                    'number_my_customers' => $customers->count(), 
                    'amount_spent' => $amount_spent, 
                    'progress' => $this->progress_percent($vip), 
                    'amount_toSpend' => $amount_toSpend,
                    'itemsNumber' => DBQueriesController::getItemsNumber($vip),
                    'time_left' => DBQueriesController::getTimeLeft($vip),
                    'itemsNumberShouldBe' => $itemsNumberShouldBe]);
            }else{
                $admin_code = $request->session()->get('admin_code');
                $vips = [];
                foreach(DBQueriesController::getSalesMyVIPs($admin_code)->get() as $key => $value){
                    array_push($vips, $value->username);
                }
                $vip_payments = DB::table('group_buying_payments')
                    ->whereIn('vip', $vips)->orderBy('updated_at', 'desc')->get();
                    
                    
                return view('desktop.sales.vip_payments', ['vip_payments' => $vip_payments]);
            }
            
        }else{
            return redirect()->route('admin_index');
        }
    }

    public function confirm_vip(Request $request){
        $ids = $request->input('ids');
        $ids_array = explode('-', $ids);
        //return $ids_array;
        DB::table('special_users')->whereIn('id', $ids_array)->update(['confirmed' => '1' ]);
        return redirect()->route('admin_index');

    }

    public function group_buy_list(Request $request){
        if($request->session()->has('admin_code')){
            $group_buy_deals = DB::table('group_buy_deal')->where('by_who', $request->session()->get('admin_code'))->orderBy('updated_at', 'desc')->get();
            return view('desktop.sales.group_buy_list', ['isDesktop' => 1, 'group_buy_deals' => $group_buy_deals]);
        }else{
            return redirect()->route('admin_index');
        }
    }

    public function renew(Request $request){
        if($request->session()->has('admin_code')){
            return view('desktop.sales.renew_password', ['resp' => 0]);
        }else{
            return redirect()->route('admin_index');
        }  
    }

    public function new_deal(Request $request){
        if($request->session()->has('admin_code')){
            return view('desktop.sales.new_deal',[
                'result' => 'ok'
            ]);
        }else{
            return redirect()->route('admin_index');
        }  
    }

    public function create_new_deal(Request $request){
        if($request->session()->has('admin_code')){
            $from_when = $request->input('from_when');
            $from_when.=" 00:00:00";
            $until_when = $request->input('until_when');
            $until_when.=" 23:59:59";
            $discount = $request->input('discount');
           
            if(empty($discount)){
                return view('desktop.sales.new_deal', [
                    'result' => 'no_discount'
                ]);
            }
            // deal_product format is: product_id,discount;
            $deal_products = $request->input('deal_products');
            $minimum_amount_of_items = $request->input('minimum_amount_of_items');
            $minimum_amount_of_money = $request->input('minimum_amount_of_money');
            $activate = $request->input('activate');
            if($activate == NULL){
                $activate = "0";
            }else{
                $activate = "1";
            }
            $new_group_id = DB::table('group_buy_deal')->insertGetId(
                ['from_when' => $from_when, 
                'until_when' =>  $until_when, 
                'minimum_amount_of_items' => $minimum_amount_of_items,
                'minimum_amount_of_money' => $minimum_amount_of_money,
                'by_who' => $request->session()->get('admin_code'),
                'discount' => $discount,
                'activated' => $activate,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
            $deal_products = substr($deal_products, 0, -1);
            $products_chosen = explode(";", $deal_products);
            foreach ($products_chosen as $key => $value) {
                $product_and_discount = explode( ",", $value);
                
                DB::table('deal_products')->insert(
                    ['group_buy_deal_id' => $new_group_id,
                    'product_id' =>  $product_and_discount[0], 
                    'discount' =>  $product_and_discount[1]
                ]);
            }
            // Get the excel file and extract the data
            //$test;
            if($request->hasFile('products_availaibilities')){
                $data = Excel::load($request->file('products_availaibilities')->getRealPath())->get();
                //return $data[0];
                foreach ($data[0] as $key => $row) {  
                    // $data['safety'] = $row['safety'];
                    // $data['max'] = $row['max'];
                    // $data['code'] = $row['code'];
                    // $data['group_buy_deal_id'] = $new_group_id;
                    if(!empty($data)){
                        DB::table('availabilities')->insert([
                            'safety' => $row['safety'],
                            'max' => $row['max'],
                            'code'=> $row['code'],
                            'group_buy_deal_id' => $new_group_id
                        ]);
                    }
                }
                //return $data;
                //DB::table('availabilities')->insert($row);
            }
            //return $test;
            // get all vip of this sale and create group_buyer with group_deal_id
            $all_vips = DB::table('special_users')
                ->where('sales_responsible', $request->session()->get('admin_code'))->get();
            foreach($all_vips as $k => $v){
                $password = DB::table('group_buyers')
                    ->where('cel', $v->username)->orderBy('updated_at', 'desc')->limit(1)->get()[0]->password;
                DB::table('group_buyers')->insert(
                    ['cel' => $v->username, 
                    'name' =>  $v->customer_name, 
                    'password' => $password,
                    'by_who' => $v->username,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'group_deal_id' => $new_group_id
                ]);
            }      
            return redirect()->route('group_buy');
        }else{
            return redirect()->route('admin_index');
        }
        
    }

    public function order_details($buyerId, Request $request){
        if($request->session()->has('admin_code')){
            $vip = $request->session()->get('admin_code');
            $vip_my_customers = DBQueriesController::getVIPMyCustomers($vip);
            $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
            $userOrders = $ordersRequest->where('userId', $buyerId)->orderBy('updated_at', 'desc')->get();
                        
            $order_details = [];
            foreach ($userOrders as $key => $value) {
                $userItems = DB::table('order_item')
                                ->where('orderId', $value->id)->get();
                foreach ($userItems as $key => $value) {
                    array_push($order_details,$value);
                }
            }
            //return $order_details;
            $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
            $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

            $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
    
            if(count($groupBuyingRequest) >=1){
                $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
                $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
            }else{
                $amount_toSpend = "-";
                $itemsNumberShouldBe = "-";
            }
            
            $itemsNumber = DBQueriesController::getItemsNumber($vip);
            return view('desktop.sales.order_details', 
            ['isDesktop' => 1, 
            'order_details' => $order_details,
            'number_my_customers' => $vip_my_customers->count(), 
            'amount_spent' => $amount_spent,
            'progress' => $this->progress_percent($vip), 
            'amount_toSpend' => $amount_toSpend,
            'itemsNumber' => $itemsNumber,
            'time_left' => DBQueriesController::getTimeLeft($vip),
            'itemsNumberShouldBe' => $itemsNumberShouldBe, 
            'buyerId' => $buyerId]);
        }else{
            return redirect()->route('admin_index');
        }
        
    }

    public function vip_checkout_to_ctbk(Request $request){
        if($request->session()->has('admin_code')){
            $payment = $request->input('twPaymentInput');
            $address = $request->input('address');
            $delivery_time = $request->input('delivery_time');
            $amount_to_pay = $request->input('amount_to_pay');
            $vip = $request->session()->get('admin_code');
            $idOfSale = date("Y").date("m").date("d").date("H").date("i").date("s");
            if(empty($address)){
                $address = " None";
            }
            $current_date = date("Y-m-d H:i:s");
            $ordersId = [];
            $totalAmount = 0;
            foreach(DBQueriesController::getVIPOrdersRequest($vip)->get() as $key => $value){
                array_push($ordersId, $value->id);
                $totalAmount += $value->totalAmount;
            }
            DB::table('group_buying_payments')->insert([
                'vip' => $vip,
                'delivery_time' => $delivery_time,
                'how' => $payment,
                'paymentNumber' => $idOfSale,
                'howmuch' => $amount_to_pay,
                'address'=> $address,
                'orderIds' => implode("-", $ordersId),
                'created_at' => $current_date,
                'updated_at' => $current_date,
                'before_discount' => $totalAmount
            ]);
            DB::table('orders')->whereIn('id', $ordersId)->update(['payment_status' => '1' ]);
            $request->session()->forget('is_paying');
            if($payment == "快遞貨到付款"){
                return view('desktop.thankyou', ['from_sales' => 1]);
            }
            if($payment == "信用卡線上付款"){
                // Get URLEnc
                $client = new Client();
                $res = $client->get("http://13.113.64.137/credit_card_payment_apure_api/urlenc.php?purchAmnt/".$amount_to_pay."/lidm/".$idOfSale);
                $urlenc = explode("</pre>", $res->getBody());
                return view('desktop.pay',
                    ['total_amount'=> $amount_to_pay, 
                    'numOfOrder' => $idOfSale, 
                    'urlenc' => $urlenc[1]]);
            }
        }else{
            return redirect()->route('admin_index');
        }
         
    }

    public function order_history(Request $request){
        if($request->session()->has('admin_code')){
            $vip = $request->session()->get('admin_code');
            $vip_my_customers = DBQueriesController::getVIPMyCustomers($vip);
            $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
            //return $ordersRequest->toSql().implode($ordersRequest->getBindings(), ",");
            $order_history = DBQueriesController::getVIPMyCustomersOrdersRequest($vip)->get();
            
            //return $totals;
            $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
            $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

            $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
    
            if(count($groupBuyingRequest) >=1){
                $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
                $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
            }else{
                $amount_toSpend = "-";
                $itemsNumberShouldBe = "-";
            }
            
            $totals = [0, 0];
            $totals[1] = $amount_spent;
            foreach ($order_history as $key => $value) {
                $totals[0]+=$value->totalForVIP;
                //$totals[1]+=$value->totalForBuyer;
            }

            $itemsNumber = DBQueriesController::getItemsNumber($vip);
            return view('desktop.sales.order_history', 
                ['isDesktop' => 1, 
                'totals' => $totals, 
                'order_history' => $order_history, 
                'number_my_customers' => $vip_my_customers->count(), 
                'amount_spent' => $amount_spent, 
                'progress' => $this->progress_percent($vip), 
                'amount_toSpend' => $amount_toSpend,
                'itemsNumber' => $itemsNumber,
                'time_left' => DBQueriesController::getTimeLeft($vip),
                'itemsNumberShouldBe' => $itemsNumberShouldBe]); 
        }else{
            return redirect()->route('admin_index');
        }
        
    }

    public function change_deal_activated(Request $request){
        DB::table('group_buy_deal')->where('id', $request->input('idofvip'))->update(['activated' => $request->input('current_state')?0:1 ]);
        return redirect()->route('group_buy');
    }

    public function add_group_buyers(Request $request){
       if($request->session()->has('admin_code') || $request->input('mobile')=='1'){
            $phoneToAdd = $request->input('phone');
            $nameToAdd = $request->input('name');
            $mobile = $request->input('mobile');
            if(!empty($mobile) && $mobile == '1'){
                $vip = $request->input('vip_phone');
            }else{
                $vip = $request->session()->get('admin_code');
            }
            
            
            $vip = $request->session()->get('admin_code');
                
            $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;
            $groupBuyingRequestId = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get()[0]->id;
            //$customer = DB::table('group_buyers')->where('cel', $phoneToAdd);
            $customer = DB::table('group_buyers')->select('id', 'name', 'cel', 'created_at')
                    ->where('cel', $phoneToAdd)
                    ->where('group_deal_id', $groupBuyingRequestId);

            if($customer->count() == 0 && !empty($phoneToAdd) && !empty($nameToAdd)){
                $number_total_added = DB::table('group_buyers')
                    ->where('by_who', $vip)
                    ->where('group_deal_id', $groupBuyingRequestId)
                    ->count();
                if($number_total_added <= 9){
                    DB::table('group_buyers')->insert(
                        ['cel' => $phoneToAdd, 
                        'name' =>  $nameToAdd, 
                        'password' => $phoneToAdd,
                        'by_who' => $request->session()->get('admin_code'),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'group_deal_id' => $groupBuyingRequestId
                    ]);
                    if(!empty($mobile) && $mobile == '1'){
                        return response()->json([
                            'success' => '1'
                        ]);
                    }else{
                        return redirect()->route('admin_index');
                    }
                    
                }
                else{
                    // -2 : You reached your maximum number of customers you can add
                    if(!empty($mobile) && $mobile == '1'){
                        return response()->json([
                            'success' => '0',
                            'reason' => '-2'
                        ]);
                    }else{
                        return view('desktop.sales.addvip', [
                            'resp' => -2,
                            'number_my_customers' => 9,
                            'itemsNumber' => 0,
                            'amount_spent' => 0,
                            'progress' => $this->progress_percent($vip), 
                            'amount_toSpend' => 0,
                            'time_left' => 0,
                            'itemsNumberShouldBe' => 0]);
                    }
                     
                }
                
            }elseif($customer->count() > 0){
                // -3 : Is alredy group buyer
                return view('desktop.sales.addvip', [
                    'resp' => -3,
                    'number_my_customers' => 0,
                    'itemsNumber' => 0,
                    'amount_spent' => 0,
                    'progress' => $this->progress_percent($vip), 
                    'amount_toSpend' => 0,
                    'time_left' => 0,
                    'itemsNumberShouldBe' => 0]);
            }else{
                // -4 : you didnt type anything
                return view('desktop.sales.addvip', [
                    'resp' => -4,
                    'number_my_customers' => 0,
                    'itemsNumber' => 0,
                    'amount_spent' => 0,
                    'progress' => $this->progress_percent($vip), 
                    'amount_toSpend' => 0,
                    'time_left' => 0,
                    'itemsNumberShouldBe' => 0]);
            }
            
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function addvip(Request $request){
        if($request->session()->has('admin_code')){
            return view('desktop.sales.addvip', ['resp' => 0]);
        }else{
            return redirect()->route('admin_index');
        }
    }

    public function submitnewvip(Request $request){
        if($request->session()->has('admin_code')){
            $phoneToAdd = $request->input('phone');
            $nameToAdd = $request->input('name');
            $email = $request->input('email');
            // $customer = DB::table('users')->where('cel', $phoneToAdd);
            // if($customer->count() >= 1){
            $IsVip = DB::table('special_users')->where('username', $phoneToAdd)->count();
            if($IsVip >= 1){
                // -2 : Is already VIP Customer
                return view('desktop.sales.addvip', ['resp' => -2]);
            }else{
                DB::table('special_users')->insert(
                    ['username' => $phoneToAdd, 
                    'sales_responsible' => $request->session()->get('admin_code'), 
                    'password' => $phoneToAdd,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'customer_name' => $nameToAdd,
                    'email' => $email
                ]);
        
                $groupBuyingRequestId = DBQueriesController::getGroupBuyingRequest($request->session()->get('admin_code'), $request->session()->get('admin_code'))->get()[0]->id;
                
                DB::table('group_buyers')->insert(
                    ['cel' => $phoneToAdd, 
                    'name' =>  $nameToAdd, 
                    'password' => $phoneToAdd,
                    'by_who' => $phoneToAdd,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'group_deal_id' => $groupBuyingRequestId
                ]);
                
                return redirect()->route('admin_index');
            }
                
            // }else{
            //     // -1 : Is not Old Customer
            //     return view('desktop.sales.addvip', ['resp' => -1]);
            // }
            
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function renewpass(Request $request){
        $old = $request->input('old_pass');
        $new = $request->input('new_pass');
        $ispecialuser = $request->input('isspecialuser');
        $ismobile = $request->input('ismobile');
        if($ismobile){
            $username = $request->input('phone');
        }else{
            $username = $request->session()->get('admin_code');
        }
        if($ispecialuser){
            $pass = DB::table('special_users')->where('username', $username)->get();
            if($pass[0]->password == $old & !$ismobile){
                DB::table('special_users')->where('username', $request->session()->get('admin_code'))->update(['password' => $new]);
                return redirect()->route('admin_index');
            }else if ($pass[0]->password == $old & $ismobile){
                DB::table('group_buyers')->where('cel', $username)->update(['password' => $new]);
                return response()->json([
                    'result' => 0 
                ]);
            }else if($pass[0]->password != $old & $ismobile){
                return response()->json([
                    'result' => -1 
                ]);
            }else{
                return view('desktop.sales.renew_password', ['resp' => -1]);
            }
        }else{
            $pass = DB::table('group_buyers')->where('cel', $username)->get();
            //return $pass;
            if($pass[0]->password == $old & !$ismobile){
                // DB::table('group_buyers')->where('cel', $username)->update(['password' => $new]);
                // return response()->json([
                //     'result' => 0 
                // ]);
            }else if ($pass[0]->password == $old & $ismobile){
                DB::table('group_buyers')->where('cel', $username)->update(['password' => $new]);
                return response()->json([
                    'result' => 0 
                ]);
            }else if($pass[0]->password != $old & $ismobile){
                return response()->json([
                    'result' => -1 
                ]);
            }else{
                return view('desktop.sales.renew_password', ['resp' => -1]);
            }
        }
        
    }

    public function remove_group_deal_vip(Request $request){
        if($request->session()->has('admin_code')){
            $idToDelete = $request->input('idofvip');
            
            DB::table('group_buy_deal')->where('id', $idToDelete)->delete();
            
            return redirect()->route('group_buy');
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function removevip(Request $request){
        if($request->session()->has('admin_code')){
            $idToDelete = $request->input('idofvip');
            if($request->session()->has('is_vip')){
                DB::table('group_buyers')->where('id', $idToDelete)->delete();
            }else{
                $vipCel = DB::table('special_users')->where('id', $idToDelete)->get()[0]->username;
                DB::table('group_buyers')->where('cel', $vipCel)->delete();
                DB::table('special_users')->where('id', $idToDelete)->delete();
            }
            
            return redirect()->route('admin_index');
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function mobile_progress_bar(Request $request){
        $vip = $request->input('vip');
        $buyer = $request->input('buyer');
        $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip); 
        $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
        $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;
        
        $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
        $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);

        $from_when = date('m/d', strtotime($groupBuyingRequest[0]->from_when));
        $until_when = date('m/d', strtotime($groupBuyingRequest[0]->until_when));
        
        $buyer_orders = DBQueriesController::getBuyerOrders($buyer, $vip);
        
        $buyer_payment = DBQueriesController::getPayments($vip, $buyer);
        return response()->json([
            'progress' => $this->progress_percent($vip),
            'amount_to_spend' => $amount_toSpend,
            'amount_spent' => $amount_spent,
            'from_when' => "".$from_when,
            'until_when' => "".$until_when, 
            'version' => $this->version,
            'orders_items' => $buyer_orders,
            'buyer_payment' => $buyer_payment
        ]);
    }

    public static function progress_percent($vip){
        $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
        $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;
        $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();   
        $amount_spent_with_discount = DBQueriesController::getAmountSpentWithDiscount($ordersRequest->get());
        $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
        $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
        
        $itemsNumber = DBQueriesController::getItemsNumber($vip);

        $percentAmount = ($amount_spent_with_discount/$amount_toSpend > 1) ? 1 : $amount_spent_with_discount/$amount_toSpend;
        $percentItems = ($itemsNumber/$itemsNumberShouldBe > 1) ? 1 : $itemsNumber/$itemsNumberShouldBe;
        $progress = $percentAmount*0.5 + $percentItems*0.5;
        return floor(round($progress, 2)*100);
    }

    public function remove_order_item(Request $request){
        if($request->session()->has('admin_code')){
            $order_item = DB::table('order_item')->where('id', $request->input('idofitem'));
            $concerned_order_id = $order_item->get()[0]->orderId;
            $concerned_item_price = $order_item->get()[0]->price;
            $concerned_item_number= $order_item->get()[0]->number;
            $concerned_item_discount = $order_item->get()[0]->discount;
            $order = DB::table('orders')->where('id', $concerned_order_id);
            $order_item->delete();
            // update totalAmount and buyerAmount
            $order->update(['totalAmount' => $order->get()[0]->totalAmount - $concerned_item_price*$concerned_item_number]);
            $order->update(['buyerAmountToPay' => $order->get()[0]->buyerAmountToPay - $concerned_item_price*$concerned_item_number*$concerned_item_discount*0.1]);
            return redirect()->route('order_details', ['buyerId'=> $request->input('buyerId')]);
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public function paymentDetails($paymentOrderNumber){
        $orders = DB::table('group_buying_payments')->select('orderIds')->where('paymentNumber', $paymentOrderNumber)->get();
            $ordersIDs = explode("-", $orders[0]->orderIds);
            $orderedItems = [];
            
            $group_by_buyer = [];

            foreach ($ordersIDs as $value) {
                $orderAuthor = DB::table('orders')
                ->where('id', $value)->get()[0]->customerName;
                array_push($group_by_buyer, $orderAuthor);
            }
            $group_by_buyer = array_unique($group_by_buyer);
            
            foreach ($group_by_buyer as $name){
                $ordersOfBuyer = DB::table('orders')
                ->select('id')
                ->whereIn('id', $ordersIDs)
                ->where('customerName', $name)->get();
                $ordersByOneBuyer = [];
                foreach ($ordersOfBuyer as $value){
                    $oneOrderItems = DB::table('order_item')
                    ->where('orderId', $value->id)->get();
                    foreach($oneOrderItems as $value2){
                        array_push($ordersByOneBuyer, $value2);
                    }
                }
                
                array_push($orderedItems, [
                    'buyerName' => $name,
                    'itemsBought' => $ordersByOneBuyer
                ]);
            }

            return $orderedItems;
    }
    public function paymentOrder($paymentOrderNumber, Request $request){
        if($request->session()->has('admin_code')){
            $orderedItems = $this->paymentDetails($paymentOrderNumber);
            //return $orderedItems;
            if($request->session()->has('is_vip')){
                $customers = DB::table('group_buyers')->select('id', 'name', 'cel', 'created_at')->where('by_who', $request->session()->get('admin_code'))->orderBy('updated_at', 'desc');
                $vip = $request->session()->get('admin_code');
                $ordersRequest = DBQueriesController::getVIPOrdersRequest($vip);
                
                $amount_spent = DBQueriesController::getAmountSpent($ordersRequest->get());
                $sales_responsible = DBQueriesController::getSalesResponsible($vip)->get()[0]->sales_responsible;

                $groupBuyingRequest = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible)->get();
                if(count($groupBuyingRequest) >=1){
                    $amount_toSpend = intval($groupBuyingRequest[0]->minimum_amount_of_money);
                    $itemsNumberShouldBe = $groupBuyingRequest[0]->minimum_amount_of_items;
                }else{
                    $amount_toSpend = "-";
                    $itemsNumberShouldBe = "-";
                }
                
                return view('desktop.sales.payment_details', [
                    'isDesktop' => 1, 
                    'orderedItems' => $orderedItems,
                    'number_my_customers' => $customers->count(), 
                    'amount_spent' => $amount_spent,
                    'progress' => $this->progress_percent($vip), 
                    'amount_toSpend' => $amount_toSpend,
                    'itemsNumber' => DBQueriesController::getItemsNumber($vip),
                    'time_left' => DBQueriesController::getTimeLeft($vip),
                    'itemsNumberShouldBe' => $itemsNumberShouldBe]);

            }else{
                return view('desktop.sales.payment_details', 
                ['isDesktop' => 1, 
                'orderedItems' => $orderedItems]);
            }
            
        }else{
            return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
        }
    }

    public static function availaibility_check($group_deal_id){
        return DB::table('order_item')
            ->join('availabilities', 'availabilities.code', '=', 'order_item.productId')
            //->join('orders', 'orders.id', '=', 'order_item.orderId')
            ->join('orders', function ($join) {
                $join->on('orders.id', '=', 'order_item.orderId')
                ->on('orders.group_buying_id', '=', 'availabilities.group_buy_deal_id');
            })
            ->select('order_item.productId', 'availabilities.safety', 'availabilities.max', 'order_item.number')
            ->where('orders.group_buying_id', $group_deal_id)
            ->groupBy('order_item.productId')->get();
    }
}
