<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class DBQueriesController extends Controller
{
    public static function getGroupBuyingRequest($vip, $sales_responsible){
        $current_date = date("Y-m-d H:i:s");
        //$sales_responsible = $this->getSalesResponsible($vip)->get()[0]->sales_responsible;
        return DB::table('group_buy_deal')->where('by_who', $sales_responsible)
        ->where('activated', '1')
        ->orderBy('until_when', 'desc')
        ->limit(1);
        
        //->where('from_when', '<=', $current_date)
        //->where('until_when', '>=', $current_date);
    }

    public static function getSalesResponsible($vip){
        return DB::table('special_users')->where('username', $vip);
    }

    public static function getVIPMyCustomers($vip){
        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;
        $group_buying_id = self::getGroupBuyingRequest($vip, $sales_responsible)->get();
        return DB::table('group_buyers')->select('id')
        ->where('group_deal_id', $group_buying_id[0]->id)
        ->where('by_who', $vip);
    }

    public static function getAmountSpent($orders){
        $totalSpent = 0;
        foreach($orders as $key => $value){
            $totalSpent+=intval($value->buyerAmountToPay);
        }
        return $totalSpent;
    }

    public static function getAmountSpentWithDiscount($orders){
        $totalSpent = 0;
        foreach($orders as $key => $value){
            $totalSpent+=intval($value->buyerAmountToPay);
        }
        return $totalSpent;
    }

    public static function getVIPOrdersRequest($vip){
        $vip_my_customers = self::getVIPMyCustomers($vip);
        $ids = [];
        foreach($vip_my_customers->get() as $key => $value){
            array_push($ids, $value->id);
        }
        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $group_buying_id = self::getGroupBuyingRequest($vip, $sales_responsible)->get();
        if(count($group_buying_id) >= 1){
            return $orderRequest = DB::table('orders')
            ->join('group_buyers', 'orders.userId', '=', 'group_buyers.id')
            ->select('orders.*', 'group_buyers.name')
            ->whereIn('userId', $ids)
            ->where('group_buying_id', $group_buying_id[0]->id)
            ->where('payment_status', "0");
        }else{
            return DB::table('orders')->where('id',0);
        } 
    }

    public static function getItemsNumber($vip){
        $ordersRequest = self::getVIPOrdersRequest($vip);
        $ids = [];
        foreach($ordersRequest->get() as $key => $value){
            array_push($ids, $value->id);
        }
        $items = DB::table('order_item')->whereIn('orderId', $ids);
        $numberItems = 0;
        foreach($items->get() as $key => $value){
            $numberItems+=intval($value->number);
        }
        return $numberItems;
    }

    public static function getSalesMyVIPs($admin_code){
        return DB::table('special_users')->select('username')->where('sales_responsible', $admin_code);
    }

    public static function getTimeLeft($vip){
        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $groupBuyings = self::getGroupBuyingRequest($vip, $sales_responsible)->get();
        if(count($groupBuyings) >= 1){
            $endtime = $groupBuyings[0]->until_when;
            $time_left = strtotime($endtime) - time(); 
            if($time_left > 0) { 
                $days = floor($time_left / 86400); 
                $time_left = $time_left - $days * 86400; 
                $hours = floor($time_left / 3600); 
                $time_left = $time_left - $hours * 3600; 
                $minutes = floor($time_left / 60); 
                $seconds = $time_left - $minutes * 60; 
            } else { 
                return array(0, 0, 0, 0); 
            } 
            return array($days, $hours, $minutes, $seconds);
        }else{
            return array(0, 0, 0, 0); 
        }
         
    }

    public static function getBuyerOrders($buyer, $vip){

        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $group_buying_id = self::getGroupBuyingRequest($vip, $sales_responsible)->get();

        $buyerId = DB::table('group_buyers')->select('id')
            ->where('cel', $buyer)
            ->where('group_deal_id', $group_buying_id[0]->id)
            ->get()[0]->id;
    
        $orders = DB::table('orders')
        ->where('userId', $buyerId)
        ->where('group_buying_id', $group_buying_id[0]->id)
        ->where('payment_status', "0")->get();
        //return $orders;
        $order_details = [];
        foreach ($orders as $key => $value) {
            $userItems = DB::table('order_item')
                            ->where('orderId', $value->id)->get();
            foreach ($userItems as $key => $value) {
                array_push($order_details,$value);
            }
        }
        return $order_details;
    }

    public static function getPayments($vip, $buyer){
        
        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $group_buying_id = self::getGroupBuyingRequest($vip, $sales_responsible)->get();
        
        $buyerId = DB::table('group_buyers')->select('id')
            ->where('cel', $buyer)
            ->where('group_deal_id', $group_buying_id[0]->id)
            ->get()[0]->id;

        $orders = DB::table('orders')
        ->select(DB::raw('SUM(orders.totalAmount) as totalForVIP'), DB::raw('SUM(orders.buyerAmountToPay) as totalForBuyer'))
        ->where('userId', $buyerId)
        ->where('group_buying_id', $group_buying_id[0]->id)
        ->where('payment_status', "0")->get();
        return $orders;
    }

    public static function getVIPMyCustomersOrdersRequest($vip){
        $vip_my_customers = self::getVIPMyCustomers($vip);
        $ids = [];
        foreach($vip_my_customers->get() as $key => $value){
            array_push($ids, $value->id);
        }
        $sales_responsible = self::getSalesResponsible($vip)->get()[0]->sales_responsible;

        $group_buying_id = self::getGroupBuyingRequest($vip, $sales_responsible)->get();
        if(count($group_buying_id) >= 1){
            return $orderRequest = DB::table('orders')
            ->join('group_buyers', 'orders.userId', '=', 'group_buyers.id')
            ->select('group_buyers.name', 'group_buyers.cel', 'group_buyers.id', DB::raw('SUM(orders.totalAmount) as totalForVIP'), DB::raw('SUM(orders.buyerAmountToPay) as totalForBuyer'))
            ->whereIn('userId', $ids)
            ->where('group_buying_id', $group_buying_id[0]->id)
            ->where('payment_status', "0")
            ->groupBy('group_buyers.id');
        }else{
            return DB::table('orders')->where('id',0);
        } 
    }

}
