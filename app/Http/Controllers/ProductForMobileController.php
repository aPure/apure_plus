<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Jenssegers\Agent\Facades\Agent;

use Illuminate\Support\Facades\DB;

class ProductForMobileController extends Controller
{
    /*
    * code, name, colors, sizes, price, type
    */
    //
    public function index(Request $request){
        $products_to_load = DB::table('products')
            ->join('deal_products', 'deal_products.product_id', '=', 'products.id')
            ->select('deal_products.discount', 'products.*')
            ->where('deal_products.group_buy_deal_id', 29)->get();
        $products = [];    
        foreach ($products_to_load as $key => $value) {
            $product = [];
            array_push($product, $value->code);
            array_push($product, $value->name);
            array_push($product, $value->colors);
            array_push($product, $value->sizes);
            array_push($product, $value->unit_price);
            array_push($product, $value->type);
            array_push($product, $value->discount);
            array_push($products, $product);
        }
        return $products;



        // $products = [
        //     // ["U17001","女圓領柔感發熱衣","42, 45, 58","L, M, S","890","A", "0.65"],
        //     // ["U17002","女V領柔感發熱衣","42, 45, 58","L, M, S","890","A", "0.65"],
        //     // ["U23001","女U領多彩發熱衣","21, 23","L, M, S, XL","690","A", "0.65"],
        //     // ["U23001","女U領多彩發熱衣","31, 58","L, M, XL","690","A", "0.65"],
        //     // ["U23001","女U領多彩發熱衣","64, 71","L, M, S, XL","690","A", "0.65"],
        //     ["V0700","Pure5.5-性感美臀低腰女三角褲","121,123,138, 142,143,144,145, 152,154,162,172,174,183,185","L, M, XL","180","B", "0.65"],
        //     ["S01001","多功吸濕排汗科技運動襪","11, 13, 15, 65","L, M","280","A", "0.65"],
        //     ["S01002","雙色船型運動襪","11, 13, 15","L","250","A", "0.65"],
        //     ["S01003","素色船型運動襪","11, 13, 15","L","250","A", "0.65"],
        //     ["S01005","斜紋氣流導引運動襪 ","11, 15, 63","L, M","250","A", "0.65"],
        //     ["S01006","高彩休閒毛圈襪","11","L, M","250","A", "0.65"],
        //     ["S01007","多功科技運動襪","41, 64, 71, 81","L","250","B", "0.65"],
        //     ["S01008","多功科技運動襪","11, 62, 63, 71","L","250","B", "0.65"],
        //     ["S01011","弧型短筒運動襪","11, 12, 14, 15","L","250","B", "0.65"],
        //     ["S04001","上班上課中性襪","11, 12, 61","L","250","B", "0.65"],
        //     ["S04002","上班上課寬口襪","11, 12, 61","L","250","B", "0.65"],
        //     ["S04003","細格紋紳士襪 ","11, 12","L","250","B", "0.65"],
        //     ["S05002","格紋淑女襪","11, 46","M","250","B", "0.65"],
        //     ["S06001","兒童襪","11, 13, 15, 64","S","250","B", "0.65"],
        //     ["S07003","短筒學生襪","11, 13, 15","L, M","250","B", "0.65"],
        //     ["S07004","船型學生襪","11, 13, 15","L, M","250","B", "0.65"]
        // ];
        
        $color_code = array([
            11=> "黑",
            12=> "深灰",
            13=> "灰",
            14=> "淺灰",
            15=> "白",
            21=> "熱情紅",
            23=> "櫻桃紅",
            28=> "橙果橘",
            31=> "活力橘",
            41=> "黃",
            42=> "芥末黃",
            45=> "杏仁白",
            46=> "米黃",
            58=> "湖水綠",
            61=> "深藍",
            62=> "寶藍",
            63=> "灰藍",
            64=> "天空藍",
            65=> "水藍",
            71=> "流蘇紫",
            81=> "粉紅",
            121=> "玫瑰紅",
            123=> "靚桃紅",
            138=> "橙果橘",
            142=> "淺鵝黃",
            143=> "淡淺黃",
            144=> "粉嫩橘",
            145=> "淺米白",
            152=> "甜粉綠",
            154=> "蘋果綠",
            162=> "繽紛藍",
            172=> "薰衣紫",
            174=> "時尚紫",
            183=> "淡藍綠",
            185=> "靚亮橘"
        ]);
        $typeAhtml = "";
        
        $typeBhtml = "";
        
        $products_info = array();

        foreach($products as $key => $value){
            $discount = $value[6];
            $sizes = $value[3];
            $type = $value[5];
            $price = $value[4];
            $colors = $value[2];
            $name = $value[1];
            $code = $value[0];
            $colors = explode(",", $colors);
            $colors = preg_replace('/\s+/', '', $colors);
            
            $code_plus_color = array();
            
            foreach($colors as $key=>$value){
                array_push($code_plus_color, $code.$value);
            }

            $sizes= explode(",", $sizes);
            $sizes = preg_replace('/\s+/', '', $sizes);
            
            $size_options = "";
            foreach($sizes as $key => $value){
                if($key==0){
                    $size_options .="<option selected value='".$value."'>".$value."</option>";
                }else{
                    $size_options .="<option value='".$value."'>".$value."</option>";
                }
            }

            $info = array();        
            foreach($colors as $key => $value){

                array_push($info, $code.$value);
                array_push($info, $name);
                array_push($info, $color_code[0][$value]);
                array_push($info, implode(",", $sizes));
                array_push($info, $price);
                array_push($info, $discount);
                array_push($products_info, $info);
                $info = array();
            }   
                
        }
        if(Agent::isDesktop()){
            if($request->session()->has('username')){
                return view('desktop.special_current_customers', ['isDesktop' => 1]);
            }else{
                return view('desktop.login', ['isDesktop' => 1, 'result'=> 0, 'phoneInDB' => 0, 'passwordNotCorrect' => 0]);
            }
        }else{
            $i = 0;
            foreach($products_info as $key => $value){
                    if($i%2==0){
                        $typeAhtml.="<div class='listing_container'>
                        <div class='left_image product_image' id='".$value[0]."'>
                            <img src='http://www.apure.com.tw/img/product/groupbuy/".$value[0]."_l_1.jpg'>
                        </div>
                        <div data-popup='.popup-blur' class='open-popup left_add_to_cart ".$value[0]."' data-number='1' data-discount='".$value[5]."' data-id='".$value[0]."' data-price='".$value[4]."' data-name='".$value[1]."' data-img='http://www.apure.com.tw/img/product/groupbuy/".$value[0]."_l_1.jpg' data-size='".$value[3]."' data-color='".$value[2]."' >
                            <img src='img/button_on.png'>
                        </div>
                        <div class='left_description'>".$value[1]."-".$value[2]."</div>
                        <div class='left_price'>$".$value[4]."</div><div class='left_group_price'>$".strval(ceil(intval($value[4])*0.65))."</div>";
                    }else if($i%2==1){
                        $typeAhtml.="<div class='right_image product_image' id='".$value[0]."'>
                            <img src='http://www.apure.com.tw/img/product/groupbuy/".$value[0]."_l_1.jpg'>
                        </div>
                        <div data-popup='.popup-blur' class='open-popup right_add_to_cart ".$value[0]."' data-number='1' data-id='".$value[0]."' data-price='".$value[4]."' data-name='".$value[1]."' data-img='http://www.apure.com.tw/img/product/groupbuy/".$value[0]."_l_1.jpg' data-size='".$value[3]."' data-color='".$value[2]."'>
                            <img src='img/button_on.png'>
                        </div>
                        <div class='right_description'>".$value[1]."-".$value[2]."</div>
                        <div class='right_price'>$".$value[4]."</div><div class='right_group_price'>$".strval(ceil(intval($value[4])*0.65))."</div>
                        </div>";
                    }
                    $i++;
                    
            }
            

            return view('welcome', [
                'A' => $typeAhtml,
                'B' => $typeBhtml
            ]);
        }
                
    }
}
