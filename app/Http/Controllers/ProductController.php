<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function fegood(){
        return view('category.fegood');
    }

    public function item($item_id){
        return view('desktop.special_current_customers_items', ['id' => $item_id]);
    }
}
