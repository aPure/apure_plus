<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\DBQueriesController;

class WorkerController extends Controller
{
    public function group_deal_id($vip){
        $sales_responsible = DBQueriesController::getSalesResponsible($vip);
        $group_id = DBQueriesController::getGroupBuyingRequest($vip, $sales_responsible->get()[0]->sales_responsible);
        return $group_id->get()[0]->id;
    }
}
